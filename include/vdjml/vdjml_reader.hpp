/** @file "/vdjml/include/vdjml/vdjml_reader.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_READER_HPP_
#define VDJML_READER_HPP_
#include <istream>
#include <fstream>
#include <string>
#include <memory>
#include "boost/scoped_ptr.hpp"
#include "boost/shared_ptr.hpp"
#include "boost/iostreams/filtering_streambuf.hpp"

#include "vdjml/config.hpp"
#include "vdjml/compression.hpp"
#include "vdjml/exception.hpp"

namespace vdjml{
class Read_result;
class Results_meta;
class Vdjml_generator_info;

namespace detail{
class Sax_handler;
class Sax_builder;
}//namespace detail

/**@brief
*******************************************************************************/
class VDJML_DECL Vdjml_reader {
   typedef boost::scoped_ptr<std::istream> is_ptr;
   typedef boost::scoped_ptr<std::ifstream> ifs_ptr;
   typedef boost::iostreams::filtering_istreambuf fisb_t;
   typedef boost::scoped_ptr<fisb_t> fisb_ptr;
   typedef boost::shared_ptr<detail::Sax_handler> shandler_ptr;
   typedef boost::shared_ptr<detail::Sax_builder> sbuilder_ptr;

   static std::ifstream* make_ifstream(
            std::string const& path,
            Compression& compr
   );

   static fisb_t* make_fisb(std::istream& os, const Compression compr);

public:
   struct Err : public base_exception {};
   typedef boost::shared_ptr<Results_meta> meta_ptr_t;
   typedef std::auto_ptr<Read_result> result_ptr_t;

   Vdjml_reader(std::istream& is, Compression compr = Uncompressed);

   Vdjml_reader(std::string const& path, Compression compr = Uncompressed);

   unsigned version() const;
   std::string version_str() const;
   Vdjml_generator_info const& generator_info() const;
   meta_ptr_t meta() const;
   void next();
   bool has_result() const;
   Read_result const& result() const;
   Read_result& result();
   result_ptr_t result_release() const;

private:
   ifs_ptr ifs_;
   fisb_ptr fisb_;
   is_ptr fisb_is_;
   std::istream& is_;
   sbuilder_ptr builder_;
   shandler_ptr handler_;
};

}//namespace vdjml
#endif /* VDJML_READER_HPP_ */
