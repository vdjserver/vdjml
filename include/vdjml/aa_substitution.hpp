/** @file "/vdjml/include/vdjml/aa_substitution.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef AA_SUBSTITUTION_HPP_
#define AA_SUBSTITUTION_HPP_
#include "vdjml/aa_index.hpp"
#include "vdjml/config.hpp"
#include "vdjml/detail/comparison_operators_macro.hpp"
#include "vdjml/vdjml_current_version.hpp"

namespace vdjml{
class Xml_writer;

/**@brief
todo: compact storage
*******************************************************************************/
class Aa_substitution {
public:

   /**
    @param read_pos0 0-based position of first nucleotide in the read
    @param read_aa amino acid in read
    @param gl_aa amino acid in germline
    */
   Aa_substitution(
            const unsigned read_pos0,
            const aminoacid::Aa read_aa,
            const aminoacid::Aa gl_aa
   )
   : pos_(read_pos0),
     aa_read_(read_aa),
     aa_gl_(gl_aa)
   {}

   /**
    @param read_pos0 0-based position of first nucleotide in the read
    @param read_aa amino acid in read
    @param gl_aa amino acid in germline
    */
   Aa_substitution(
            const unsigned read_pos0,
            const char read_aa,
            const char gl_aa
   )
   : pos_(read_pos0),
     aa_read_(aminoacid_index(read_aa)),
     aa_gl_(aminoacid_index(gl_aa))
   {}

   /**
    @param read_pos0 0-based position of first nucleotide in the read
    @param read_aa amino acid in read
    @param gl_aa amino acid in germline
    */
   Aa_substitution(
            const unsigned read_pos0,
            std::string const& read_aa,
            std::string const& gl_aa
   )
   : pos_(read_pos0),
     aa_read_(aminoacid_index(read_aa)),
     aa_gl_(aminoacid_index(gl_aa))
   {}

   bool operator==(Aa_substitution const& aas) const {return pos_ == aas.pos_;}
   bool operator<(Aa_substitution const& aas) const {return pos_ < aas.pos_;}
   VDJML_COMPARISON_OPERATOR_MEMBERS(Aa_substitution)

   /**@return 0-based read position of the codone's first nucleotide */
   unsigned read_position() const {return pos_;}

   /**@return amino acid encoded by the read sequence */
   aminoacid::Aa read_aa() const {return aa_read_;}

   /**@return amino acid encoded by the germline sequence */
   aminoacid::Aa gl_aa() const {return aa_gl_;}

private:
   unsigned pos_;
   aminoacid::Aa aa_read_;     /**< amino acid in read */
   aminoacid::Aa aa_gl_;       /**< amino acid in germline */
};

/**@brief
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Aa_substitution const& aas,
         const unsigned version = VDJML_CURRENT_VERSION
);

}//namespace vdjml
#endif /* AA_SUBSTITUTION_HPP_ */
