/** @file "/vdjml/include/vdjml/read_result_fwd.hpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef READ_RESULT_FWD_HPP_
#define READ_RESULT_FWD_HPP_
#include <memory>
#include "vdjml/vdjml_current_version.hpp"
#include "vdjml/config.hpp"

namespace vdjml{

class Xml_writer;
class Results_meta;
class Read_result;

typedef std::auto_ptr<Read_result> read_result_ptr;

/**@brief
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Read_result const& rr,
         Results_meta const& rm,
         const unsigned version = VDJML_CURRENT_VERSION
);


}//namespace vdjml
#endif /* READ_RESULT_FWD_HPP_ */
