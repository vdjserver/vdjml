/** @file "/vdjml/include/vdjml/vdjml_version.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_VERSION_HPP_
#define VDJML_VERSION_HPP_
#include <limits>
#include "boost/lexical_cast.hpp"
#include "vdjml/vdjml_current_version.hpp"
#include "vdjml/config.hpp"
#include "vdjml/exception.hpp"

namespace vdjml{

/**@brief
*******************************************************************************/
inline unsigned version_major(const unsigned v) {
   return v / VDJML_VERSION_MAJOR_MULTIPLIER;
}

/**@brief
*******************************************************************************/
inline unsigned version_minor(const unsigned v) {
   return v % VDJML_VERSION_MAJOR_MULTIPLIER;
}

/**@brief
*******************************************************************************/
inline unsigned version(const unsigned major, const unsigned minor) {
   if(
         ( major >= std::numeric_limits<unsigned>::max() /
         VDJML_VERSION_MAJOR_MULTIPLIER ) ||
         minor >= VDJML_VERSION_MAJOR_MULTIPLIER
   ) {
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("version value exceeds the limit")
               << base_exception::int1_t(major)
               << base_exception::int2_t(minor)
      );
   }
   return major * 1000 + minor;
}

/**@brief
*******************************************************************************/
inline std::string version(const unsigned v) {
   return
            boost::lexical_cast<std::string>(version_major(v)) + '.' +
            boost::lexical_cast<std::string>(version_minor(v))
            ;
}

/**@brief parse string like [0-4294966].[0-999]
*******************************************************************************/
VDJML_DECL unsigned version(std::string const& v);

/**@brief
*******************************************************************************/
inline bool is_supported(const unsigned v) {
   switch(v) {
   case VDJML_CURRENT_VERSION: return true;
   }
   return false;
}

}//namespace vdjml
#endif /* VDJML_VERSION_HPP_ */
