/** @file "/vdjml/include/vdjml/trim_complement.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef TRIM_COMPLEMENT_HPP_
#define TRIM_COMPLEMENT_HPP_
#include <string>
#include "boost/range.hpp"
#include "boost/range/as_literal.hpp"
#include "boost/utility/string_ref.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/nucleotide_index.hpp"
#include "vdjml/interval.hpp"

namespace vdjml{

/**@brief Reverse-complement sequence
*******************************************************************************/
template<class Range> inline std::string reverse_complement(Range const& r) {
   std::string rstr(boost::size(boost::as_literal(r)), 'X');
   std::string::reverse_iterator i = rstr.rbegin();
   typedef typename boost::range_iterator<const Range>::type iter;
   iter j = boost::begin(boost::as_literal(r)), end = boost::end(boost::as_literal(r));
   for(; j != end; ++i, ++j) {
      *i = nucleotide_complement((char)*j);
   }
   return rstr;
}

/**@brief Trim and optionally reverse-complement a sequence
*******************************************************************************/
inline std::string trim_complement(
         const boost::string_ref seq,
         Interval const& si,
         const bool reverse
) {
   if( si.last0() > seq.size() ) BOOST_THROW_EXCEPTION(
      base_exception()
      << base_exception::msg_t("sequence interval and length mismatch")
      << base_exception::int1_t(si.last0())
      << base_exception::int2_t(seq.size())
   );

   return reverse ?
            reverse_complement(seq.substr(si.pos0(), si.length())) :
            seq.substr(si.pos0(), si.length()).to_string()
            ;
}

}//namespace vdjml
#endif /* TRIM_COMPLEMENT_HPP_ */
