/** @file "/vdjml/include/vdjml/nucleotide_iterator.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef NUCLEOTIDE_ITERATOR_HPP_
#define NUCLEOTIDE_ITERATOR_HPP_
#include "boost/utility/string_ref.hpp"
#include "vdjml/aligned_sequence.hpp"
#include "vdjml/btop.hpp"
#include "vdjml/config.hpp"
#include "vdjml/nucleotide_index.hpp"
#include "vdjml/nucleotide_match.hpp"

namespace vdjml{

/**@brief Iterate over BTOP from mismatch to mismatch, while keeping track of
query and reference positions.
*******************************************************************************/
class VDJML_DECL Mismatch_iter {
public:
   typedef Btop::Err Err;
   typedef Nucleotide_match value_type;

   explicit Mismatch_iter(Btop const& bt);

   boost::array<std::size_t,2> const& positions() const {return pos_;}

   char const* letters() const {
      BOOST_ASSERT( is_nucleotide(curr_[0]) );
      BOOST_ASSERT( is_nucleotide(curr_[1]) );
      BOOST_ASSERT( 1 < (end_ - curr_) && "should be at least two characters" );
      return curr_;
   }

   bool is_gap(const seq::Seq s) const {return ::vdjml::is_gap(letters()[s]);}

   /**@brief find next mismatch  */
   void next() {
      BOOST_ASSERT(! at_end() );
      step_char();
      step_num();
   }

   /**@return true if end of BTOP was reached.
    In that case, position() points one nucleotide beyond query and reference
    sequences.
    */
   bool at_end() const {return curr_ == end_;}

   /**Find first non-gap nucleotide at or after ap.
      If not present, point after last nucleotide
      @param ap position in read or germline sequence
    */
   void move_to(Aligned_pos const& ap) {
      for( ; ! at_end(); next() ) {

         //if current position is greater than requested, a non-gap nucleotide
         //must have been encountered
         if( ap.pos() < positions()[ap.kind()] ) break;

         //stop iterating only if not a gap
         if( ap.pos() == positions()[ap.kind()] && ! is_gap(ap.kind()) ) break;
      }
   }

   Nucleotide_match get() const {
      boost::array<char,2> curr = {{curr_[0], curr_[1]}};
      return Nucleotide_match(pos_, curr);
   }

private:
   char const* curr_;
   char const* end_;
   boost::array<std::size_t,2> pos_;

   void step_num();
   void step_char();
};

namespace detail{

/**@brief
*******************************************************************************/
class VDJML_DECL Aligned_nuc_iter_impl {
public:
   typedef Btop::Err Err;

   boost::array<std::size_t,2> const& positions() const {return pos_;}

   bool at_end() const {
      return
               mi_.at_end() &&
               positions()[seq::Read] == mi_.positions()[seq::Read]
                                                         ;
   }

   void next() {
      BOOST_ASSERT( ! at_end() );
      if( positions()[seq::Read] != mi_.positions()[seq::Read] ) {
         ++pos_[seq::Read];
         ++pos_[seq::GL];
      } else {
         if( mi_.is_gap(seq::Read) ) {
            ++pos_[seq::GL];
         } else {
            ++pos_[seq::Read];
            if( ! mi_.is_gap(seq::GL) ) ++pos_[seq::GL];
         }
         mi_.next();
      }
   }

   /** if ap is beyond the sequence stop at last + 1 position */
   void move_to(Aligned_pos const& ap) {
      if( ap.pos() < positions()[ap.kind()] ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("only forward movement is supported")
               << Err::int1_t(ap.pos())
               << Err::int2_t(positions()[ap.kind()])
      );

      if( mi_.positions()[ap.kind()] <= ap.pos() ) {
         mi_.move_to(ap);
      }

      if( mi_.positions()[ap.kind()] >= ap.pos() ) {
         pos_[ap.kind()] = ap.pos();
         const seq::Seq Other = static_cast<seq::Seq>(! ap.kind());
         pos_[Other] =
                  ap.pos() +
                  mi_.positions()[Other] - mi_.positions()[ap.kind()];
      } else {
         BOOST_ASSERT(
                  mi_.at_end() &&
                  "if mismatch iterator did not reach destination it must be "
                  "at end"
         );
         pos_ = mi_.positions();
      }
   }

protected:
   Aligned_nuc_iter_impl(Btop const& btop);

   Mismatch_iter mi_;
   boost::array<std::size_t,2> pos_;
};

}//namespace detail

/**@brief
*******************************************************************************/
class Match_char {
public:
   explicit Match_char(const char c) : c_(c) {}
   char get(const std::size_t) const {return c_;}
   void check(char, const std::size_t) const {}
   seq::Seq kind() const {return seq::Read;}

private:
   char c_;
};

/**@brief Hold input sequence reference and its kind: read or germline
*******************************************************************************/
struct In_seq_ref : public Aligned_seq {
   typedef Btop::Err Err;

   explicit In_seq_ref(Aligned_seq const& as) : Aligned_seq(as) {}

   In_seq_ref(const seq::Seq k, const boost::string_ref s)
   : Aligned_seq(k, s) {}

   char get(const std::size_t n) const {
      if( n >= seq().size() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("input sequence too short")
               << Err::int1_t(n)
      );
      BOOST_ASSERT( is_nucleotide(seq()[n]) );
      return seq()[n];
   }

   void check(char c, const std::size_t n) const {
      if(
               c != nucleotide::Special::Gap &&
               std::toupper(c) != std::toupper(get(n))
      ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("mismatch between BTOP and sequence")
               << Err::str1_t(sanitize(c))
               << Err::str2_t(sanitize(get(n)))
               << Err::int1_t(n)
      );
   }
};

/**@brief Hold input sequence value and its kind: read or germline
*******************************************************************************/
class In_seq_val {
public:
   typedef Btop::Err Err;

   In_seq_val(const seq::Seq in, std::string const& seq)
   : seq_(seq), in_(in)
   {}

   seq::Seq kind() const {return in_;}

   char get(const std::size_t n) const {
      if( n >= seq_.size() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("input sequence too short")
               << Err::int1_t(n)
      );
      BOOST_ASSERT( is_nucleotide(seq_[n]) );
      return seq_[n];
   }

   void check(char c, const std::size_t n) const {
      if(
               c != nucleotide::Special::Gap &&
               std::toupper(c) != std::toupper(get(n))
      ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("mismatch between BTOP and sequence")
               << Err::str1_t(sanitize(c))
               << Err::str2_t(sanitize(get(n)))
               << Err::int1_t(n)
      );
   }
private:
   std::string seq_;
   seq::Seq in_;
};

/**@brief Iterate over pairs of nucleotides in aligned sequences
*******************************************************************************/
template<class SeqIn> class Aligned_nuc_iter :
         public detail::Aligned_nuc_iter_impl {
public:
   typedef Nucleotide_match value_type;

   Aligned_nuc_iter(Btop const& btop, SeqIn const& seq)
   : detail::Aligned_nuc_iter_impl(btop),
     seq_(seq)
   {}

   boost::array<char,2> letters() const {
      if( pos_[seq::Read] != mi_.positions()[seq::Read] ) {
         const boost::array<char,2> a = {{
                  seq_.get(pos_[seq_.kind()]),
                  seq_.get(pos_[seq_.kind()])
         }};
         return a;
      }
      const boost::array<char,2> a = {{mi_.letters()[0], mi_.letters()[1]}};
      seq_.check(a[seq_.kind()], pos_[seq_.kind()]);
      return a;
   }

   Nucleotide_match get() const {
      return Nucleotide_match(positions(), letters());
   }

private:
   SeqIn seq_;
};

typedef Aligned_nuc_iter<In_seq_ref> nucleotide_iter_s;
typedef Aligned_nuc_iter<In_seq_val> nucleotide_iter_sv;
typedef Aligned_nuc_iter<Match_char> nucleotide_iter;

/**@brief
*******************************************************************************/
inline nucleotide_iter_s nucleotide_iterator(
         Btop const& btop,
         Aligned_seq const& seq
) {
   return nucleotide_iter_s(btop, In_seq_ref(seq));
}

/**@brief
*******************************************************************************/
inline nucleotide_iter nucleotide_iterator(
         Btop const& btop,
         const char match_char = nucleotide::Special::Unknown
) {
   return nucleotide_iter(btop, Match_char(match_char));
}

}//namespace vdjml
#endif /* NUCLEOTIDE_ITERATOR_HPP_ */
