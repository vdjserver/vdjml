/** @file "/vdjml/include/vdjml/generator_info.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef GENERATOR_INFO_HPP_
#define GENERATOR_INFO_HPP_
#include <string>
#include "boost/date_time/posix_time/ptime.hpp"
#include "vdjml/config.hpp"
#include "vdjml/vdjml_current_version.hpp"

namespace vdjml{
class Xml_writer;

/**@brief Information about the software that generated VDJML
*******************************************************************************/
class VDJML_DECL Vdjml_generator_info {
public:
   Vdjml_generator_info(
            std::string const& name,
            std::string const& version,
            boost::posix_time::ptime const& time_gmt
   );

   Vdjml_generator_info(
            std::string const& name = "",
            std::string const& version = "",
            std::string const& time_gmt = ""
   );

   std::string const& name() const {return name_;}
   std::string const& version() const {return version_;}
   boost::posix_time::ptime datetime() const {return time_gmt_;}
   std::string datetime_str() const;

private:
   std::string name_;
   std::string version_;
   boost::posix_time::ptime time_gmt_;
};

/**
@param xw XML writer
@param gi generator info
@param version format version
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Vdjml_generator_info const& gi,
         const unsigned version = VDJML_CURRENT_VERSION
);

}//namespace vdjml
#endif /* GENERATOR_INFO_HPP_ */
