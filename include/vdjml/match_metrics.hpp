/** @file "/vdjml/include/vdjml/match_metrics.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef MATCH_METRICS_HPP_
#define MATCH_METRICS_HPP_
#include "boost/cstdint.hpp"
#include "vdjml/config.hpp"
#include "vdjml/vdjml_current_version.hpp"
#include "vdjml/percent.hpp"

namespace vdjml{
class Xml_writer;
class Match_metrics;
class Segment_match;

namespace detail{

bool parse(
         std::string const& name,
         std::string const& val,
         Match_metrics& mm
);

}//namespace detail

/**@brief
*******************************************************************************/
class VDJML_DECL Match_metrics {
   enum {
      StopCodon = 0, MutatedInvariant = 1, Inverted = 2, OutFrameIndel = 3,
      OutFrameVDJ = 4
   };

   static unsigned char set(
            const bool stop_codon,
            const bool mutated_invariant,
            const bool inverted,
            const bool out_frame_indel,
            const bool out_frame_vdj
   ) {
      return (unsigned char) 0 |
               stop_codon << StopCodon |
               mutated_invariant << MutatedInvariant |
               inverted << Inverted |
               out_frame_indel << OutFrameIndel |
               out_frame_vdj << OutFrameVDJ
               ;
   }

   friend class Segment_match;

   friend bool detail::parse(
            std::string const& name,
            std::string const& val,
            Match_metrics& mm
   );

public:
   typedef boost::int_least16_t score_t;
   typedef boost::uint_least16_t count_t;

   /** alignment score value indicating that it is undefined */
   static const score_t unscore;

   /**
    @param identity
    @param score
    @param substitutions
    @param insertions
    @param deletions
    @param stop_codon
    @param mutated_invariant
    @param inverted
    @param out_frame_indel
    @param out_frame_vdj
    */
   Match_metrics(
            const float identity          = Percent().percent(),
            const int score               = unscore,
            const unsigned substitutions  = 0,
            const unsigned insertions     = 0,
            const unsigned deletions      = 0,
            const bool stop_codon         = false,
            const bool mutated_invariant  = false,
            const bool inverted           = false,
            const bool out_frame_indel    = false,
            const bool out_frame_vdj      = false
   )
   : identity_(identity),
     score_(score),
     substitutions_(substitutions),
     insertions_(insertions),
     deletions_(deletions),
     v_(
              set(
                 stop_codon,
                 mutated_invariant,
                 inverted,
                 out_frame_indel,
                 out_frame_vdj
              )
     )
   {}

   score_t score() const {return score_;}
   Percent identity() const {return identity_;}
   count_t insertions() const {return insertions_;}
   count_t deletions() const {return deletions_;}
   count_t substitutions() const {return substitutions_;}
   bool stop_codon() const {return v_ & 1 << StopCodon;}
   bool mutated_invariant() const {return v_ & 1 << MutatedInvariant;}
   bool inverted() const {return v_ & 1 << Inverted;}
   bool out_frame_indel() const {return v_ & 1 << OutFrameIndel;}
   bool out_frame_vdj() const {return v_ & 1 << OutFrameVDJ;}
   bool frame_shift() const {return out_frame_indel() || out_frame_vdj();}

   bool productive() const {
      return ! (frame_shift() || stop_codon() || mutated_invariant());
   }

   bool operator==(Match_metrics const& mm) const {
      return
               identity_ == mm.identity_ &&
               score_ == mm.score_ &&
               substitutions_ == mm.substitutions_ &&
               insertions_ == mm.insertions_ &&
               deletions_ == mm.deletions_ &&
               v_ == mm.v_
               ;
   }

private:
   Percent identity_;
   score_t score_;
   count_t substitutions_;
   count_t insertions_;
   count_t deletions_;
   unsigned char v_;
};

/**@brief
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Match_metrics const& mm,
         const unsigned version = VDJML_CURRENT_VERSION
);

}//namespace vdjml
#endif /* MATCH_METRICS_HPP_ */
