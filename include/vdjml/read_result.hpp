/** @file "/vdjml/include/vdjml/read_result.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef READ_RESULT_HPP_
#define READ_RESULT_HPP_
#include <string>
#include <vector>
#include "vdjml/exception.hpp"
#include "vdjml/read_result_fwd.hpp"
#include "vdjml/segment_combination.hpp"
#include "vdjml/segment_match_map.hpp"

namespace vdjml{

/**@brief Analysis results of a single sequencing read
*******************************************************************************/
class VDJML_DECL Read_result {
public:
   typedef std::vector<Segment_combination> seg_comb_store;
   struct Err : public base_exception {};

   explicit Read_result(std::string const& id)
   : id_(id)
   {}

   Segment_match const& operator[](const Seg_match_id id) const {
      return smm_[id];
   }

   Segment_match& operator[](const Seg_match_id id) {
      return smm_[id];
   }

   Seg_match_id insert(Segment_match const& rsm) {
      return smm_.insert(rsm);
   }

   void insert(Segment_combination const& sc);

   std::string const& id() const {return id_;}

   Segment_match_map const & segment_matches() const   {return smm_;}
   Segment_match_map       & segment_matches()         {return smm_;}
   seg_comb_store const & segment_combinations() const   {return scm_;}
   seg_comb_store       & segment_combinations()         {return scm_;}

private:
   std::string id_;
   Segment_match_map smm_;
   seg_comb_store scm_;
};

}//namespace vdjml
#endif /* READ_RESULT_HPP_ */
