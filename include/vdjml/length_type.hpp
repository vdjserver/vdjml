/** @file "/vdjml/include/vdjml/length_type.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef LENGTH_TYPE_HPP_
#define LENGTH_TYPE_HPP_
#include "boost/cstdint.hpp"

#ifndef VDJML_SEQUENCE_LENGTH_TYPE
/** 65,536 max length */
#define VDJML_SEQUENCE_LENGTH_TYPE boost::uint_least16_t
#endif

namespace vdjml{

typedef VDJML_SEQUENCE_LENGTH_TYPE length_t;

}//namespace vdjml
#endif /* LENGTH_TYPE_HPP_ */
