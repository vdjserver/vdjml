/** @file "/vdjml/include/vdjml/chain_type.hpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef CHAIN_TYPE_HPP_
#define CHAIN_TYPE_HPP_

namespace vdjml{

/**@brief Chain type
*******************************************************************************/
enum Chain_type {
   Chain_heavy, Chain_light, Chain_undetermined
};

}//namespace vdjml
#endif /* CHAIN_TYPE_HPP_ */
