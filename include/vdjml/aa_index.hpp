/** @file "/vdjml/include/vdjml/aa_index.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef AA_INDEX_HPP_
#define AA_INDEX_HPP_
#include "vdjml/config.hpp"
#include "vdjml/exception.hpp"

namespace vdjml{ namespace aminoacid{

/**@brief
*******************************************************************************/
struct Special {
   BOOST_STATIC_CONSTANT(char, Stop = '*');
};

/**@brief
*******************************************************************************/
enum Aa {
   Ala,   ///< Alanine, A
   Arg,   ///< Arginine, R
   Asn,   ///< Asparagine, N
   Asp,   ///< Aspartic Acid, D
   Cys,   ///< Cysteine, C
   Gln,   ///< Glutamine, Q
   Glu,   ///< Glutamic Acid, E
   Gly,   ///< Glycine, G
   His,   ///< Histidine, H
   Ile,   ///< Isoleucine, I
   Leu,   ///< Leucine, L
   Lys,   ///< Lysine, K
   Met,   ///< Methionine, M
   Phe,   ///< Phenylalanine, F
   Pro,   ///< Proline, P
   Ser,   ///< Serine, S
   Thr,   ///< Threonine, T
   Trp,   ///< Tryptophan, W
   Tyr,   ///< Tyrosine, Y
   Val,   ///< Valine, V
   Stop   ///< Stop codon, *
};

/**@brief
*******************************************************************************/
inline char to_capital(const Aa n) {
   switch(n) {
   case Ala: return 'A';
   case Cys: return 'C';
   case Asp: return 'D';
   case Glu: return 'E';
   case Phe: return 'F';
   case Gly: return 'G';
   case His: return 'H';
   case Ile: return 'I';
   case Lys: return 'K';
   case Leu: return 'L';
   case Met: return 'M';
   case Asn: return 'N';
   case Pro: return 'P';
   case Gln: return 'Q';
   case Arg: return 'R';
   case Ser: return 'S';
   case Thr: return 'T';
   case Val: return 'V';
   case Trp: return 'W';
   case Tyr: return 'Y';
   case Stop: return Special::Stop;
   default:
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("invalid amino acid")
               << base_exception::int1_t(n)
      );
   }
   return Stop; // never comes here
}

/**@brief
*******************************************************************************/
inline char to_small(const Aa n) {
   switch(n) {
   case Ala: return 'a';
   case Cys: return 'c';
   case Asp: return 'd';
   case Glu: return 'e';
   case Phe: return 'f';
   case Gly: return 'g';
   case His: return 'h';
   case Ile: return 'i';
   case Lys: return 'k';
   case Leu: return 'l';
   case Met: return 'm';
   case Asn: return 'n';
   case Pro: return 'p';
   case Gln: return 'q';
   case Arg: return 'r';
   case Ser: return 's';
   case Thr: return 't';
   case Val: return 'v';
   case Trp: return 'w';
   case Tyr: return 'y';
   case Stop: return Special::Stop;
   default:
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("invalid amino acid")
               << base_exception::int1_t(n)
      );
   }
   return Stop; // never comes here
}

/**@brief
*******************************************************************************/
inline char const* to_3letter(const Aa n) {
   switch(n) {
   case Ala: return "Ala";
   case Cys: return "Cys";
   case Asp: return "Asp";
   case Glu: return "Glu";
   case Phe: return "Phe";
   case Gly: return "Gly";
   case His: return "His";
   case Ile: return "Ile";
   case Lys: return "Lys";
   case Leu: return "Leu";
   case Met: return "Met";
   case Asn: return "Asn";
   case Pro: return "Pro";
   case Gln: return "Gln";
   case Arg: return "Arg";
   case Ser: return "Ser";
   case Thr: return "Thr";
   case Val: return "Val";
   case Trp: return "Trp";
   case Tyr: return "Tyr";
   case Stop: return "Stop";
   default:
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("invalid amino acid")
               << base_exception::int1_t(n)
      );
   }
   return 0; // never comes here
}

}// namespace aa

/**@brief
*******************************************************************************/
VDJML_DECL aminoacid::Aa aminoacid_index(std::string aa_str);

/**@brief
*******************************************************************************/
inline aminoacid::Aa aminoacid_index(const char c) {
   using namespace aminoacid;
   switch(c) {
   case 'A':
   case 'a':
      return Ala;
   case 'C':
   case 'c':
      return Cys;
   case 'D':
   case 'd':
      return Asp;
   case 'E':
   case 'e':
      return Glu;
   case 'F':
   case 'f':
      return Phe;
   case 'G':
   case 'g':
      return Gly;
   case 'H':
   case 'h':
      return His;
   case 'I':
   case 'i':
      return Ile;
   case 'K':
   case 'k':
      return Lys;
   case 'L':
   case 'l':
      return Leu;
   case 'M':
   case 'm':
      return Met;
   case 'N':
   case 'n':
      return Asn;
   case 'P':
   case 'p':
      return Pro;
   case 'Q':
   case 'q':
      return Gln;
   case 'R':
   case 'r':
      return Arg;
   case 'S':
   case 's':
      return Ser;
   case 'T':
   case 't':
      return Thr;
   case 'V':
   case 'v':
      return Val;
   case 'W':
   case 'w':
      return Trp;
   case 'Y':
   case 'y':
      return Tyr;
   case Special::Stop:
      return Stop;
   default:
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("invalid amino acid code")
               << base_exception::str1_t(sanitize(c))
      );
   }
   return Ala; // never comes here
}

}//namespace vdjml
#endif /* AA_INDEX_HPP_ */
