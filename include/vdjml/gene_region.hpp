/** @file "/vdjml/include/vdjml/gene_region.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef GENE_REGION_HPP_
#define GENE_REGION_HPP_
#include "vdjml/config.hpp"
#include "vdjml/detail/comparison_operators_macro.hpp"
#include "vdjml/interval.hpp"
#include "vdjml/match_metrics.hpp"
#include "vdjml/object_ids.hpp"
#include "vdjml/vdjml_current_version.hpp"

namespace vdjml{
class Xml_writer;
class Results_meta;

/**@brief
*******************************************************************************/
class Gene_region {
public:
   Gene_region(
            const Region_id region,
            const Numsys_id num_system,
            const Aligner_id aligner,
            Interval const& read_range,
            Match_metrics const& mm
   )
   : num_system_(num_system),
     aligner_(aligner),
     region_(region),
     mm_(mm),
     read_range_(read_range)
   {}

   Numsys_id numbering_system() const {return num_system_;}
   Aligner_id aligner() const {return aligner_;}
   Region_id region_type() const {return region_;}
   Interval const& read_range() const {return read_range_;}
   Match_metrics const& match_metrics() const {return mm_;}

   bool operator==(Gene_region const& gr) const {
      return num_system_ == gr.num_system_ && region_ == gr.region_;
   }

   bool operator<(Gene_region const& gr) const {
      if( num_system_ < gr.num_system_ ) return true;
      if( gr.num_system_ < num_system_ ) return false;
      return region_ < gr.region_;
   }

   VDJML_COMPARISON_OPERATOR_MEMBERS(Gene_region)

private:
   Numsys_id num_system_;
   Aligner_id aligner_;
   Region_id region_;
   Match_metrics mm_;
   Interval read_range_; ///< read positions aligned
};

/**@brief
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Gene_region const& gr,
         Results_meta const& rm,
         const unsigned version = VDJML_CURRENT_VERSION
);

}//namespace vdjml
#endif /* GENE_REGION_HPP_ */
