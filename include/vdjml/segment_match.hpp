/** @file "/vdjml/include/vdjml/segment_match.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef SEGMENT_MATCH_HPP_
#define SEGMENT_MATCH_HPP_
#include <string>
#include <vector>
#include "boost/foreach.hpp"
#include "vdjml/aa_substitution.hpp"
#include "vdjml/btop.hpp"
#include "vdjml/config.hpp"
#include "vdjml/detail/comparison_operators_macro.hpp"
#include "vdjml/detail/id_map.hpp"
#include "vdjml/detail/vector_set.hpp"
#include "vdjml/gene_segment_type.hpp"
#include "vdjml/gl_segment_match.hpp"
#include "vdjml/match_metrics.hpp"
#include "vdjml/object_ids.hpp"
#include "vdjml/vdjml_current_version.hpp"
#include "vdjml/length_type.hpp"

namespace vdjml{
class Xml_writer;
class Results_meta;

/**@brief Alignment results for a read segment
@details
*******************************************************************************/
class VDJML_DECL Segment_match {
   friend class Segment_match_map;

public:
   typedef detail::Id_map<Gl_seg_match_id, Gl_segment_match> germline_segment_map;
   typedef detail::Vector_set<Aa_substitution> aa_substitution_set;
   typedef Interval::length_t length_t;
   struct Err : public base_exception {};

   /**
    @param read_pos0
    @param btop
    @param mm
    */
   Segment_match(
            const std::size_t read_pos0,
            Btop const& btop,
            Match_metrics const& mm = Match_metrics()
   );

   Seg_match_id id() const {return id_;}
   Btop const& btop() const {return btop_;}

   /**@return read sequence nucleotide range that matches to germline segment */
   Interval const& read_range() const {return read_range_;}
   Interval gl_range() const {return gl_range(gl_segments().front());}

   Interval gl_range(Gl_segment_match const& gsm) const {
      return Interval::pos0_len(gsm.gl_position(), gl_len_);
   }

   /**@return match metrics */
   Match_metrics const& match_metrics() const {return mm_;}
   germline_segment_map const& gl_segments() const {return gsv_;}
   aa_substitution_set const& aa_substitutions() const {return aass_;}
   length_t gl_length() const {return gl_len_;}

   Gl_seg_match_id insert(Gl_segment_match const& gsm) {
      if( Gl_seg_match_id const* p = find(gsm) ) return *p;
      const Gl_seg_match_id id = gsv_.insert(gsm);
      gsv_[id].id_ = id;
      return id;
   }

   Gl_seg_match_id const* find(Gl_segment_match const& gsm) const {
      BOOST_FOREACH(Gl_segment_match const& gsm0, gsv_) {
         if( gsm0 == gsm ) return &gsm0.id_;
      }
      return 0;
   }

   void insert(Aa_substitution const& aas) {
      aass_.insert(aas);
   }

   bool operator==(Segment_match const& sm) const {
      return btop_ == sm.btop_ && read_range_ == sm.read_range_;
   }

   bool operator<(Segment_match const& sm) const {
      if( read_range() < sm.read_range() ) return true;
      if( sm.read_range() < read_range() ) return false;
      return btop() < sm.btop();
   }

   VDJML_COMPARISON_OPERATOR_MEMBERS(Segment_match)

private:
   Seg_match_id id_;
   Btop btop_;
   Interval read_range_;
   Match_metrics mm_;
   germline_segment_map gsv_;
   aa_substitution_set aass_;
   length_t gl_len_;

   void init(const std::size_t read_pos0);
};

/**@brief
*******************************************************************************/
VDJML_DECL Nucleotide_match nucleotide_match(
         Segment_match const& sm,
         Aligned_pos pos0,
         Gl_segment_match const& gsm
);

/**@brief
*******************************************************************************/
inline Nucleotide_match nucleotide_match(
         Segment_match const& sm,
         Aligned_pos const& pos0
) {
   return nucleotide_match(sm, pos0, sm.gl_segments().front());
}

/**@brief
*******************************************************************************/
VDJML_DECL void merge(Segment_match& sm1, Segment_match const& sm2);

/**@brief
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Segment_match const& sm,
         Results_meta const& rm,
         const unsigned version = VDJML_CURRENT_VERSION
);

}//namespace vdjml
#endif /* SEGMENT_MATCH_HPP_ */
