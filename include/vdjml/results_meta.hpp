/** @file "/vdjml/include/vdjml/results_meta.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013-4 @author Mikhail K Levin
*******************************************************************************/
#ifndef RESULTS_META_HPP_
#define RESULTS_META_HPP_
#include "vdjml/aligner_map.hpp"
#include "vdjml/config.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/gene_region_type_map.hpp"
#include "vdjml/germline_db_map.hpp"
#include "vdjml/germline_segment_map.hpp"
#include "vdjml/num_system_map.hpp"
#include "vdjml/vdjml_current_version.hpp"

namespace vdjml{
class Xml_writer;

namespace detail{class Proto_results_meta;}

/**@brief Metadata for a collection of alignment results of sequencing reads
*******************************************************************************/
class VDJML_DECL Results_meta {
   friend class detail::Proto_results_meta;

public:
   struct Err : public base_exception {};

   Aligner_info const& operator[](const Aligner_id id) const {return am_[id];}
   Gl_db_info const& operator[](const Gl_db_id id) const {return gm_[id];}
   std::string const& operator[](const Numsys_id id) const {return nsm_[id];}
   Gl_segment_info const& operator[](const Gl_seg_id id) const {return gsm_[id];}
   std::string const& operator[](const Region_id id) const {return grm_[id];}

   Aligner_id insert(Aligner_info const& al) {return am_.insert(al);}
   Gl_db_id insert(Gl_db_info const& gl_db) {return gm_.insert(gl_db);}
   Gl_seg_id insert(Gl_segment_info const& gls) {return gsm_.insert(gls);}

   Aligner_map const & aligner_map() const {return am_;}
   Aligner_map       & aligner_map() {return am_;}

   Germline_db_map const   & gl_db_map() const {return gm_;}
   Germline_db_map         & gl_db_map() {return gm_;}

   Num_system_map const & num_system_map() const {return nsm_;}
   Num_system_map       & num_system_map() {return nsm_;}

   Gl_segment_map const & gl_segment_map() const {return gsm_;}
   Gl_segment_map       & gl_segment_map() {return gsm_;}

   Gene_region_map const & gene_region_map() const {return grm_;}
   Gene_region_map       & gene_region_map() {return grm_;}

private:
   Aligner_map am_;
   Germline_db_map gm_;
   Num_system_map nsm_;
   Gl_segment_map gsm_;
   Gene_region_map grm_;
};

/**
@param xw XML writer
@param rm metadata
@param version format version
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Results_meta const& rm,
         const unsigned version = VDJML_CURRENT_VERSION
);

}//namespace vdjml
#endif /* RESULTS_META_HPP_ */
