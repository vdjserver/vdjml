/** @file "/vdjml/include/vdjml/vdjml_writer.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_WRITER_HPP_
#define VDJML_WRITER_HPP_
#include <fstream>
#include <ostream>
#include <string>
#include "boost/iostreams/filtering_streambuf.hpp"
#include "boost/noncopyable.hpp"
#include "boost/scoped_ptr.hpp"
#include "vdjml/compression.hpp"
#include "vdjml/config.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/vdjml_current_version.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml{
class Read_result;
class Results_meta;

/**@brief Incrementally serialize VDJ alignment results
*******************************************************************************/
class VDJML_DECL Vdjml_writer : boost::noncopyable {
   typedef boost::scoped_ptr<std::ostream> os_ptr;
   typedef boost::scoped_ptr<std::ofstream> ofs_ptr;
   typedef boost::iostreams::filtering_ostreambuf fosb_t;
   typedef boost::scoped_ptr<fosb_t> fosb_ptr;

   static std::ofstream* make_ofstream(
            std::string const& path,
            Compression& compr
   );

   static fosb_t* make_fosb(const Compression compr, std::ostream& os);

public:
   struct Err : public base_exception {};

   /** write to stream */
   Vdjml_writer(
            std::ostream& os,
            Results_meta const& rm,
            Compression compr = Uncompressed,
            const unsigned version = VDJML_CURRENT_VERSION,
            Xml_writer_options const& opts = Xml_writer_options()
   )
   : rm_(rm),
     ofs_(0),
     fosb_(make_fosb(compr, os)),
     os_(fosb_ ? new std::ostream(&(*fosb_)) : 0),
     xw_(os_ ? *os_ : os, opts),
     version_(version),
     need_init_(true)
   {}

   /** write to file */
   Vdjml_writer(
            std::string const& path,
            Results_meta const& rm,
            Compression compr = Unknown_compression,
            const unsigned version = VDJML_CURRENT_VERSION,
            Xml_writer_options const& opts = Xml_writer_options()
   )
   : rm_(rm),
     ofs_(make_ofstream(path, compr)),
     fosb_(make_fosb(compr, *ofs_)),
     os_(fosb_ ? new std::ostream(&(*fosb_)) : 0),
     xw_(os_ ? *os_ : *ofs_, opts),
     version_(version),
     need_init_(true)
   {}

   void operator() (Read_result const& rr);

   void close() {xw_.close_stream();}

private:
   Results_meta const& rm_;
   ofs_ptr ofs_;
   fosb_ptr fosb_;
   os_ptr os_;
   Xml_writer xw_;
   unsigned version_;
   bool need_init_;

   void init_xml();
};

}//namespace vdjml
#endif /* VDJML_WRITER_HPP_ */
