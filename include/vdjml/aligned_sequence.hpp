/** @file "/vdjml/include/vdjml/aligned_sequence.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef ALIGNED_SEQUENCE_HPP_
#define ALIGNED_SEQUENCE_HPP_
#include "boost/utility/string_ref.hpp"

namespace vdjml{

namespace seq{
enum Seq { Read = 0, GL = 1 };
}//namespace seq

namespace detail{

/**@brief
*******************************************************************************/
class Aligned {
public:
   Aligned(const seq::Seq k) : kind_(k) {}
   seq::Seq kind() const {return kind_;}
   bool is_gl() const {return kind_ == seq::GL;}
private:
   seq::Seq kind_;
};

}//namespace detail

/**@brief Wrapper to indicate whether position is relative read or germline
sequence
*******************************************************************************/
struct Aligned_pos : public detail::Aligned {
   BOOST_STATIC_CONSTANT(std::size_t, unset = static_cast<std::size_t>(-1));

   static Aligned_pos read(const std::size_t pos) {
      return Aligned_pos(seq::Read, pos);
   }

   static Aligned_pos gl(const std::size_t pos) {
      return Aligned_pos(seq::GL, pos);
   }

   Aligned_pos() : Aligned(seq::Read), pos_(unset) {}
   Aligned_pos(const seq::Seq k, const std::size_t pos) : Aligned(k), pos_(pos) {}
   std::size_t pos() const {return pos_;}
   bool is_set() const {return pos_ != unset;}

   std::size_t pos_;
};

/**@brief Wrapper to indicate whether sequence is a read or germline
*******************************************************************************/
struct Aligned_seq : public detail::Aligned {
   static Aligned_seq read(const boost::string_ref s) {
      return Aligned_seq(seq::Read, s);
   }

   static Aligned_seq gl(const boost::string_ref s) {
      return Aligned_seq(seq::GL, s);
   }

   Aligned_seq()
   : Aligned(seq::Read), seq_() {}

   Aligned_seq(const seq::Seq k, const boost::string_ref s)
   : Aligned(k), seq_(s) {}

   boost::string_ref seq() const {return seq_;}

   boost::string_ref seq_;
};

}//namespace vdjml
#endif /* ALIGNED_SEQUENCE_HPP_ */
