/** @file "/vdjml/include/vdjml/gl_segment_match.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef GL_SEGMENT_MATCH_HPP_
#define GL_SEGMENT_MATCH_HPP_
#include "vdjml/config.hpp"
#include "vdjml/detail/comparison_operators_macro.hpp"
#include "vdjml/interval.hpp"
#include "vdjml/object_ids.hpp"
#include "vdjml/vdjml_current_version.hpp"

namespace vdjml{
class Xml_writer;
class Results_meta;
class Segment_match;

/**@brief Alignment to germline segment
*******************************************************************************/
class Gl_segment_match {
   friend class Segment_match;
public:
   Gl_segment_match(
            const Numsys_id numsys,
            const Aligner_id aligner,
            const Gl_seg_id germline_segment,
            const unsigned pos0
   )
   : id_(),
     num_system_(numsys),
     aligner_(aligner),
     gl_segment_(germline_segment),
     pos0_(pos0)
   {}

   bool operator==(Gl_segment_match const& gsm) const {
      return
               num_system_ == gsm.num_system_ &&
               aligner_ == gsm.aligner_ &&
               gl_segment_ == gsm.gl_segment_ &&
               pos0_ == gsm.pos0_
               ;
   }

   bool operator<(Gl_segment_match const& gsm) const {
      if( num_system_ < gsm.num_system_ ) return true;
      if( gsm.num_system_ < num_system_ ) return false;
      if( aligner_ < gsm.aligner_ ) return true;
      if( gsm.aligner_ < aligner_ ) return false;
      if( gl_segment_ < gsm.gl_segment_ ) return true;
      if( gsm.gl_segment_ < gl_segment_ ) return false;
      return pos0_ < gsm.pos0_;
   }

   VDJML_COMPARISON_OPERATOR_MEMBERS(Gl_segment_match)

   Gl_seg_match_id id() const {return id_;}
   Numsys_id num_system() const {return num_system_;}
   Aligner_id aligner() const {return aligner_;}
   Gl_seg_id gl_segment() const {return gl_segment_;}
   unsigned gl_position() const {return pos0_;}

private:
   Gl_seg_match_id id_;
   Numsys_id num_system_;
   Aligner_id aligner_;
   Gl_seg_id gl_segment_;
   unsigned pos0_;
};

/**@brief
*******************************************************************************/
VDJML_DECL std::string const&
segment_name(Gl_segment_match const& gsm, Results_meta const& rm);

/**@brief
*******************************************************************************/
VDJML_DECL std::string const&
numbering_system(Gl_segment_match const& gsm, Results_meta const& rm);

/**@brief
*******************************************************************************/
VDJML_DECL char
segment_type(Gl_segment_match const& gsm, Results_meta const& rm);

/**@brief
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Gl_segment_match const& gsm,
         Results_meta const& rm,
         const unsigned version = VDJML_CURRENT_VERSION
);

}//namespace vdjml
#endif /* GL_SEGMENT_MATCH_HPP_ */
