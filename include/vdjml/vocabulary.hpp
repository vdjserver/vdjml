/** @file "/vdjml/include/vdjml/vocabulary.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VOCABULARY_HPP_
#define VOCABULARY_HPP_
#include "boost/preprocessor/stringize.hpp"
#include "vdjml/vdjml_current_version.hpp"


#define VDJML_STATIC_STRING_METHOD(name, val)                                  \
   static std::string const& name() {                                          \
      static const std::string s = val; return s;                              \
   }                                                                           \
/* */

#define VDJML_STATIC_STRING_METHOD_ID(elem)                                    \
   VDJML_STATIC_STRING_METHOD(id_, BOOST_PP_STRINGIZE(elem))                   \
/* */

#define VDJML_TAG_OPEN(elem)                                                   \
struct elem{                                                                   \
   VDJML_STATIC_STRING_METHOD_ID(elem)                                         \
/* */

#define VDJML_TAG(elem)                                                        \
VDJML_TAG_OPEN(elem)                                                           \
};                                                                             \
/* */

namespace vdjml{ namespace vocab{

/**@brief
*******************************************************************************/
template<unsigned V = VDJML_CURRENT_VERSION> struct vdjml;

/**@brief
*******************************************************************************/
template<>
struct vdjml<1000> {
   VDJML_STATIC_STRING_METHOD_ID(vdjml)
   VDJML_STATIC_STRING_METHOD(namespace_iri, "http://vdjserver.org/vdjml/xsd/1/")
   VDJML_TAG(version)
   VDJML_TAG(num_system)

   struct range {
      VDJML_TAG(pos0)
      VDJML_TAG(len)
   };

   struct match_metrics {
      VDJML_TAG(identity)
      VDJML_TAG(score)
      VDJML_TAG(insertions)
      VDJML_TAG(deletions)
      VDJML_TAG(substitutions)
      VDJML_TAG(stop_codon)
      VDJML_TAG(mutated_invariant)
      VDJML_TAG(inverted)
      VDJML_TAG(out_frame_indel)
      VDJML_TAG(out_frame_vdj)
   };

   VDJML_TAG_OPEN(meta)
      VDJML_TAG_OPEN(generator)
         VDJML_TAG(name)
         VDJML_TAG(version)
         VDJML_TAG(time_gmt)
      };

      VDJML_TAG_OPEN(aligner)
         VDJML_TAG(aligner_id)
         VDJML_TAG(name)
         VDJML_TAG(version)
         VDJML_TAG(run_id)
         VDJML_TAG(parameters)
         VDJML_TAG(uri)
      };

      VDJML_TAG_OPEN(germline_db)
         VDJML_TAG(gl_db_id)
         VDJML_TAG(name)
         VDJML_TAG(version)
         VDJML_TAG(species)
         VDJML_TAG(uri)
      };
   };

   VDJML_TAG_OPEN(read_results)
      VDJML_TAG_OPEN(read)
         VDJML_TAG(read_id)
         VDJML_TAG_OPEN(alignment)
            struct segment_match : public match_metrics {
               VDJML_STATIC_STRING_METHOD_ID(segment_match)
               VDJML_TAG(segment_match_id)
               VDJML_TAG(read_pos0)
               VDJML_TAG(read_len)
               VDJML_TAG(gl_len)
               VDJML_TAG(btop)
               VDJML_TAG_OPEN(gl_seg_match)
                  VDJML_TAG(gl_seg_match_id)
                  VDJML_TAG(gl_pos0)
                  typedef num_system num_system_t;
                  VDJML_TAG(name)
                  typedef meta::germline_db::gl_db_id gl_db_id;
                  VDJML_TAG(type)
                  typedef meta::aligner::aligner_id aligner_id;
               };
               VDJML_TAG_OPEN(aa_substitution)
                  VDJML_TAG(read_pos0)
                  VDJML_TAG(read_aa)
                  VDJML_TAG(gl_aa)
               };
            };
            VDJML_TAG_OPEN(combination)
               VDJML_TAG(segments)
               struct region : public match_metrics {
                  VDJML_STATIC_STRING_METHOD_ID(region)
                  VDJML_TAG(name)
                  typedef num_system num_system_t;
                  typedef meta::aligner::aligner_id aligner_id;
                  VDJML_TAG(read_pos0)
                  VDJML_TAG(read_len)
               };
            };
         };
      };
   };
};

}//namespace vocab
}//namespace vdjml
#endif /* VOCABULARY_HPP_ */
