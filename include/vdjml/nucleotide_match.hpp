/** @file "/vdjml/include/vdjml/nucleotide_match.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef NUCLEOTIDE_MATCH_HPP_
#define NUCLEOTIDE_MATCH_HPP_
#include <iosfwd>
#include "boost/array.hpp"
#include "boost/assert.hpp"
#include "vdjml/config.hpp"
#include "vdjml/aligned_sequence.hpp"
#include "vdjml/nucleotide_index.hpp"

namespace vdjml{

/**@brief Information about two aligned nucleotides
*******************************************************************************/
struct VDJML_DECL Nucleotide_match {
   Nucleotide_match();

   Nucleotide_match(
            boost::array<std::size_t,2> const& pos,
            boost::array<char,2> const& chars
   )
   : pos_(pos), chars_(chars)
   {}

   bool is_gap(const seq::Seq s) const {
      return chars_[s] == nucleotide::Special::Gap;
   }

   std::size_t read_pos() const {return pos_[seq::Read];}
   std::size_t gl_pos() const {return pos_[seq::GL];}
   char read_char() const {return chars_[seq::Read];}
   char gl_char() const {return chars_[seq::GL];}

   bool is_insertion() const {
      return chars_[seq::GL] == nucleotide::Special::Gap;
   }

   bool is_deletion() const {
      return chars_[seq::Read] == nucleotide::Special::Gap;
   }

   bool is_match() const {
      return chars_[seq::Read] == chars_[seq::GL];
   }

   boost::array<std::size_t,2> pos_;
   boost::array<char,2> chars_;
};

/**
*******************************************************************************/
template<class ChT, class Tr> inline std::basic_ostream<ChT,Tr>& operator<<(
      std::basic_ostream<ChT,Tr>& os,
      Nucleotide_match const& m
) {
    return
             os
             << m.read_pos()
             << m.read_char()
             << m.gl_char()
             << m.gl_pos()
             ;
}

}//namespace vdjml
#endif /* NUCLEOTIDE_MATCH_HPP_ */
