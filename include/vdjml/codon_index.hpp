/** @file "/vdjml/include/vdjml/codon_index.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef CODON_INDEX_HPP_
#define CODON_INDEX_HPP_
#include <string>
#include "boost/array.hpp"
#include "boost/preprocessor/cat.hpp"
#include "boost/preprocessor/seq/elem.hpp"
#include "boost/preprocessor/seq/enum.hpp"
#include "boost/preprocessor/seq/for_each_product.hpp"

#include "vdjml/aa_index.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/nucleotide_index.hpp"

#define VDJML_MAKE_CODON_NAME_(p) BOOST_PP_CAT(                                \
         BOOST_PP_SEQ_ELEM(0, BOOST_PP_SEQ_ELEM(0, p)),                        \
         BOOST_PP_CAT(                                                         \
                  BOOST_PP_SEQ_ELEM(0, BOOST_PP_SEQ_ELEM(1, p)),               \
                  BOOST_PP_SEQ_ELEM(0, BOOST_PP_SEQ_ELEM(2, p))                \
                  )                                                            \
         )                                                                     \
/* */

#define VDJML_COMPUTE_CODON_SHIFT 5

//define as macro, not as function, to use result as compile-time value in C++98
#define VDJML_COMPUTE_CODON_VALUE(n1, n2, n3)                                  \
   (                                                                           \
            n1 << (VDJML_COMPUTE_CODON_SHIFT*2) |                              \
            n2 << VDJML_COMPUTE_CODON_SHIFT |                                  \
            n3                                                                 \
   )                                                                           \
/* */

#define VDJML_MAKE_CODON_VALUE_(p)                                             \
   VDJML_COMPUTE_CODON_VALUE(                                                  \
      BOOST_PP_SEQ_ELEM(1, BOOST_PP_SEQ_ELEM(0, p)),                           \
      BOOST_PP_SEQ_ELEM(1, BOOST_PP_SEQ_ELEM(1, p)),                           \
      BOOST_PP_SEQ_ELEM(1, BOOST_PP_SEQ_ELEM(2, p))                            \
   )                                                                           \
/* */

#define VDJML_MAKE_CODON_MACRO_(r, p)                                          \
      VDJML_MAKE_CODON_NAME_(p) = VDJML_MAKE_CODON_VALUE_(p),                  \
                                                                               \
/* */

/** @brief generate enum for all amino acid codons from a nucleotide enum

   Each enum value is formed by combining three nucleotide enum values.
   Nucleotide enum may include wildcards and other values.
   All combinations of nucleotide enum values should be supported.
   @param name enum name
   @param nucs sequence of nucleotide letters and values
   @param max maximal value of nucleotide enum; used to define largest possible
   codon value, to ensure that codon enum supports all possible values
 */
#define VDJML_MAKE_CODON_ENUM(name, nucs, max) enum name {                     \
         BOOST_PP_SEQ_FOR_EACH_PRODUCT(                                        \
                  VDJML_MAKE_CODON_MACRO_, (nucs)(nucs)(nucs)                  \
         )                                                                     \
         max_value_ = VDJML_COMPUTE_CODON_VALUE(max, max, max)                 \
}                                                                              \
/* */

namespace vdjml{ namespace codon{

/**@brief
*******************************************************************************/
VDJML_MAKE_CODON_ENUM(
         Codon,
         ((A) (::vdjml::nucleotide::Adenine))
         ((C) (::vdjml::nucleotide::Cytosine))
         ((G) (::vdjml::nucleotide::Guanine))
         ((T) (::vdjml::nucleotide::Thymine)),
         ::vdjml::nucleotide::Unknown
);

#undef VDJML_MAKE_CODON_NAME_
#undef VDJML_MAKE_CODON_VALUE_
#undef VDJML_MAKE_CODON_MACRO_
#undef VDJML_MAKE_CODON_ENUM

/**@brief
*******************************************************************************/
enum Nuc_index { One = 0, Two = 1, Three = 2 };

/**@brief
*******************************************************************************/
inline boost::array<vdjml::nucleotide::Nuc,3> nucleotides(const Codon cdn) {
   static const unsigned Mask = ~(~0U << VDJML_COMPUTE_CODON_SHIFT);
   const boost::array<vdjml::nucleotide::Nuc,3> a =
   {{
            (vdjml::nucleotide::Nuc)((cdn >> (VDJML_COMPUTE_CODON_SHIFT * 2)) & Mask),
            (vdjml::nucleotide::Nuc)((cdn >> VDJML_COMPUTE_CODON_SHIFT) & Mask),
            (vdjml::nucleotide::Nuc)(cdn & Mask)
   }};
   return a;
}

/**@brief
*******************************************************************************/
inline std::string to_string(const Codon cdn) {
   const boost::array<vdjml::nucleotide::Nuc,3> a = nucleotides(cdn);
   std::string s(3, 'X');
   s[0] = to_capital(a[0]);
   s[1] = to_capital(a[1]);
   s[2] = to_capital(a[2]);
   return s;
}

/**@brief
*******************************************************************************/
inline vdjml::aminoacid::Aa translate(const Codon cdn) {
   using namespace aminoacid;
   switch(cdn) {
   case GCA:
   case GCC:
   case GCG:
   case GCT:
      return Ala;
   case AGA:
   case AGG:
   case CGA:
   case CGC:
   case CGG:
   case CGT:
      return Arg;
   case AAC:
   case AAT:
      return Asn;
   case GAC:
   case GAT:
      return Asp;
   case TGC:
   case TGT:
      return Cys;
   case CAA:
   case CAG:
      return Gln;
   case GAA:
   case GAG:
      return Glu;
   case GGA:
   case GGC:
   case GGG:
   case GGT:
      return Gly;
   case CAC:
   case CAT:
      return His;
   case ATA:
   case ATC:
   case ATT:
      return Ile;
   case CTA:
   case CTC:
   case CTG:
   case CTT:
   case TTA:
   case TTG:
      return Leu;
   case AAA:
   case AAG:
      return Lys;
   case ATG:
      return Met;
   case TTC:
   case TTT:
      return Phe;
   case CCA:
   case CCC:
   case CCG:
   case CCT:
      return Pro;
   case AGC:
   case AGT:
   case TCA:
   case TCC:
   case TCG:
   case TCT:
      return Ser;
   case ACA:
   case ACC:
   case ACG:
   case ACT:
      return Thr;
   case TGG:
      return Trp;
   case TAC:
   case TAT:
      return Tyr;
   case GTA:
   case GTC:
   case GTG:
   case GTT:
      return Val;
   case TAA:
   case TAG:
   case TGA:
      return Stop;
   default:
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("cannot translate")
               << base_exception::str1_t(sanitize(to_string(cdn)))
      );
   }
   return Stop; // never comes here
}

}//namespace codon

namespace nucleotide{

/**@brief
*******************************************************************************/
inline vdjml::codon::Codon codon_index(
         const Nuc n1,
         const Nuc n2,
         const Nuc n3
) {
   return (vdjml::codon::Codon)VDJML_COMPUTE_CODON_VALUE(n1, n2, n3);
}

/**@brief
*******************************************************************************/
inline bool is_translatable(
         const Nuc n1,
         const Nuc n2,
         const Nuc n3
) {
   return
            is_translatable(n1) &&
            is_translatable(n2) &&
            is_translatable(n3)
            ;
}

/**@brief
*******************************************************************************/
inline aminoacid::Aa translate(
         const Nuc n1,
         const Nuc n2,
         const Nuc n3
) {
   return translate(codon_index(n1, n2, n3));
}

}//namespace nucleotide

namespace codon{

/**@brief
*******************************************************************************/
inline bool is_translatable(const Codon cdn) {
   const boost::array<vdjml::nucleotide::Nuc,3> a = nucleotides(cdn);
   return is_translatable(a[0], a[1], a[2]);
}

/**@brief
*******************************************************************************/
inline bool has_gaps(const Codon cdn) {
   const boost::array<vdjml::nucleotide::Nuc,3> a = nucleotides(cdn);
   return
            a[0] == nucleotide::Gap ||
            a[1] == nucleotide::Gap ||
            a[2] == nucleotide::Gap
            ;
}

}//namespace codon

}//namespace vdjml
#endif /* CODON_INDEX_HPP_ */
