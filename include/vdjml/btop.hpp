/** @file "/vdjml/include/vdjml/btop.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef BTOP_HPP_
#define BTOP_HPP_
#include <string>
#include <iosfwd>
#include "boost/array.hpp"
#include "vdjml/aligned_sequence.hpp"
#include "vdjml/config.hpp"
#include "vdjml/detail/comparison_operators_macro.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/length_type.hpp"
#include "vdjml/nucleotide_index.hpp"
#include "vdjml/nucleotide_match.hpp"

namespace vdjml{
class Mismatch_iter;

/**@brief Store BTOP (Blast traceback operations)
*******************************************************************************/
class VDJML_DECL Btop {
   friend class Mismatch_iter;
public:
   static const std::size_t unset;
   struct Err : public base_exception {};

   explicit Btop(std::string const& val);

   std::string str() const {return val_;}
   bool empty() const {return val_.empty();}
   bool operator==(Btop const& bt) const {return val_ == bt.val_;}
   bool operator<(Btop const& bt) const {return val_ < bt.val_;}
   VDJML_COMPARISON_OPERATOR_MEMBERS(Btop)

private:
   std::string val_;

   static void validate(std::string const&);
};

/**
@param btop BTOP structure
@param seq read or germline sequence
@return true if btop matches seq by length and content
*******************************************************************************/
VDJML_DECL bool check(Btop const& btop, Aligned_seq const& seq);

/**
@param btop BTOP structure
@param seq read or germline sequence
@throw Btop::Err if seq has incorrect size, invalid or mismatching nucleotide
character
*******************************************************************************/
VDJML_DECL void validate(Btop const& btop, Aligned_seq const& seq);

/**
*******************************************************************************/
template<class ChT, class Tr> inline
std::basic_ostream<ChT,Tr>& operator<<(
      std::basic_ostream<ChT,Tr>& os,
      Btop const& btop
) {
   return os << btop.str();
}

/**@brief
*******************************************************************************/
struct VDJML_DECL Btop_stats {
   explicit Btop_stats(Btop const& btop);

   length_t matches() const {
      return read_len_ - insertions_ - substitutions_;
   }

   length_t substitutions_;
   length_t insertions_;
   length_t deletions_;
   length_t read_len_;
   length_t gl_len_;
};

/**
@param btop BTOP structure
@param pos position in query or reference sequence
@param match character to indicate a match
@return aligned positions and nucleotides
*******************************************************************************/
VDJML_DECL
Nucleotide_match nucleotide_match(
         Btop const& btop,
         Aligned_pos const& pos,
         const char match = nucleotide::Special::Unknown
);

/**
@param btop BTOP structure
@param pos position in query or reference sequence
@param seq read or germline sequence
@return aligned positions and nucleotides
*******************************************************************************/
VDJML_DECL
Nucleotide_match nucleotide_match(
         Btop const& btop,
         Aligned_pos const& pos,
         Aligned_seq const& seq
);

/**
*******************************************************************************/
struct Sequence_match {
   boost::array<std::size_t,2> start_;
   boost::array<std::size_t,2> end_;
   boost::array<std::string,2> seq_;
};


/**
*******************************************************************************/
template<class ChT, class Tr> inline
std::basic_ostream<ChT,Tr>& operator<<(
      std::basic_ostream<ChT,Tr>& os,
      Sequence_match const& sm
) {
   os
   << sm.start_[0] << '\t' << sm.seq_[0] << '\t' << sm.end_[0] << '\n'
   << sm.start_[1] << '\t' << sm.seq_[1] << '\t' << sm.end_[1] << '\n'
   ;
   return os;
}


/**
@param btop BTOP structure
@param start position in read or germline sequence to start showing alignment
@param end position in read or germline sequence up to which to show alignment
@param match character to indicate a match
*******************************************************************************/
VDJML_DECL Sequence_match sequence_match(
         Btop const& btop,
         Aligned_pos const& start,
         Aligned_pos const& end,
         const char match = nucleotide::Special::Unknown
);

/**
@param btop BTOP structure
@param match character to indicate a match
*******************************************************************************/
inline Sequence_match sequence_match(
         Btop const& btop,
         const char match = nucleotide::Special::Unknown
) {
   return sequence_match(btop, Aligned_pos(), Aligned_pos(), match);
}

/**
@param btop BTOP structure
@param start position in read or germline sequence to start showing alignment
@param end position in read or germline sequence up to which to show alignment
@param seq read or germline sequence
@return structure containing aligned sequences from start to end and their
start and end indices
@throws Btop::Err if there is a mismatch between btop and seq
*******************************************************************************/
VDJML_DECL Sequence_match sequence_match(
         Btop const& btop,
         Aligned_pos const& start,
         Aligned_pos const& end,
         Aligned_seq const& seq
);

/**
@param btop BTOP structure
@param seq read or germline sequence
*******************************************************************************/
inline Sequence_match sequence_match(
         Btop const& btop,
         Aligned_seq const& seq
) {
   return sequence_match(
            btop,
            Aligned_pos(),
            Aligned_pos(),
            seq
   );
}

/**
@param btop BTOP structure
@param start read or germline frame position
@param seq read or germline sequence
@return true if no stop codons found starting from start position
*******************************************************************************/
VDJML_DECL bool is_open_frame(
         Btop const& btop,
         Aligned_pos const& start,
         Aligned_seq const& seq
);

/**
@param btop BTOP structure
@param start read or germline frame position
@param seq read or germline sequence
@return 0,1, or 2 for open frame found or -1 if no open frame is found
*******************************************************************************/
VDJML_DECL int find_open_frame(
         Btop const& btop,
         Aligned_pos const& start,
         Aligned_seq const& seq
);

}//namespace vdjml
#endif /* BTOP_HPP_ */
