/** @file "/vdjml/include/vdjml/segment_combination.hpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef SEGMENT_COMBINATION_HPP_
#define SEGMENT_COMBINATION_HPP_
#include "vdjml/config.hpp"
#include "vdjml/detail/comparison_operators_macro.hpp"
#include "vdjml/detail/vector_set.hpp"
#include "vdjml/gene_region.hpp"
#include "vdjml/object_ids.hpp"
#include "vdjml/vdjml_current_version.hpp"
#include <set>
namespace vdjml{
class Xml_writer;
class Results_meta;

namespace detail{
class Sb_region;
class Sb_combination;
}//namespace detail

/**@brief
*******************************************************************************/
class Segment_combination {
   friend class detail::Sb_combination;

   explicit Segment_combination()
   : smv_(), grv_()
   {}

public:
   typedef detail::Vector_set<Seg_match_id> segment_set;
   typedef detail::Vector_set<Gene_region> gene_region_set;

   explicit Segment_combination(
            const Seg_match_id id1,
            const Seg_match_id id2 = Seg_match_id(),
            const Seg_match_id id3 = Seg_match_id(),
            const Seg_match_id id4 = Seg_match_id(),
            const Seg_match_id id5 = Seg_match_id()
   )
   : smv_()
   {
      if( id1 ) smv_.insert(id1);
      if( id2 ) smv_.insert(id2);
      if( id3 ) smv_.insert(id3);
      if( id4 ) smv_.insert(id4);
      if( id5 ) smv_.insert(id5);
   }

   segment_set const& segments() const {return smv_;}
   gene_region_set const& regions() const {return grv_;}
   void insert(const Seg_match_id sm_id) {smv_.insert(sm_id);}
   void insert(Gene_region const& gr) {grv_.insert(gr);}

   bool operator<(Segment_combination const& sc) const {
      return smv_ < sc.smv_;
   }

   bool operator==(Segment_combination const& sc) const {
      return smv_ == sc.smv_;
   }

   VDJML_COMPARISON_OPERATOR_MEMBERS(Segment_combination)

private:
   segment_set smv_;
   gene_region_set grv_;
};

/**@brief
*******************************************************************************/
VDJML_DECL void write(
         Xml_writer& xw,
         Segment_combination const& sc,
         Results_meta const& rm,
         const unsigned version = VDJML_CURRENT_VERSION
);

}//namespace vdjml
#endif /* SEGMENT_COMBINATION_HPP_ */
