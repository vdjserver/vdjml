/** @file "/vdjml/include/vdjml/nucleotide_index.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013-4 @author Mikhail K Levin
*******************************************************************************/
#ifndef NUCLEOTIDE_INDEX_HPP_
#define NUCLEOTIDE_INDEX_HPP_
#include "boost/assert.hpp"
#include "vdjml/exception.hpp"

namespace vdjml{ namespace nucleotide{

/**@brief
*******************************************************************************/
struct Special {
   BOOST_STATIC_CONSTANT(char, Gap = '-');
   BOOST_STATIC_CONSTANT(char, Unknown = '.');
};

/**@brief
*******************************************************************************/
enum Nuc {
   Adenine,
   Cytosine,
   Guanine,
   Thymine,
   Any,
   Uracil,     ///< U
   Purine,     ///< R : A, G
   Pyrimidine, ///< Y : C, T, U
   Ketone,     ///< K : G, T, U
   Amine,      ///< M : A, C
   Strong,     ///< S : C, G
   Weak,       ///< W : A, T, U
   not_A,      ///< B : C G T
   not_C,      ///< D : A G T
   not_G,      ///< H : A C T
   not_T,      ///< V : A C G
   Gap,
   Unknown
};

/**@brief
*******************************************************************************/
inline Nuc complement(const Nuc n) {
   switch (n) {
   case Adenine: return Thymine;
   case Cytosine: return Guanine;
   case Guanine: return Cytosine;
   case Thymine: return Adenine;
   case Uracil: return Adenine;
   case Purine: return Pyrimidine;
   case Pyrimidine: return Purine;
   case Ketone: return Amine;
   case Amine: return Ketone;
   case Strong: return Weak;
   case Weak: return Strong;
   case not_A: return not_T;
   case not_C: return not_G;
   case not_G: return not_C;
   case not_T: return not_A;
   case Any: return Any;
   case Unknown: return Unknown;
   default: BOOST_THROW_EXCEPTION(
            base_exception()
            << base_exception::msg_t("invalid nucleotide")
            << base_exception::int1_t(n)
   );
   }
   return Unknown; //never comes to this
}

/**@brief
*******************************************************************************/
inline bool is_translatable(const Nuc n) {
   switch (n) {
   case Adenine:
   case Cytosine:
   case Guanine:
   case Thymine:
      return true;
   default:
      return false;
   }
}

/**@brief
*******************************************************************************/
inline char to_capital(const Nuc n) {
   switch (n) {
   case Adenine: return 'A';
   case Cytosine: return 'C';
   case Guanine: return 'G';
   case Thymine: return 'T';
   case Uracil: return 'U';
   case Purine: return 'R';
   case Pyrimidine: return 'Y';
   case Ketone: return 'K';
   case Amine: return 'M';
   case Strong: return 'S';
   case Weak: return 'W';
   case not_A: return 'B';
   case not_C: return 'D';
   case not_G: return 'H';
   case not_T: return 'V';
   case Any: return 'N';
   case Gap: return Special::Gap;
   case Unknown: return Special::Unknown;
   default: BOOST_THROW_EXCEPTION(
            base_exception()
            << base_exception::msg_t("invalid nucleotide")
            << base_exception::int1_t(n)
   );
   }
   return 'X'; //never comes to this
}

/**@brief
*******************************************************************************/
inline char to_small(const Nuc n) {
   switch (n) {
   case Adenine: return 'a';
   case Cytosine: return 'c';
   case Guanine: return 'g';
   case Thymine: return 't';
   case Uracil: return 'u';
   case Purine: return 'r';
   case Pyrimidine: return 'y';
   case Ketone: return 'k';
   case Amine: return 'm';
   case Strong: return 's';
   case Weak: return 'w';
   case not_A: return 'b';
   case not_C: return 'd';
   case not_G: return 'h';
   case not_T: return 'v';
   case Any: return 'n';
   case Gap: return Special::Gap;
   case Unknown: return Special::Unknown;
   default: BOOST_THROW_EXCEPTION(
            base_exception()
            << base_exception::msg_t("invalid nucleotide")
            << base_exception::int1_t(n)
   );
   }
   return 'X'; //never comes to this
}

/**@brief
*******************************************************************************/
inline bool is_ambiguous(const Nuc n) {
   switch (n) {
   case Adenine:
   case Cytosine:
   case Guanine:
   case Thymine:
   case Uracil:
      return false;
   default:
      return true;
   }
}

}//namespace nucleotide

/**@brief
*******************************************************************************/
inline bool is_nucleotide(const char c) {
   switch(c) {
   case 'A':
   case 'a':
   case 'C':
   case 'c':
   case 'G':
   case 'g':
   case 'T':
   case 't':
   case 'U':
   case 'u':
   case 'R':
   case 'r':
   case 'Y':
   case 'y':
   case 'K':
   case 'k':
   case 'M':
   case 'm':
   case 'S':
   case 's':
   case 'W':
   case 'w':
   case 'B':
   case 'b':
   case 'D':
   case 'd':
   case 'H':
   case 'h':
   case 'V':
   case 'v':
   case 'N':
   case 'n':
   case nucleotide::Special::Gap:
      return true;
   default:
      return false;
   }
}

/**@brief
*******************************************************************************/
inline nucleotide::Nuc nucleotide_index(const char c) {
   using namespace nucleotide;
   switch (c) {
   case 'A':
   case 'a':
      return Adenine;
   case 'C':
   case 'c':
      return Cytosine;
   case 'G':
   case 'g':
      return Guanine;
   case 'T':
   case 't':
      return Thymine;
   case 'U':
   case 'u':
      return Uracil;
   case 'R':
   case 'r':
      return Purine;
   case 'Y':
   case 'y':
      return Pyrimidine;
   case 'K':
   case 'k':
      return Ketone;
   case 'M':
   case 'm':
      return Amine;
   case 'S':
   case 's':
      return Strong;
   case 'W':
   case 'w':
      return Weak;
   case 'B':
   case 'b':
      return not_A;
   case 'D':
   case 'd':
      return not_C;
   case 'H':
   case 'h':
      return not_G;
   case 'V':
   case 'v':
      return not_T;
   case 'N':
   case 'n':
      return Any;
   case Special::Gap:
      return Gap;
   }
   BOOST_THROW_EXCEPTION(
            base_exception()
            << base_exception::msg_t("invalid character")
            << base_exception::str1_t(sanitize(c))
   );
   return Unknown; //never comes to this
}

/**@brief
*******************************************************************************/
inline bool is_nucleotide_ambiguous(const char c) {
   switch (c) {
   case 'A':
   case 'a':
   case 'C':
   case 'c':
   case 'G':
   case 'g':
   case 'T':
   case 't':
   case 'U':
   case 'u':
      return false;
   case 'R':
   case 'r':
   case 'Y':
   case 'y':
   case 'K':
   case 'k':
   case 'M':
   case 'm':
   case 'S':
   case 's':
   case 'W':
   case 'w':
   case 'B':
   case 'b':
   case 'D':
   case 'd':
   case 'H':
   case 'h':
   case 'V':
   case 'v':
   case 'N':
   case 'n':
      return true;
   }
   BOOST_THROW_EXCEPTION(
            base_exception()
            << base_exception::msg_t("invalid character")
            << base_exception::str1_t(sanitize(c))
   );
   return true; //never comes to this
}

/**@brief
*******************************************************************************/
inline char nucleotide_complement(const char c) {
   switch(c) {
   case 'A': return 'T';
   case 'a': return 't';
   case 'T': return 'A';
   case 't': return 'a';
   case 'C': return 'G';
   case 'c': return 'g';
   case 'G': return 'C';
   case 'g': return 'c';
   case 'U': return 'A';
   case 'u': return 'a';
   case 'R': return 'Y';
   case 'r': return 'y';
   case 'Y': return 'R';
   case 'y': return 'r';
   case 'K': return 'M';
   case 'k': return 'm';
   case 'M': return 'K';
   case 'm': return 'k';
   case 'S': return 'W';
   case 's': return 'w';
   case 'W': return 'S';
   case 'w': return 's';
   case 'N': return 'N';
   case 'n': return 'n';
   default:
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("invalid nucleotide symbol")
               << base_exception::str1_t(sanitize(c))
      );
   }
   return 0; //never comes to this
}

/**@brief
*******************************************************************************/
inline bool is_gap(const char c) {return c == nucleotide::Special::Gap;}

}//namespace vdjml
#endif /* NUCLEOTIDE_INDEX_HPP_ */
