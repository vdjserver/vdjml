/** @file "/vdjml/include/vdjml/codon_iterator.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef CODON_ITERATOR_HPP_
#define CODON_ITERATOR_HPP_
#include <functional>
#include <string>
#include "boost/array.hpp"
#include "boost/assert.hpp"
#include "boost/optional.hpp"
#include "boost/utility/string_ref.hpp"
#include "vdjml/codon_match.hpp"
#include "vdjml/config.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/nucleotide_iterator.hpp"

namespace vdjml{

/**@brief
*******************************************************************************/
class Codon_seq_iter {
public:
   struct Err : public base_exception {};

   explicit Codon_seq_iter(const boost::string_ref seq)
   : seq_(seq), pos_(0)
   {}

   bool at_end() const {return (seq_.size() - pos_) < 3;}

   void next() {
      if( at_end() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("at sequence end")
               << Err::int1_t(pos_)
      );
      pos_ += 3;
   }

   codon::Codon codon() const {
      if( at_end() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("at sequence end")
               << Err::int1_t(pos_)
      );
      return codon_index(
               nucleotide_index(seq_[pos_]),
               nucleotide_index(seq_[pos_ + 1]),
               nucleotide_index(seq_[pos_ + 2])
      );
   }

   std::size_t position() const {return pos_;}

private:
   boost::string_ref seq_;
   std::size_t pos_;
};

/**@brief Accept all nucleotide matches
*******************************************************************************/
class All_positions : public std::unary_function<Nucleotide_match,bool> {
public:
   bool operator() (Nucleotide_match const& nm) const {return true;}
};

/**@brief Reject gaps in specified sequence
*******************************************************************************/
class Skip_seq_gaps : public std::unary_function<Nucleotide_match,bool> {
public:
   explicit Skip_seq_gaps(const seq::Seq in) : in_(in) {}
   bool operator() (Nucleotide_match const& nm) const {return ! nm.is_gap(in_);}

private:
   seq::Seq in_;
};

/**@brief Iterate over codons of aligned sequences

The codons consist of nucleotides accepted by predicate object, Pred
*******************************************************************************/
template<class SeqIn, class Pred> class Aligned_codon_iter {

   typedef Aligned_nuc_iter<SeqIn> nuc_iter_t;
public:
   typedef Codon_match value_type;

   Aligned_codon_iter(
            Btop const& btop,
            Aligned_pos const& start,
            SeqIn const& seq,
            Pred const& pred
   )
   : i_(btop, seq),
     pred_(pred),
     cm_()
   {
      i_.move_to(start);
      next();
   }

   Codon_match const& get() const {return cm_.get();}

   void next() {
      cm_.reset();
      if( i_.at_end() ) return;
      boost::array<Nucleotide_match,3> anm;
      if( ! next<codon::One>(anm) ) return;
      if( ! next<codon::Two>(anm) ) return;
      if( ! next<codon::Three>(anm) ) return;
      cm_.reset(Codon_match(anm));
   }

   bool at_end() const {return ! cm_;}

private:
   nuc_iter_t i_;
   Pred pred_;
   boost::optional<Codon_match> cm_;

   template<codon::Nuc_index N> bool
   next(boost::array<Nucleotide_match,3>& anm) {
      for( ; ! i_.at_end(); i_.next() ) {
         const Nucleotide_match nm = i_.get();
         if( pred_(nm) ) {
            anm[N] = nm;
            i_.next();
            return true;
         }
      }
      return false;
   }
};

typedef Aligned_codon_iter<In_seq_ref, All_positions> codon_iter_sb;
typedef Aligned_codon_iter<In_seq_val, All_positions> codon_iter_svb;
typedef Aligned_codon_iter<Match_char, All_positions> codon_iter_b;
typedef Aligned_codon_iter<In_seq_ref, Skip_seq_gaps> codon_iter_sbsg;
typedef Aligned_codon_iter<In_seq_val, Skip_seq_gaps> codon_iter_svbsg;
typedef Aligned_codon_iter<Match_char, Skip_seq_gaps> codon_iter_bsg;

/**@brief
*******************************************************************************/
inline codon_iter_sb codon_iterator(
   Btop const& btop,
   Aligned_pos const& start,
   Aligned_seq const& seq
) {
   return codon_iter_sb(
      btop,
      start,
      In_seq_ref(seq),
      All_positions()
   );
}

/**@brief
*******************************************************************************/
inline codon_iter_b codon_iterator(
   Btop const& btop,
   Aligned_pos const& start,
   const char match
) {
   return codon_iter_b(
      btop,
      start,
      Match_char(match),
      All_positions()
   );
}

/**@brief
*******************************************************************************/
inline codon_iter_sbsg codon_iterator(
   Btop const& btop,
   Aligned_pos const& start,
   Aligned_seq const& seq,
   const seq::Seq in
) {
   return codon_iter_sbsg(
      btop,
      start,
      In_seq_ref(seq),
      Skip_seq_gaps(in)
   );
}

/**@brief
*******************************************************************************/
inline codon_iter_bsg codon_iterator(
   Btop const& btop,
   Aligned_pos const& start,
   const char match,
   const seq::Seq in
) {
   return codon_iter_bsg(
      btop,
      start,
      Match_char(match),
      Skip_seq_gaps(in)
   );
}

}//namespace vdjml
#endif /* CODON_ITERATOR_HPP_ */
