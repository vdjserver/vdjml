/** @file "/vdjml/include/vdjml/sanitize_string.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef SANITIZE_STRING_HPP_
#define SANITIZE_STRING_HPP_
#include <string>
#include "boost/utility/string_ref.hpp"
#include "vdjml/config.hpp"

namespace vdjml{

/**@brief
*******************************************************************************/
VDJML_DECL std::string sanitize(const char c);

/**@brief
*******************************************************************************/
VDJML_DECL std::string sanitize(std::string const& str);

/**@brief
*******************************************************************************/
VDJML_DECL std::string sanitize(
         const boost::string_ref str,
         const std::size_t max_len
);

}//namespace vdjml
#endif /* SANITIZE_STRING_HPP_ */
