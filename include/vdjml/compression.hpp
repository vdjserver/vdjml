/** @file "/vdjml/include/vdjml/compression.hpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef COMPRESSION_HPP_
#define COMPRESSION_HPP_
#include <string>
#include <iosfwd>
#include "vdjml/config.hpp"

namespace vdjml{

/**@brief 
*******************************************************************************/
enum Compression {
   Uncompressed, Unknown_compression, gzip,
   bzip2, zlib, pkzip, rar, seven_z
};

/**@return compression based on file extension
*******************************************************************************/
VDJML_DECL Compression guess_compression_ext(std::string const& path);

/**@return compression based on file magic number
*******************************************************************************/
VDJML_DECL Compression guess_compression_magic(std::string const& path);

/**@return compression based on stream magic number
*******************************************************************************/
VDJML_DECL Compression guess_compression_magic(std::istream& is);

}//namespace vdjml
#endif /* COMPRESSION_HPP_ */
