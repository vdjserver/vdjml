/** @file "/vdjml/include/vdjml/codon_match.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef CODON_MATCH_HPP_
#define CODON_MATCH_HPP_
#include "boost/array.hpp"
#include "boost/assert.hpp"
#include "vdjml/codon_index.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/aligned_sequence.hpp"
#include "vdjml/nucleotide_match.hpp"

namespace vdjml{

/**@brief Information about two aligned amino acid codons
*******************************************************************************/
class Codon_match {
public:
   struct Err : public base_exception {};

   Codon_match() {}

   explicit Codon_match(boost::array<Nucleotide_match,3> const& anm)
   : anm_(anm)
   {}

   std::size_t position(
            const seq::Seq in,
            const codon::Nuc_index i = codon::One
   ) const {
      return anm_[i].pos_[in];
   }

   std::size_t read_pos(const codon::Nuc_index i = codon::One) const {
      return anm_[i].pos_[seq::Read];
   }

   std::size_t gl_pos(const codon::Nuc_index i = codon::One) const {
      return anm_[i].pos_[seq::GL];
   }

   char read_char(const codon::Nuc_index i) const {
      return anm_[i].chars_[seq::Read];
   }

   char gl_char(const codon::Nuc_index i) const {
      return anm_[i].chars_[seq::GL];
   }

   bool is_contiguous(const seq::Seq in) const {
      return
               position(in, codon::One) + 1 == position(in, codon::Two) &&
               position(in, codon::Two) + 1 == position(in, codon::Three)
               ;
   }

   bool is_contiguous() const {
      return is_contiguous(seq::Read) && is_contiguous(seq::GL);
   }

   nucleotide::Nuc nucleotide(
      const seq::Seq in,
      const codon::Nuc_index i
   ) const {
      return nucleotide_index(anm_[i].chars_[in]);
   }

   codon::Codon codon(const seq::Seq in) const {
      return codon_index(
         nucleotide(in, codon::One),
         nucleotide(in, codon::Two),
         nucleotide(in, codon::Three)
      );
   }

   bool is_translatable(const seq::Seq in) const {
      return
               vdjml::nucleotide::is_translatable(
                  nucleotide(in, codon::One),
                  nucleotide(in, codon::Two),
                  nucleotide(in, codon::Three)

               );
   }

   bool is_translatable() const {
      return is_translatable(seq::Read) && is_translatable(seq::GL);
   }

   aminoacid::Aa translate(const seq::Seq in) const {
      return vdjml::codon::translate(codon(in));
   }

   bool is_match() const {
      return
               anm_[codon::One].is_match() &&
               anm_[codon::Two].is_match() &&
               anm_[codon::Three].is_match()
               ;
   }

   bool is_silent() const {
      return is_match() || (translate(seq::Read) == translate(seq::GL));
   }

private:
   boost::array<Nucleotide_match,3> anm_;
};

}//namespace vdjml
#endif /* CODON_MATCH_HPP_ */
