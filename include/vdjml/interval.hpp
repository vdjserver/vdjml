/** @file "/vdjml/include/vdjml/interval.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef INTERVAL_HPP_
#define INTERVAL_HPP_
#include <limits>
#include <iosfwd>
#include "vdjml/detail/comparison_operators_macro.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/length_type.hpp"

namespace vdjml{

/**@brief
*******************************************************************************/
class Interval {
public:
   struct Err : public base_exception {};
   typedef ::vdjml::length_t length_t;

private:
   Interval();

   Interval(const std::size_t pos, const std::size_t len)
   : pos_(pos), len_(len)
   {
      if( std::numeric_limits<length_t>::max() < pos ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("position out of range")
               << Err::int1_t(pos)
      );
      if( std::numeric_limits<length_t>::max() < len ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("length out of range")
               << Err::int1_t(len)
      );
   }

public:

   static Interval const& unset() {
      static const Interval i(std::numeric_limits<length_t>::max(), 0);
      return i;
   }

   /**
    @param pos0 zero-based interval starting position
    @param len interval length
    */
   static Interval pos0_len(const std::size_t pos0, const std::size_t len) {
      return Interval(pos0, len);
   }

   /**
    @param first0 zero-based interval starting position
    @param last0 zero-based last position of interval
    */
   static Interval first_last0(const std::size_t first0, const std::size_t last0) {
      if( last0 < first0 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("invalid interval: last < first")
               << Err::int1_t(first0)
               << Err::int2_t(last0)
      );

      return Interval(first0, last0 - first0 + 1);
   }

   /**
    @param first1 one-based interval starting position
    @param last1 one-based last position of interval
    */
   static Interval first_last1(const std::size_t first1, const std::size_t last1) {
      if( first1 == 0 || last1 == 0 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("one-based position is expected")
               << Err::int1_t(first1)
               << Err::int2_t(last1)
      );

      if( last1 < first1 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("invalid interval: last < first")
               << Err::int1_t(first1)
               << Err::int2_t(last1)
      );

      return Interval(first1 - 1, last1 - first1 + 1);
   }

   /**@return zero-based start position of interval */
   std::size_t pos0() const {return pos_;}

   /**@return one-based start position of interval */
   std::size_t pos1() const {return pos_ + 1;}

   /**@return zero-based last position of interval */
   std::size_t last0() const {return len_ ? (std::size_t)pos_ + len_ - 1 : pos_;}

   /**@return one-based last position of interval */
   std::size_t last1() const {return len_ ? (std::size_t)pos_ + len_ : pos_;}

   /**@return interval length */
   std::size_t length() const {return len_;}

   bool operator==(Interval const& i) const {
      return pos_ == i.pos_ && len_ == i.len_;
   }

   bool operator<(Interval const& i) const {
      if( pos_ < i.pos_ ) return true;
      if( i.pos_ < pos_ ) return false;
      return len_ < i.len_;
   }

   VDJML_COMPARISON_OPERATOR_MEMBERS(Interval)

private:
   length_t pos_;
   length_t len_;
};

/**
*******************************************************************************/
template<class ChT, class Tr> inline
std::basic_ostream<ChT,Tr>& operator<<(
      std::basic_ostream<ChT,Tr>& os,
      Interval const& i
) {
   return os << "{pos0:" << i.pos0() << " len:" << i.length() << '}';
}

}//namespace vdjml
#endif /* INTERVAL_HPP_ */
