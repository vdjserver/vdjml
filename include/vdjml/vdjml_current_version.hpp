/** @file "/vdjml/include/vdjml/vdjml_current_version.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_CURRENT_VERSION_HPP_
#define VDJML_CURRENT_VERSION_HPP_

#define VDJML_VERSION_MAJOR_MULTIPLIER 1000

#define VDJML_CURRENT_VERSION 1000

#endif /* VDJML_CURRENT_VERSION_HPP_ */
