/** @file "/vdjml/binding/python/gene_region.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/gene_region.hpp"
using vdjml::Gene_region;
using vdjml::Match_metrics;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_gene_region() {

   /**
    * Gene_region
    */
   bp::class_<Gene_region>(
      "Gene_region",
      "information about gene region alignment",
      bp::init<
         const vdjml::Region_id,
         const vdjml::Numsys_id,
         const vdjml::Aligner_id,
         vdjml::Interval const&,
         vdjml::Match_metrics const&
      >(
               (
                        bp::arg("region"),
                        bp::arg("num_system"),
                        bp::arg("aligner"),
                        bp::arg("range"),
                        bp::arg("mm")
               )
      )
   )
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   .def("numbering_system", &Gene_region::numbering_system, "numbering system ID")
   .def("aligner", &Gene_region::aligner, "aligner ID")
   .def("region_type", &Gene_region::region_type, "region ID")
   .def(
            "read_range",
            &Gene_region::read_range,
            return_ref(),
            "read sequence range"
   )
   .def(
            "match_metrics",
            &Gene_region::match_metrics,
            return_ref(),
            "match metrics"
   )
   ;
}
