/** @file "/vdjml/binding/python/read_result.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
#include "boost/python/suite/indexing/vector_indexing_suite.hpp"
namespace bp = boost::python;

#include "vdjml/vdjml_current_version.hpp"
#include "vdjml/read_result.hpp"
using vdjml::Read_result;
using vdjml::Segment_match;
using vdjml::Segment_match_map;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_read_result() {

   /**
    * Read_result
    */
   bp::class_<Read_result, std::auto_ptr<Read_result> >(
      "Read_result",
      "Analysis results for one sequencing read",
      bp::init<std::string>()
   )
   .def("id", &Read_result::id, return_ref(), "read ID string")
   .def(
      "__getitem__",
      static_cast<
         vdjml::Segment_match const& (Read_result::*)(const vdjml::Seg_match_id) const
      >(&Read_result::operator[]),
      bp::return_internal_reference<>(),
      "access segment match"
   )
   .def(
      "segment_matches",
      static_cast<
         vdjml::Segment_match_map const& (Read_result::*)() const
      >(&Read_result::segment_matches),
      bp::return_internal_reference<>(),
      "map of segment matches"
   )
   .def(
      "segment_combinations",
      static_cast<
         Read_result::seg_comb_store const& (Read_result::*)() const
      >(&Read_result::segment_combinations),
      bp::return_internal_reference<>(),
      "list of segment combinations"
   )
   .def(
      "insert",
      static_cast<
         vdjml::Seg_match_id (Read_result::*)(vdjml::Segment_match const&)
      >(&Read_result::insert),
      "insert segment match"
   )
   .def(
      "insert",
      static_cast<
         void (Read_result::*)(vdjml::Segment_combination const&)
      >(&Read_result::insert),
      "insert combination of segment matches"
   )
   ;

   /**
    * Read_result::seg_comb_store
    */
   bp::class_<Read_result::seg_comb_store>(
      "Segment_combinations_list"
   )
   .def(bp::vector_indexing_suite<Read_result::seg_comb_store>())
   ;

   /**
    * Segment_match_map
    */
   bp::class_<Segment_match_map>(
            "Segment_match_map",
            "Collection of segment matches",
            bp::no_init
   )
   .def("__len__", &Segment_match_map::size)
   .def("empty", &Segment_match_map::empty)
   .def(
            "__getitem__",
            static_cast<
               Segment_match const& (Segment_match_map::*)(const vdjml::Seg_match_id) const
            >(&Segment_match_map::operator[]),
            bp::return_internal_reference<>(),
            "access segment match"
   )
   .def(
      "__iter__",
      bp::range<bp::return_value_policy<bp::copy_const_reference> >(
               &Segment_match_map::begin, &Segment_match_map::end
      )
   )
   ;
}
