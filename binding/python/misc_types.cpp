/** @file "/vdjml/binding/python/misc_types.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/aa_index.hpp"
#include "vdjml/btop.hpp"
#include "vdjml/compression.hpp"
#include "vdjml/gene_region_type.hpp"
#include "vdjml/interval.hpp"
#include "vdjml/btop.hpp"
#include "vdjml/aa_index.hpp"
#include "vdjml/numbering_system.hpp"
#include "vdjml/xml_writer_options.hpp"
using vdjml::Btop;
using vdjml::Xml_writer_options;
using vdjml::Interval;
using vdjml::Num_system;
using vdjml::Gene_region_type;
using namespace vdjml::aminoacid;
using namespace vdjml::nucleotide;

void export_misc_types() {

   /**
    * Nucleotide
    */
   bp::enum_<vdjml::nucleotide::Nuc>("Nucleotide")
   .value("Adenine", Adenine)
   .value("Cytosine", Cytosine)
   .value("Guanine", Guanine)
   .value("Thymine", Thymine)
   .value("Any", Any)
   .value("Uracil", Uracil)
   .value("Purine", Purine)     ///< R : A, G
   .value("Pyrimidine", Pyrimidine) ///< Y : C, T, U
   .value("Ketone", Ketone)     ///< K : G, T, U
   .value("Amine", Amine)      ///< M : A, C
   .value("Strong", Strong)     ///< S : C, G
   .value("Weak", Weak)       ///< W : A, T, U
   .value("not_A", not_A)      ///< B : C G T
   .value("not_C", not_C)      ///< D : A G T
   .value("not_G", not_G)      ///< H : A C T
   .value("not_T", not_T)      ///< V : A C G
   .value("Gap", Gap)
   .value("Unknown", Unknown)
   ;

   /**
    * Aminoacid
    */
   bp::enum_<vdjml::aminoacid::Aa>("Aminoacid")
   .value("Ala", Ala)
   .value("Cys", Cys)
   .value("Asp", Asp)
   .value("Glu", Glu)
   .value("Phe", Phe)
   .value("Gly", Gly)
   .value("His", His)
   .value("Ile", Ile)
   .value("Lys", Lys)
   .value("Leu", Leu)
   .value("Met", Met)
   .value("Asn", Asn)
   .value("Pro", Pro)
   .value("Gln", Gln)
   .value("Arg", Arg)
   .value("Ser", Ser)
   .value("Thr", Thr)
   .value("Val", Val)
   .value("Trp", Trp)
   .value("Tyr", Tyr)
   .value("Stop", Stop)
   ;

   /**
    * Compression
    */
   bp::enum_<vdjml::Compression>("Compression")
   .value("Uncompressed", vdjml::Uncompressed)
   .value("Unknown_compression", vdjml::Unknown_compression)
   .value("gzip", vdjml::gzip)
   .value("bzip2", vdjml::bzip2)
   .value("zlib", vdjml::zlib)
   .value("pkzip", vdjml::pkzip)
   .value("rar", vdjml::rar)
   .value("seven_z", vdjml::seven_z)
   ;

   /**
    * Xml_writer_options
    */
   bp::class_<Xml_writer_options> (
      "Xml_writer_options",
      "Options for XML output",
      bp::init<
         std::string const&,
         std::string const&,
         char,
         std::string const&,
         const std::size_t
      >(
           (
              bp::arg("indent") = "   ",
              bp::arg("encoding") = "UTF-8",
              bp::arg("quote") = '"',
              bp::arg("xml_version") = "1.0",
              bp::arg("buff_size") = 1024
           )
      )
   )
   .def_readwrite("indent", &Xml_writer_options::indent_, "indentation string")
   .def_readwrite("encoding", &Xml_writer_options::encoding_)
   .def_readwrite("quote", &Xml_writer_options::quote_, "quotation character")
   .def_readwrite("xml_version", &Xml_writer_options::xml_version_)
   .def_readwrite("buff_size", &Xml_writer_options::buff_size_, "output buffer size")
   ;

   /**
    * Interval
    */
   bp::class_<Interval>("Interval", "sequence interval", bp::no_init)
   .def(
         "pos0_len",
         &Interval::pos0_len,
         "create interval from 0-based starting position and length",
         (bp::arg("pos0"), bp::arg("len"))
   )
   .staticmethod("pos0_len")
   .def(
         "first_last1",
         &Interval::first_last1,
         "create interval from 1-based first and last positions",
         (bp::arg("first1"), bp::arg("last1"))
   )
   .staticmethod("first_last1")
   .def(
         "first_last0",
         &Interval::first_last0,
         "create interval from 0-based first and last positions",
         (bp::arg("first0"), bp::arg("last0"))
   )
   .staticmethod("first_last0")
   .def("length", &Interval::length)
   .def("pos0", &Interval::pos0)
   .def("pos1", &Interval::pos1)
   .def("last0", &Interval::last0)
   .def("last1", &Interval::last1)
   .def(str(bp::self))
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   ;

   /**
    * Num_system
    */
   bp::object ns = bp::class_<Num_system>("Num_system", bp::no_init);
   ns.attr("imgt") = Num_system::imgt;
   ns.attr("kabat") = Num_system::kabat;

   /**
    * Gene_region_type
    */
   bp::object grt =
            bp::class_<Gene_region_type>("Gene_region_type", bp::no_init);
   grt.attr("fr1") = Gene_region_type::fr1;
   grt.attr("fr2") = Gene_region_type::fr2;
   grt.attr("fr3") = Gene_region_type::fr3;
   grt.attr("cdr1") = Gene_region_type::cdr1;
   grt.attr("cdr2") = Gene_region_type::cdr2;
   grt.attr("cdr3") = Gene_region_type::cdr3;
   grt.attr("vd_junction") = Gene_region_type::vd_junction;
   grt.attr("dj_junction") = Gene_region_type::dj_junction;
   grt.attr("vj_junction") = Gene_region_type::vj_junction;
}
