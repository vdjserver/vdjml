/** @file "/vdjml/binding/python/result_builder.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/result_builder.hpp"
#include "vdjml/results_meta.hpp"
using vdjml::Aligner_id;
using vdjml::Gl_db_id;
using vdjml::Gl_seg_id;
using vdjml::Gl_seg_match_id;
using vdjml::Interval;
using vdjml::Match_metrics;
using vdjml::Numsys_id;
using vdjml::Read_result;
using vdjml::Region_id;
using vdjml::Result_builder;
using vdjml::Result_factory;
using vdjml::Results_meta;
using vdjml::Seg_match_id;
using vdjml::Segment_combination_builder;
using vdjml::Segment_match_builder;
using vdjml::Segment_match;

void export_result_builder() {

   /**
    * Segment_combination_builder
    */
   bp::class_<Segment_combination_builder>(
      "Segment_combination_builder",
      "Construct alignment results for a combination of germline gene segments",
      bp::no_init
   )
   .def(
      "insert_region",
      static_cast<
         void (Segment_combination_builder::*)(
                  std::string const&,
                  Interval const&,
                  Match_metrics const&,
                  const Numsys_id num_system,
                  const Aligner_id
         )
      >(&Segment_combination_builder::insert_region),
//      "add gene region alignment info",
      (
               bp::arg("name"),
               bp::arg("read_range"),
               bp::arg("metric")       = Match_metrics(),
               bp::arg("num_system")   = Numsys_id(),
               bp::arg("aligner_id")   = Aligner_id()
      )
   )
   .def(
      "insert_region",
      static_cast<
         void (Segment_combination_builder::*)(
                  const Region_id,
                  Interval const&,
                  Match_metrics const&,
                  Numsys_id num_system,
                  Aligner_id
         )
      >(&Segment_combination_builder::insert_region),
      (
               bp::arg("region"),
               bp::arg("read_range"),
               bp::arg("metric")       = Match_metrics(),
               bp::arg("num_system")   = Numsys_id(),
               bp::arg("aligner_id")   = Aligner_id()
      ),
      "indicate gene region location in read sequence"
      "\n\n"
      ":param region: :class:`Region_id`, type of region" "\n"
      ":param read_range: :class:`Interval`, start and end positions "
         "in read sequence" "\n"
      ":param metric: :class:`Match_metrics`, alignment metrics between read "
         "and germline sequences; default: no metrics recorded" "\n"
      ":param num_system: :class:`Numsys_id`, default: no numbering system "
         "recorded" "\n"
      ":param aligner_id: :class:`Aligner_id`, default: current aligner ID is "
         "used" "\n"
   )
   ;

   /**
    * Segment_match_builder
    */
   bp::class_<Segment_match_builder>(
            "Segment_match_builder",
            "Construct alignment results for one sequencing read segment match",
            bp::no_init
   )
   .def(
      "insert_gl_segment_match",
      static_cast<
         Gl_seg_match_id (Segment_match_builder::*)(
                  const Gl_seg_id,
                  const unsigned,
                  Numsys_id,
                  Aligner_id
         )
      >(&Segment_match_builder::insert_gl_segment_match),
      "add germline segment alignment info",
      (
               bp::arg("gl_segment_id"),
               bp::arg("pos0"),
               bp::arg("num_system_id")   = Numsys_id(),
               bp::arg("aligner")         = Aligner_id()
      )
   )
   .def(
      "insert_gl_segment_match",
      static_cast<
         Gl_seg_match_id (Segment_match_builder::*)(
                  const char,
                  std::string const&,
                  const unsigned,
                  Gl_db_id gl_database,
                  Numsys_id num_system,
                  Aligner_id aligner
         )
      >(&Segment_match_builder::insert_gl_segment_match),
      "add germline segment alignment info",
      (
               bp::arg("vdj"),
               bp::arg("seg_name"),
               bp::arg("gl_pos0"),
               bp::arg("gl_database") = Gl_db_id(),
               bp::arg("num_system") = Numsys_id(),
               bp::arg("aligner") = Aligner_id()
      )
   )
   .def(
      "insert_aa_substitution",
      static_cast<
      void (Segment_match_builder::*)(
               const unsigned,
               const char,
               const char
      )
      >(&Segment_match_builder::insert_aa_substitution),
      "add amino acid substitution information",
      (
               bp::arg("read_pos0"),
               bp::arg("read_aa"),
               bp::arg("gl_aa")
      )
   )
   .def(
      "insert_aa_substitution",
      static_cast<
      void (Segment_match_builder::*)(
               const unsigned,
               std::string const&,
               std::string const&
      )
      >(&Segment_match_builder::insert_aa_substitution),
      "add amino acid substitution information",
      (
               bp::arg("read_pos0"),
               bp::arg("read_aa"),
               bp::arg("gl_aa")
      )
   )
   .def(
      "get",
      static_cast<
         Segment_match const& (Segment_match_builder::*)() const
      >(&Segment_match_builder::get),
      bp::return_internal_reference<>(),
      "get segment match structure"
   )
   ;

   /**
    * Result_builder
    */
   bp::class_<Result_builder>(
      "Result_builder",
      "Construct alignment results for one sequencing read",
      bp::init<Results_meta&, std::string const&>(
               (bp::arg("meta"), bp::arg("read_id"))
      )
   )
   .def(
      "get",
      static_cast<
         Read_result const& (Result_builder::*)() const
      >(&Result_builder::get),
      bp::return_internal_reference<>(),
      "get result object (internal reference)"
   )
   .def(
      "release",
      &Result_builder::release,
      "get final result object (independent copy); "
      "Result_builder object cannot be used anymore"
   )
   .def(
      "insert_segment_match",
      &Result_builder::insert_segment_match,
      "add new segment match",
      (
               bp::arg("read_pos0"),
               bp::arg("btop"),
               bp::arg("vdj"),
               bp::arg("seg_name"),
               bp::arg("gl_pos0"),
               bp::arg("metric") = Match_metrics(),
               bp::arg("gl_database") = Gl_db_id(),
               bp::arg("num_system") = Numsys_id(),
               bp::arg("aligner") = Aligner_id()
      )
   )
   .def(
      "insert_segment_combination",
      &Result_builder::insert_segment_combination,
      (
               bp::arg("seg_match_1"),
               bp::arg("seg_match_2") = Seg_match_id(),
               bp::arg("seg_match_3") = Seg_match_id(),
               bp::arg("seg_match_4") = Seg_match_id(),
               bp::arg("seg_match_5") = Seg_match_id()
      )
   )
   ;

   /**
    * Result_factory
    */
   bp::class_<Result_factory>(
      "Result_factory",
      "Construct alignment results for many sequencing reads",
      bp::init<Results_meta&>()
   )
   .def(
      "new_result",
      &Result_factory::new_result,
      "new result builder",
      (bp::arg("read_id"))
   )
   .def(
      "set_default_gl_database",
      static_cast<
         void (Result_factory::*)(const Gl_db_id)
      >(&Result_factory::set_default_gl_database),
      "set default germline database"
   )
   .def(
      "set_default_gl_database",
      static_cast<
         Gl_db_id (Result_factory::*)(
                  std::string const&,
                  std::string const&,
                  std::string const&,
                  std::string const&
         )
      >(&Result_factory::set_default_gl_database),
      "set default germline database",
      (bp::arg("name"), bp::arg("version"),
               bp::arg("species"), bp::arg("url") = "")
   )
   .def(
      "set_default_aligner",
      static_cast<
         void (Result_factory::*)(const Aligner_id)
      >(&Result_factory::set_default_aligner),
      "set default aligner"
   )
   .def(
      "set_default_aligner",
      static_cast<
         Aligner_id (Result_factory::*)(
                  std::string const&,
                  std::string const&,
                  std::string const&,
                  std::string const&,
                  const unsigned
         )
      >(&Result_factory::set_default_aligner),
      "set default aligner",
      (
               bp::arg("name"),
               bp::arg("version"),
               bp::arg("parameters") = "",
               bp::arg("uri") = "",
               bp::arg("run_id") = 0
      )
   )
   .def(
      "set_default_num_system",
      static_cast<
         void (Result_factory::*)(const Numsys_id)
      >(&Result_factory::set_default_num_system),
      "set default numbering system"
   )
   .def(
      "set_default_num_system",
         static_cast<
         Numsys_id (Result_factory::*)(std::string const&)
      >(&Result_factory::set_default_num_system),
      "set default numbering system",
      (bp::arg("name"))
   )
   ;
}
