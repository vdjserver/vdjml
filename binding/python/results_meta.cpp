/** @file "/vdjml/binding/python/results_meta.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/results_meta.hpp"
using vdjml::Results_meta;

using vdjml::Aligner_info;
using vdjml::Gl_db_info;
using vdjml::Gl_segment_info;

using vdjml::Aligner_id;
using vdjml::Gl_db_id;
using vdjml::Numsys_id;
using vdjml::Gl_seg_id;
using vdjml::Region_id;

using vdjml::Aligner_map;
using vdjml::Germline_db_map;
using vdjml::Num_system_map;
using vdjml::Gl_segment_map;
using vdjml::Gene_region_map;

typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_results_meta() {

   /**
    * Results_meta
    */
   bp::class_<Results_meta, boost::shared_ptr<Results_meta> >(
      "Results_meta",
      "Metadata for a collection of alignment results of sequencing reads"
   )
   .def(
      "__getitem__",
      static_cast<
         Aligner_info const& (Results_meta::*)(const Aligner_id) const
      >(&Results_meta::operator[]),
      bp::return_internal_reference<>(),
      "get aligner info"
   )
   .def(
      "__getitem__",
      static_cast<
         Gl_db_info const& (Results_meta::*)(const Gl_db_id) const
      >(&Results_meta::operator[]),
      bp::return_internal_reference<>(),
      "get germline DB info"
   )
   .def(
      "__getitem__",
      static_cast<
         std::string const& (Results_meta::*)(const Numsys_id) const
      >(&Results_meta::operator[]),
      return_ref(),
      "get numbering system name"
   )
   .def(
      "__getitem__",
      static_cast<
         Gl_segment_info const& (Results_meta::*)(const Gl_seg_id) const
      >(&Results_meta::operator[]),
      bp::return_internal_reference<>(),
      "get germline segment info"
   )
   .def(
      "__getitem__",
      static_cast<
         std::string const& (Results_meta::*)(const Region_id) const
      >(&Results_meta::operator[]),
      return_ref(),
      "get gene region info"
   )
   .def(
      "insert",
      static_cast<
         Aligner_id (Results_meta::*)(Aligner_info const&)
      >(&Results_meta::insert),
      "insert information about aligner software"
   )
   .def(
      "insert",
      static_cast<
         Gl_db_id (Results_meta::*)(Gl_db_info const&)
      >(&Results_meta::insert),
      "insert information about database of germline segments"
   )
   .def(
      "insert",
      static_cast<
         Gl_seg_id (Results_meta::*)(Gl_segment_info const&)
      >(&Results_meta::insert),
      "insert information about germline segment"
   )
   .def(
      "aligner_map",
      static_cast<
         Aligner_map& (Results_meta::*)()
      >(&Results_meta::aligner_map),
      bp::return_internal_reference<>(),
      "return a map of aligner software descriptions"
   )
   .def(
      "gl_db_map",
      static_cast<
         Germline_db_map& (Results_meta::*)()
      >(&Results_meta::gl_db_map),
      bp::return_internal_reference<>(),
      "return a map of germline database descriptions"
   )
   .def(
      "num_system_map",
      static_cast<
         Num_system_map& (Results_meta::*)()
      >(&Results_meta::num_system_map),
      bp::return_internal_reference<>(),
      "return a map of numbering systems"
   )
   .def(
      "gl_segment_map",
      static_cast<
         Gl_segment_map& (Results_meta::*)()
      >(&Results_meta::gl_segment_map),
      bp::return_internal_reference<>(),
      "return a map of germline segment descriptions"
   )
   .def(
      "gene_region_map",
      static_cast<
         Gene_region_map& (Results_meta::*)()
      >(&Results_meta::gene_region_map),
      bp::return_internal_reference<>(),
      "return a map of gene region descriptions"
   )
   ;

   /**
    * Aligner_map
    */
   bp::class_<Aligner_map>(
            "Aligner_map",
            "aligner info map",
            bp::no_init
   )
   .def("__len__", &Aligner_map::size)
   .def("empty", &Aligner_map::empty)
   .def(
      "__getitem__",
      static_cast<
      Aligner_info const& (Aligner_map::*)(const Aligner_id) const
      >(&Aligner_map::operator[]),
      bp::return_internal_reference<>(),
      "access aligner info"
   )
   .def(
      "__iter__",
      bp::range<return_ref>(
         static_cast<
            Aligner_map::const_iterator (Aligner_map::*)()const
         >(&Aligner_map::begin),
         static_cast<
            Aligner_map::const_iterator (Aligner_map::*)()const
         >(&Aligner_map::end)
      )
   )
   ;

   /**
    * Germline_db_map
    */
   bp::class_<Germline_db_map>(
            "Germline_db_map",
            "map of germline database descriptions",
            bp::no_init
   )
   .def("__len__", &Germline_db_map::size)
   .def("empty", &Germline_db_map::empty)
   .def(
      "__getitem__",
      static_cast<
      Gl_db_info const& (Germline_db_map::*)(const Gl_db_id) const
      >(&Germline_db_map::operator[]),
      bp::return_internal_reference<>(),
      "access aligner info"
   )
   .def(
      "__iter__",
      bp::range<return_ref>(
         static_cast<
            Germline_db_map::const_iterator (Germline_db_map::*)()const
         >(&Germline_db_map::begin),
         static_cast<
            Germline_db_map::const_iterator (Germline_db_map::*)()const
         >(&Germline_db_map::end)
      )
   )
   ;

   /**
    * Num_system_map
    */
   bp::class_<Num_system_map>(
            "Num_system_map",
            "map of numbering system names",
            bp::no_init
   )
   .def("__len__", &Num_system_map::size)
   .def("empty", &Num_system_map::empty)
   .def(
      "__getitem__",
      static_cast<
         std::string const& (Num_system_map::*)(const Numsys_id) const
      >(&Num_system_map::operator[]),
      return_ref(),
      "access aligner info"
   )
   .def(
      "__iter__",
      bp::range<return_ref>(
         static_cast<
            Num_system_map::const_iterator (Num_system_map::*)()const
         >(&Num_system_map::begin),
         static_cast<
            Num_system_map::const_iterator (Num_system_map::*)()const
         >(&Num_system_map::end)
      )
   )
   ;

   /**
    * Gl_segment_map
    */
   bp::class_<Gl_segment_map>(
            "Gl_segment_map",
            "map of germline segment descriptions",
            bp::no_init
   )
   .def("__len__", &Gl_segment_map::size)
   .def("empty", &Gl_segment_map::empty)
   .def(
      "__getitem__",
      static_cast<
         Gl_segment_info const& (Gl_segment_map::*)(const Gl_seg_id) const
      >(&Gl_segment_map::operator[]),
      bp::return_internal_reference<>(),
      "access germline segment description"
   )
   .def(
      "__iter__",
      bp::range<return_ref>(
         static_cast<
            Gl_segment_map::const_iterator (Gl_segment_map::*)()const
         >(&Gl_segment_map::begin),
         static_cast<
            Gl_segment_map::const_iterator (Gl_segment_map::*)()const
         >(&Gl_segment_map::end)
      )
   )
   ;

   /**
    * Gene_region_map
    */
   bp::class_<Gene_region_map>(
            "Gene_region_map",
            "map of gene region names",
            bp::no_init
   )
   .def("__len__", &Gene_region_map::size)
   .def("empty", &Gene_region_map::empty)
   .def(
      "__getitem__",
      static_cast<
         std::string const& (Gene_region_map::*)(const Region_id) const
      >(&Gene_region_map::operator[]),
      return_ref(),
      "access gene region name"
   )
   .def(
      "__iter__",
      bp::range<return_ref>(
         static_cast<
            Gene_region_map::const_iterator (Gene_region_map::*)()const
         >(&Gene_region_map::begin),
         static_cast<
            Gene_region_map::const_iterator (Gene_region_map::*)()const
         >(&Gene_region_map::end)
      )
   )
   ;
}

