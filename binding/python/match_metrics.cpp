/** @file "/vdjml/binding/python/match_metrics.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/match_metrics.hpp"
using vdjml::Match_metrics;
using vdjml::Percent;

void export_match_metrics() {

   /**
    * Percent
    */
   bp::object p = bp::class_<Percent>(
      "Percent",
      bp::init<const float>("p")
   )
   .def(str(bp::self))
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   .def("percent", &Percent::percent)
   .def("fraction", &Percent::fraction)
   ;
   p.attr("unset") = Percent::unset;

   /**
    * Match_metrics
    */
   bp::object mm = bp::class_<Match_metrics>(
      "Match_metrics",
      "Metrics of sequence alignment",
      bp::init<>()
   )
   .def(
            bp::init<
               const float,
               const int,
               const unsigned,
               const unsigned,
               const unsigned,
               const bool,
               const bool,
               const bool,
               const bool,
               const bool
            >(
               (
                        bp::arg("identity")           = Percent::unset,
                        bp::arg("score")              = Match_metrics::unscore,
                        bp::arg("substitutions")      = 0,
                        bp::arg("insertions")         = 0,
                        bp::arg("deletions")          = 0,
                        bp::arg("stop_codon")         = false,
                        bp::arg("mutated_invariant")  = false,
                        bp::arg("inverted")           = false,
                        bp::arg("out_frame_indel")    = false,
                        bp::arg("out_frame_vdj")      = false
               )
            )
   )
   .def("score", &Match_metrics::score, "alignment score")
   .def("identity", &Match_metrics::identity, "percent identity")
   .def(
      "insertions",
      &Match_metrics::insertions,
      "number of bases inserted in the read"
   )
   .def(
      "deletions",
      &Match_metrics::deletions,
      "number of bases deleted in the read"
   )
   .def(
      "substitutions",
      &Match_metrics::substitutions,
      "number of base substitutions"
   )
   .def(
      "stop_codon",
      &Match_metrics::stop_codon,
      "return True if stop codon present"
      )
   .def(
      "mutated_invariant",
      &Match_metrics::mutated_invariant,
      "return True if invariant amino acid mutated"
   )
   .def(
      "inverted",
      &Match_metrics::inverted,
      "return True if sequence inverted"
   )
   .def(
      "out_frame_indel",
      &Match_metrics::out_frame_indel,
      "return True if INDEL causes frameshift"
   )
   .def(
      "out_frame_vdj",
      &Match_metrics::out_frame_vdj,
      "return True if VDJ rearrangement is out of frame"
   )
   .def(
      "frame_shift",
      &Match_metrics::frame_shift,
      "return True if a frame shift is present (out_frame_indel() || out_frame_vdj())"
   )
   .def(
      "productive",
      &Match_metrics::productive,
      "return True if sequence is productive "
      "( ! (out_frame_indel() || out_frame_vdj() || mutated_invariant()) )"
   )
   ;
   mm.attr("unscore") = Match_metrics::unscore;

}
