/** @file "/vdjml/binding/python/nucleotide_iterator.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/nucleotide_iterator.hpp"
#include "wrap_btop_iterator.hpp"

using vdjml::nucleotide_iter;
using vdjml::Aligned_nuc_iter;
using vdjml::nucleotide_iter_sv;
using vdjml::Aligned_seq;
using vdjml::Btop;
using vdjml::Mismatch_iter;
using vdjml::Nucleotide_match;
using vdjml::Wrap_iter;

namespace{

/**
*******************************************************************************/
Mismatch_iter make_mismatch_iter(Btop const& btop) {
   return Mismatch_iter(btop);
}

/*
*******************************************************************************/
bp::object call_nucleotide_iterator(
         Btop const& btop,
         std::string const& read_seq,
         std::string const& gl_seq,
         char const match_char
) {
   if( read_seq.size() ) {
      nucleotide_iter_sv i(btop, vdjml::In_seq_val(vdjml::seq::Read, read_seq));
      return bp::object(i);
   }
   if( gl_seq.size() ) {
      nucleotide_iter_sv i(btop, vdjml::In_seq_val(vdjml::seq::GL, gl_seq));
      return bp::object(i);
   }

   return bp::object(nucleotide_iterator(btop, match_char));
}

}//anonymous namespace

/**
*******************************************************************************/
void export_nucleotide_iterators() {

   /**
    * Mismatch_iter
    */
   Wrap_iter<Mismatch_iter>("Mismatch_iter");

   /**
    * Nucleotide_iter_s
    * Hold Python sequence by value
    */
   Wrap_iter<nucleotide_iter_sv>("Nucleotide_iter_s");

   /**
    * Nucleotide_iter
    */
   Wrap_iter<nucleotide_iter>("Nucleotide_iter");

   /**
    * mismatch_iterator()
    */
   bp::def(
            "mismatches",
            &make_mismatch_iter,
            "return nucleotide mismatch iterator",
            (
                     bp::arg("btop")
            )
   );

   /**
    * nucleotide_iterator()
    */
   bp::def(
            "nucleotides",
            &call_nucleotide_iterator,
            (
                     bp::arg("btop"),
                     bp::arg("read_seq")     = "",
                     bp::arg("gl_seq")       = "",
                     bp::arg("match_char")   = '.'
            ),
            "return nucleotide iterator"
   );

}
