/** @file "/vdjml/binding/python/vdjml_reader.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/compression.hpp"
#include "vdjml/generator_info.hpp"
#include "vdjml/read_result.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vdjml_reader.hpp"

using vdjml::Read_result;
using vdjml::Results_meta;
using vdjml::Vdjml_generator_info;
using vdjml::Vdjml_reader;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_vdjml_reader() {

   /**
    * Vdjml_generator_info
    */
   bp::class_<Vdjml_generator_info>(
      "Vdjml_generator_info",
      "Info about aligner software",
      bp::no_init
   )
   .def(
      "name",
      &Vdjml_generator_info::name, return_ref(),
      "VDJML file generator name"
   )
   .def(
      "version",
      &Vdjml_generator_info::version,
      return_ref(),
      "VDJML file generator version"
   )
   .def(
      "datetime",
      &Vdjml_generator_info::datetime,
      "file creation date and time, GMT"
   )
   .def(
      "datetime_str",
      &Vdjml_generator_info::datetime_str,
      "file creation date and time, GMT"
   )
   ;

   /**
    * Vdjml_reader
    */
   bp::class_<Vdjml_reader, boost::noncopyable>(
      "Vdjml_reader",
      "Incrementally parse VDJML read-by-read",
      bp::init<std::string const&, vdjml::Compression>(
         (
                  bp::arg("file_name"),
                  bp::arg("compression") = vdjml::Uncompressed
         )
      )
   )
   .def("version", &Vdjml_reader::version, "VDJML version of the file")
   .def("version_str", &Vdjml_reader::version_str, "VDJML version of the file")
   .def(
      "generator_info",
      &Vdjml_reader::generator_info,
      return_ref(),
      "information about VDJML generator"
   )
   .def("meta", &Vdjml_reader::meta, "results meta")
   .def("next", &Vdjml_reader::next, "parse next read result")
   .def("has_result", &Vdjml_reader::has_result, "return True if result was found")
   .def(
      "result",
      static_cast<
      Read_result const&(Vdjml_reader::*)() const
      >(&Vdjml_reader::result),
      bp::return_value_policy<bp::copy_const_reference>(),
      "return parsed result"
   )
   ;
}
