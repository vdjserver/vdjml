/** @file "/vdjml/binding/python/vdjml_writer.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/vdjml_writer.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/read_result.hpp"
#include "vdjml/compression.hpp"
using vdjml::Results_meta;
using vdjml::Vdjml_writer;

void export_vdjml_writer() {

   /**
    * Vdjml_writer
    */
   bp::class_<Vdjml_writer, boost::noncopyable>(
            "Vdjml_writer",
            "Incrementally serialize VDJ alignment results",
            bp::init<
               std::string const&,  Results_meta const&,
               vdjml::Compression, const unsigned,
               vdjml::Xml_writer_options const&
            >(
                  (
                        bp::arg("file_name"),
                        bp::arg("meta"),
                        bp::arg("compression") = vdjml::Unknown_compression,
                        bp::arg("version") = VDJML_CURRENT_VERSION,
                        bp::arg("options") = vdjml::Xml_writer_options()
                  )
            )
   )
   .def(
            "__call__",
            &Vdjml_writer::operator(),
            (
                     bp::arg("result")
            ),
            "serialize read result"
   )
   ;
}
