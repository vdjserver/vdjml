/** @file "/vdjml/binding/python/vdjml.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/exception.hpp"
#include "vdjml.hpp"

namespace{
void exception_translator(boost::exception const& e) {
   PyErr_SetString(
         PyExc_RuntimeError, boost::diagnostic_information(e).c_str()
   );
}

}//anonymous namespace

BOOST_PYTHON_MODULE(_vdjml_py) {
   bp::docstring_options doc_opts(
            true, //show_user_defined
            true, //show_py_signatures
            false //show_cpp_signatures
   );

   bp::register_exception_translator<boost::exception>(&exception_translator);

   bp::object package = bp::scope();
   package.attr("__path__") = "_vdjml_py";

   export_ids();
   export_match_metrics();
   export_btop();
   export_misc_types();
   export_nucleotide_iterators();
   export_codon_iterators();
   export_read_result();
   export_result_store();
   export_results_meta();
   export_vdjml_writer();
   export_vdjml_reader();
   export_segment_match();
   export_segment_combination();
   export_result_builder();
   export_gene_region();
   export_germline_db_info();
   export_aligner_info();
   export_germline_segment_info();
}
