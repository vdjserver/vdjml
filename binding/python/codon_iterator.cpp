/** @file "/vdjml/binding/python/codon_iterator.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/optional.hpp"
#include "boost/python.hpp"
namespace bp = boost::python;
#include "vdjml/codon_iterator.hpp"
#include "wrap_btop_iterator.hpp"

using vdjml::Aligned_pos;
using vdjml::Btop;
using vdjml::codon_iter_b;
using vdjml::codon_iter_bsg;
using vdjml::codon_iter_svb;
using vdjml::codon_iter_svbsg;
using vdjml::Codon_match;
using vdjml::Wrap_iter;

namespace{

/*
*******************************************************************************/
struct Codon_match_wrap {
   static void check_i_nuc(const int n) {
      typedef Codon_match::Err Err;
      if( n < 0 || n > 2 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("invalid nucleotide index; should be [0,2]")
               << Err::int1_t(n)
      );
   }

   static std::size_t read_pos(Codon_match const& cm, const int n) {
      check_i_nuc(n);
      return cm.read_pos(static_cast<vdjml::codon::Nuc_index>(n));
   }

   static std::size_t gl_pos(Codon_match const& cm, const int n) {
      check_i_nuc(n);
      return cm.gl_pos(static_cast<vdjml::codon::Nuc_index>(n));
   }

   static char read_char(Codon_match const& cm, const int n) {
      check_i_nuc(n);
      return cm.read_char(static_cast<vdjml::codon::Nuc_index>(n));
   }

   static char gl_char(Codon_match const& cm, const int n) {
      check_i_nuc(n);
      return cm.gl_char(static_cast<vdjml::codon::Nuc_index>(n));
   }

   static vdjml::nucleotide::Nuc read_nuc(Codon_match const& cm, const int n) {
      check_i_nuc(n);
      return cm.nucleotide(
               vdjml::seq::Read,
               static_cast<vdjml::codon::Nuc_index>(n)
      );
   }

   static vdjml::nucleotide::Nuc gl_nuc(Codon_match const& cm, const int n) {
      check_i_nuc(n);
      return cm.nucleotide(
               vdjml::seq::GL,
               static_cast<vdjml::codon::Nuc_index>(n)
      );
   }

   static bool is_read_contiguous(Codon_match const& cm) {
      return cm.is_contiguous(vdjml::seq::Read);
   }

   static bool is_gl_contiguous(Codon_match const& cm) {
      return cm.is_contiguous(vdjml::seq::GL);
   }

   static bool is_read_translatable(Codon_match const& cm) {
      return cm.is_translatable(vdjml::seq::Read);
   }

   static bool is_gl_translatable(Codon_match const& cm) {
      return cm.is_translatable(vdjml::seq::GL);
   }

   static vdjml::aminoacid::Aa translate_read(Codon_match const& cm) {
      return cm.translate(vdjml::seq::Read);
   }

   static vdjml::aminoacid::Aa translate_gl(Codon_match const& cm) {
      return cm.translate(vdjml::seq::GL);
   }

};

/*
*******************************************************************************/
bp::object call_codon_iterator(
         Btop const& btop,
         const std::size_t read_start,
         const std::size_t gl_start,
         std::string const& read_seq,
         std::string const& gl_seq,
         const bool follow_read,
         const bool follow_gl,
         char const match_char
) {
   typedef Btop::Err Err;
   Aligned_pos start;
   if( read_start != Btop::unset ) start = Aligned_pos::read(read_start);
   else if( gl_start != Btop::unset ) start = Aligned_pos::gl(gl_start);
   else BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t(
                     "start position, \"read_start\" or \"gl_start\" is "
                     "undefined"
            )
   );

   boost::optional<vdjml::seq::Seq> follow;
   if( follow_read ) follow.reset(vdjml::seq::Read);
   else if( follow_gl ) follow.reset(vdjml::seq::GL);

   boost::optional<vdjml::In_seq_val> seq;
   if( read_seq.size() ) {
      seq.reset(vdjml::In_seq_val(vdjml::seq::Read, read_seq));
   } else if( gl_seq.size() ) {
      seq.reset(vdjml::In_seq_val(vdjml::seq::GL, gl_seq));
   }

   if( seq && follow ) {
      codon_iter_svbsg i(
               btop,
               start,
               seq.get(),
               vdjml::Skip_seq_gaps(follow.get())
      );
      return bp::object(i);
   }

   if( seq && ! follow ) {
      codon_iter_svb i(btop, start, seq.get(), vdjml::All_positions());
      return bp::object(i);
   }

   if( ! seq && follow ) {
      codon_iter_bsg i(
               btop,
               start,
               vdjml::Match_char(match_char),
               vdjml::Skip_seq_gaps(follow.get())
      );
      return bp::object(i);
   }

   codon_iter_b i(
            btop,
            start,
            vdjml::Match_char(match_char),
            vdjml::All_positions()
   );

   return bp::object(i);
}

}//anonymous namespace

/**
*******************************************************************************/
void export_codon_iterators() {

   /**
    * Codon_match
    */
   bp::class_<Codon_match>(
      "Codon_match",
      "Information about a pair of aligned codons, nucleotide triples, "
      "which may potentially contain gaps",
      bp::no_init
   )
   .def(
            "read_pos",
            &Codon_match_wrap::read_pos,
            bp::arg("i") = 0,
            ":param int i: nucleotide position in the codon [0,2]" "\n"
            ":return: position of codon nucleotide in read sequence; "
            "if a deletion, return position of the first "
            "nucleotide to the right, if at end of sequence, return sequence "
            "length"
   )
   .def(
            "gl_pos",
            &Codon_match_wrap::gl_pos,
            (
                     bp::arg("i") = 0
            ),
            ":param int i: nucleotide position in the codon [0,2]" "\n"
            ":return: position of codon nucleotide in germline sequence; "
            "if a deletion, return position of the first "
            "nucleotide to the right, if at end of sequence, return sequence "
            "length"
   )
   .def(
            "read_char",
            &Codon_match_wrap::read_char,
            (
                     bp::arg("i")
            ),
            ":param int i: nucleotide position in the codon [0,2]" "\n"
            ":return: codon nucleotide in read sequence"
   )
   .def(
            "gl_char",
            &Codon_match_wrap::gl_char,
            (
                     bp::arg("i")
            ),
            ":param int i: nucleotide position in the codon [0,2]" "\n"
            ":return: codon nucleotide in germline sequence"
   )
   .def(
            "read_nuc",
            &Codon_match_wrap::read_nuc,
            (
                     bp::arg("i")
            ),
            ":param int i: nucleotide position in the codon [0,2]" "\n"
            ":return: codon nucleotide in read sequence"
   )
   .def(
            "gl_nuc",
            &Codon_match_wrap::gl_nuc,
            (
                     bp::arg("i")
            ),
            ":param int i: nucleotide position in the codon [0,2]" "\n"
            ":return: codon nucleotide in germline sequence"
   )
   .def(
            "is_read_contiguous",
            &Codon_match_wrap::is_read_contiguous,
            ":return: True if read codon contains no gaps"
   )
   .def(
            "is_gl_contiguous",
            &Codon_match_wrap::is_gl_contiguous,
            ":return: True if germline codon contains no gaps"
   )
   .def(
            "is_read_translatable",
            &Codon_match_wrap::is_read_translatable,
            ":return: True if read codon can be unambiguously translated"
   )
   .def(
            "is_gl_translatable",
            &Codon_match_wrap::is_gl_translatable,
            ":return: True if germline codon can be unambiguously translated"
   )
   .def(
            "is_translatable",
            static_cast<bool (Codon_match::*)() const>(
                     &Codon_match::is_translatable
            ),
            ":return: True if both codons can be unambiguously translated"
   )
   .def(
            "translate_read",
            &Codon_match_wrap::translate_read,
            ":return: amino acid encoded by read codon" "\n"
            ":raises RuntimeError: if not :meth:`is_read_translatable`"
   )
   .def(
            "translate_gl",
            &Codon_match_wrap::translate_gl,
            ":return: amino acid encoded by germline codon" "\n"
            ":raises RuntimeError: if not :meth:`is_gl_translatable`"
   )
   .def(
            "is_match",
            &Codon_match::is_match,
            ":return: True if same nucleotides in read and germline"
   )
   .def(
            "is_silent",
            &Codon_match::is_silent,
            ":return: True if :meth:`is_match` or "
            "if :meth:`translate_read` == :meth:`translate_gl`" "\n"
            ":raises RuntimeError: if not :meth:`is_match` and "
            "not :meth:`is_translatable`"
   )
   ;

   /**
    * Codon_iter_b
    */
   Wrap_iter<codon_iter_b>("Codon_iter_b");

   /**
    * Codon_iter_bsg
    */
   Wrap_iter<codon_iter_bsg>("Codon_iter_bsg");

   /**
    * Codon_iter_sb
    */
   Wrap_iter<codon_iter_svb>("Codon_iter_sb");

   /**
    * Codon_iter_sbsg
    */
   Wrap_iter<codon_iter_svbsg>("Codon_iter_sbsg");

   /**
    * codons()
    */
   bp::def(
            "codons",
            &call_codon_iterator,
            ":return: codon iterator",
            (
                     bp::arg("btop"),
                     bp::arg("read_start")   = Btop::unset,
                     bp::arg("gl_start")     = Btop::unset,
                     bp::arg("read_seq")     = "",
                     bp::arg("gl_seq")       = "",
                     bp::arg("follow_read")  = false,
                     bp::arg("follow_gl")    = false,
                     bp::arg("match_char")   = '.'
            )
   );

}
