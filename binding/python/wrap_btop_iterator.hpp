/** @file "/vdjml/binding/python/wrap_btop_iterator.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef WRAP_BTOP_ITERATOR_HPP_
#define WRAP_BTOP_ITERATOR_HPP_
#include "boost/python.hpp"

namespace vdjml{

/**
*******************************************************************************/
template<class Iter> struct Wrap_iter {
   typedef Iter iter_type;
   typedef typename iter_type::value_type value_type;

   static value_type next(iter_type& mi) {
      if( mi.at_end() ) {
         PyErr_SetString(PyExc_StopIteration, "end of Btop");
         boost::python::throw_error_already_set();
      }
      const value_type m = mi.get();
      mi.next();
      return m;
   }

   static boost::python::object const&
   pass_through(boost::python::object const& o) {return o;}

   Wrap_iter(char const* name) {
      boost::python::class_<iter_type>(name, boost::python::no_init)
      .def("next", &next)
      .def(
               "__iter__",
               &pass_through,
               boost::python::return_value_policy<
                  boost::python::copy_const_reference
               >()
      )
      ;
   }
};

}//namespace vdjml
#endif /* WRAP_BTOP_ITERATOR_HPP_ */
