/** @file "/vdjml/binding/python/germline_segment.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/germline_segment_info.hpp"
using vdjml::Gl_segment_info;
using vdjml::Gl_db_id;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_germline_segment_info() {

   /**
    * Gl_segment_info
    */
   bp::class_<Gl_segment_info>(
      "Gl_segment_info",
      "germline segment description",
      bp::init<
         const Gl_db_id,
         const char,
         std::string const&
      >(
               (
                        bp::arg("gl_db_id"),
                        bp::arg("vdj"),
                        bp::arg("name")
               )
      )
   )
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   .def("id", &Gl_segment_info::id, "germline segment ID")
   .def("gl_database", &Gl_segment_info::gl_database, "germline database ID")
   .def("segment_type", &Gl_segment_info::segment_type, "germline segment type")
   .def("name", &Gl_segment_info::name, return_ref(), "germline segment name")
   ;

}
