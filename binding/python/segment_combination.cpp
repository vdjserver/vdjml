/** @file "/vdjml/binding/python/segment_combination.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/segment_combination.hpp"
using vdjml::Gene_region;
using vdjml::Segment_combination;
using vdjml::Seg_match_id;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_segment_combination() {

   /**
    * Segment_combination
    */
   bp::class_<Segment_combination>(
      "Segment_combination",
      "combination of aligned germline segments",
      bp::init<
         const Seg_match_id,
         const Seg_match_id,
         const Seg_match_id,
         const Seg_match_id,
         const Seg_match_id
      >(
               (
                        bp::arg("seg_match_1"),
                        bp::arg("seg_match_2") = Seg_match_id(),
                        bp::arg("seg_match_3") = Seg_match_id(),
                        bp::arg("seg_match_4") = Seg_match_id(),
                        bp::arg("seg_match_5") = Seg_match_id()
               )
      )
   )
   .def(
      "segments",
      &Segment_combination::segments,
      bp::return_internal_reference<>(),
      "set of segment match IDs"
   )
   .def(
      "regions",
      &Segment_combination::regions,
      bp::return_internal_reference<>(),
      "collection of gene regions"
   )
   .def(
      "insert",
      static_cast<
         void (Segment_combination::*)(const Seg_match_id)
      >(&Segment_combination::insert),
      "insert segment match ID"
   )
   .def(
      "insert",
      static_cast<
         void (Segment_combination::*)(Gene_region const&)
      >(&Segment_combination::insert),
      "insert gene region"
   )
   ;

   /**
    * segment_set
    */
   typedef Segment_combination::segment_set segment_set;
   bp::class_<segment_set>(
      "Segment_match_id_set",
      "set of segment match IDs",
      bp::no_init
   )
   .def("__len__", &segment_set::size)
   .def("empty", &segment_set::empty)
   .def(
      "__getitem__",
      static_cast<
         Seg_match_id const& (segment_set::*)(const std::size_t) const
      >(&segment_set::operator[]),
      bp::return_internal_reference<>(),
      "access segment match"
   )
   .def(
      "__iter__",
      bp::range<return_ref>(
         static_cast<
            segment_set::const_iterator (segment_set::*)()const
         >(&segment_set::begin),
            static_cast<
            segment_set::const_iterator (segment_set::*)()const
         >(&segment_set::end)
      )
   )
   ;

   /**
    * gene_region_set
    */
   typedef Segment_combination::gene_region_set gr_set;
   bp::class_<gr_set>(
            "Gene_region_set",
            "set of gene regions",
            bp::no_init
   )
   .def("__len__", &gr_set::size)
   .def("empty", &gr_set::empty)
   .def(
      "__getitem__",
      static_cast<
         Gene_region const& (gr_set::*)(const std::size_t) const
      >(&gr_set::operator[]),
      bp::return_internal_reference<>(),
      "access segment match"
   )
   .def(
      "__iter__",
      bp::range<return_ref>(
         static_cast<
            gr_set::const_iterator (gr_set::*)()const
         >(&gr_set::begin),
         static_cast<
            gr_set::const_iterator (gr_set::*)()const
         >(&gr_set::end)
      )
   )
   ;
}

