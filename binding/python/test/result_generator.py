#!/usr/bin/env python

'''
vdjml/binding/python/test/result_generator.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2014
'''

import unittest
from sample_result_generator import *

class Test(unittest.TestCase):
    def test_1(self):
        # create results metainformation object
        meta = vdjml.Results_meta()

        # make read result writer
        rrw = vdjml.Vdjml_writer('out/temp/py_out_3.vdjml', meta)
        # write compressed VDJML
        # rrw = vdjml.Vdjml_writer('out/temp/py_out_3.vdjml.gz', meta)
        # rrw = vdjml.Vdjml_writer('out/temp/py_out_3.vdjml.bz2', meta)

        result_store = vdjml.Result_store(meta)

        for result in get_results(meta, 10):
            rrw(result)  # write out the result
#             result_store.insert(result)  # or store it in results store
#             print result
#             print result.id()
#             print result.segment_matches()
#             print result.segment_combinations()
            pass  # or do something else

if __name__ == '__main__': unittest.main()
