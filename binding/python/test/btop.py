#!/usr/bin/env python

'''
vdjml/binding/python/test/btop.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2014
'''

import unittest
import vdjml

class Test(unittest.TestCase):
    def test_btop(self):
        b = vdjml.Btop('')
        self.assertTrue(b.empty())
        
    def test_docstring(self):
        self.assertTrue(
                        'return True if Btop is empty' in 
                        vdjml.Btop.empty.__doc__
                        )
        
    def test_btop_stats(self):
        '''
           01234567  89
           ..C.G...--GATG... read
           ..A.-...AGC--C... gl
           0123 456789  10
        '''
        b = vdjml.Btop('2CA1G-3-A-GGCA-T-GC3')
        stats = vdjml.Btop_stats(b)
        self.assertEqual(stats.insertions_, 3)
        self.assertEqual(stats.deletions_, 2)
        self.assertEqual(stats.substitutions_, 3)
        self.assertEqual(stats.read_len_, 15)
        self.assertEqual(stats.gl_len_, 14)
        self.assertEqual(stats.matches(), 9)
        
    def test_nucleotide_match(self):
        '''
           0123456789
           .....AA... read
           .....C-... germline
           012345 678
        '''
        b = vdjml.Btop('5ACA-3')
        
        nm = vdjml.nucleotide_match(b, read_pos0=6)
        self.assertEqual(nm.read_pos(), 6)
        self.assertEqual(nm.gl_pos(), 6)
        self.assertEqual(nm.read_nuc(), 'A')
        self.assertEqual(nm.gl_nuc(), '-')
        
        nm = vdjml.nucleotide_match(b, gl_pos0=6)
        self.assertEqual(nm.read_pos(), 7)
        self.assertEqual(nm.gl_pos(), 6)
        self.assertEqual(nm.read_nuc(), '.')
        self.assertEqual(nm.gl_nuc(), '.')
        
        nm = vdjml.nucleotide_match(b, read_pos0=8, match_char='M')
        self.assertEqual(nm.read_pos(), 8)
        self.assertEqual(nm.gl_pos(), 7)
        self.assertEqual(nm.read_nuc(), 'M')
        self.assertEqual(nm.gl_nuc(), 'M')
        
        nm = vdjml.nucleotide_match(b, gl_pos0=8)
        self.assertEqual(nm.read_pos(), 9)
        self.assertEqual(nm.gl_pos(), 8)
        self.assertEqual(nm.read_nuc(), '.')
        self.assertEqual(nm.gl_nuc(), '.')
        
    def test_sequence_match(self):
        '''
           0123456789
           .....AA... read
           .....C-... germline
           012345 678
        '''
        b = vdjml.Btop('5ACA-3')
        
        s = vdjml.sequence_match(b)
        self.assertEqual(s.start_[0], 0)
        self.assertEqual(s.start_[1], 0)
        self.assertEqual(s.end_[0], 10)
        self.assertEqual(s.end_[1], 9)
        self.assertEqual(s.seq_[0], '.....AA...')
        self.assertEqual(s.seq_[1], '.....C-...')

        s = vdjml.sequence_match(b, match_char='M')
        self.assertEqual(s.seq_[0], 'MMMMMAAMMM')	#read
        self.assertEqual(s.seq_[1], 'MMMMMC-MMM')	#ref

        s = vdjml.sequence_match(b, gl_start=2, gl_end=7)
        self.assertEqual(s.start_[0], 2)
        self.assertEqual(s.start_[1], 2)
        self.assertEqual(s.end_[0], 8)
        self.assertEqual(s.end_[1], 7)
        self.assertEqual(s.seq_[0], '...AA.')
        self.assertEqual(s.seq_[1], '...C-.')

        s = vdjml.sequence_match(b, read_start=2, read_end=7)
        self.assertEqual(s.start_[0], 2)		#read start
        self.assertEqual(s.start_[1], 2)		#ref  start
        self.assertEqual(s.end_[0], 7)			#read end
        self.assertEqual(s.end_[1], 6)			#ref end
        self.assertEqual(s.seq_[0], '...AA')		#query alignment
        self.assertEqual(s.seq_[1], '...C-')		#reference/germline alignment

        s = vdjml.sequence_match(b, gl_start=2, gl_end=6)
        self.assertEqual(s.start_[0], 2)
        self.assertEqual(s.start_[1], 2)
        self.assertEqual(s.end_[0], 7)
        self.assertEqual(s.end_[1], 6)
        self.assertEqual(s.seq_[0], '...AA')
        self.assertEqual(s.seq_[1], '...C-')

        s = vdjml.sequence_match(b, read_start=2, read_end=6)
        self.assertEqual(s.start_[0], 2)
        self.assertEqual(s.start_[1], 2)
        self.assertEqual(s.end_[0], 6)
        self.assertEqual(s.end_[1], 6)
        self.assertEqual(s.seq_[0], '...A')
        self.assertEqual(s.seq_[1], '...C')

        s = vdjml.sequence_match(
                                b, 
                                read_start=2, 
                                read_end=6,
                                read_seq='gggggAAccc',
                                )
        self.assertEqual(s.start_[0], 2)
        self.assertEqual(s.start_[1], 2)
        self.assertEqual(s.end_[0], 6)
        self.assertEqual(s.end_[1], 6)
        self.assertEqual(s.seq_[0], 'gggA')
        self.assertEqual(s.seq_[1], 'gggC')

        s = vdjml.sequence_match(
                                b, 
                                read_start=2, 
                                read_end=6,
                                gl_seq='gggggccc'
                                )
        self.assertEqual(s.start_[0], 2)
        self.assertEqual(s.start_[1], 2)
        self.assertEqual(s.end_[0], 6)
        self.assertEqual(s.end_[1], 6)
        self.assertEqual(s.seq_[0], 'gggA')
        self.assertEqual(s.seq_[1], 'gggC')

	#additional test, aquire alignment only from germ_line
        s = vdjml.sequence_match(
                                b,
                                gl_start=2, 
                                gl_end=6,
                                gl_seq='gggggcccccccccccccccccccccccc'
				#       01234567890123456789012345678
				#	  xxxxx   1         2
                                )
        self.assertEqual(s.start_[0], 2)	
        self.assertEqual(s.start_[1], 2)
        self.assertEqual(s.end_[0], 7)
        self.assertEqual(s.end_[1], 6)
        self.assertEqual(s.seq_[0], 'gggAA')
        self.assertEqual(s.seq_[1], 'gggC-')


        s = vdjml.sequence_match(
                                b, 
                                gl_seq='gggggCggg'
                                )
        self.assertEqual(s.start_[0], 0)
        self.assertEqual(s.start_[1], 0)
        self.assertEqual(s.end_[0], 10)
        self.assertEqual(s.end_[1], 9)
        self.assertEqual(s.seq_[0], 'gggggAAggg')
        self.assertEqual(s.seq_[1], 'gggggC-ggg')
        
    def test_trim_complement(self):
        s1 = 'actgactg'
        s2 = vdjml.trim_complement(s1, vdjml.Interval.pos0_len(0, len(s1)), False)
        self.assertEqual(s2, s1)
        s2 = vdjml.trim_complement(s1, vdjml.Interval.pos0_len(1, 5), False)
        self.assertEqual(s2, 'ctgac')
        s2 = vdjml.trim_complement(s1, vdjml.Interval.pos0_len(1, 5), True)
        self.assertEqual(s2, 'gtcag')

if __name__ == '__main__': unittest.main()
