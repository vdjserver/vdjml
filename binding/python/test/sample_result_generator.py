#!/usr/bin/env python

'''
vdjml/binding/python/test/sample_result_generator.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2014
'''

import vdjml

def get_results(meta, n_max):
    '''
    Example of result generator.
    VDJ alignment results are obtained from some source, e.g., a file.
    This generator recycles same results.
    It will stop after creating n_max results. 
    '''
    factory = vdjml.Result_factory(meta)
    
    factory.set_default_aligner(
                             name='V-QUEST', 
                             version='3.2.32', 
                             parameters='', 
                             run_id=0
                             )
    
    factory.set_default_gl_database(
                                 name='IGHV', 
                                 version='123-0', 
                                 species='Homo Sapiens', 
                                 url='http://www.imgt.org'
                                 )
    
    factory.set_default_num_system(vdjml.Num_system.imgt)

    n = 0
    while n < n_max: #or check for the end of source file
        n += 1
        rb = factory.new_result('READ_ID_' + str(n))
        smb1 = rb.insert_segment_match(
                            read_pos0 = 1,
                            btop='61AC10A-136',
                            vdj='V',
                            seg_name='IGHV3-21*01',
                            gl_pos0=22,
                            metric=vdjml.Match_metrics(
                                                       identity=96.0,
                                                       score=264, 
                                                       substitutions=11,
                                                       insertions=0, 
                                                       deletions=0,
                                                       stop_codon=False,
                                                       mutated_invariant=False,
                                                       inverted=False
                                                    )
                                     )
        smb1.insert_aa_substitution(
                                 read_pos0=60, 
                                 read_aa='P', 
                                 gl_aa='T'
                                 )
        
        smb1a = rb.insert_segment_match(
                            read_pos0 = 1,
                            btop='61AC10A-136',
                            vdj='V',
                            seg_name='IGHV3-21*02',
                            gl_pos0=22,
                            metric=vdjml.Match_metrics(
                                                       identity=96.0,
                                                       score=264, 
                                                       mutated_invariant=False,
                                                       inverted=False
                                                    )
                                        )
        #segment matches smb1 and smb1a have same BTOPs and same read ranges
        #therefore they should be merged automatically
        
        smb2 = rb.insert_segment_match(
                            read_pos0 = 275,
                            btop='20',
                            vdj='D',
                            seg_name='IGHD3-22*01',
                            gl_pos0=11,
                            metric=vdjml.Match_metrics(100, 22)
                                       )
        
        smb3 = rb.insert_segment_match(
                            read_pos0 = 311,
                            btop='5AC35',
                            vdj='J',
                            seg_name='IGHJ4*02',
                            gl_pos0=7,
                            metric=vdjml.Match_metrics(97.6, 40)
                                       )
        
        scb = rb.insert_segment_combination(
                                          smb1.get().id(),
                                          smb2.get().id(),
                                          smb3.get().id() 
                                          )
        scb.insert_region(
                        name='FR1',
                        read_range=vdjml.Interval.first_last1(1,54),
                        metric=vdjml.Match_metrics(100.0, 54)
                       )
        scb.insert_region(
                        name='CDR1',
                        read_range=vdjml.Interval.first_last1(55,78),
                        metric=vdjml.Match_metrics(83.3, 24, 4)
                       )
        scb.insert_region(
                        name='FR2',
                        read_range=vdjml.Interval.first_last1(79,129),
                        metric=vdjml.Match_metrics(98, 59, 1)
                       )
        
        #critical piece: this is what makes this function a generator
        yield rb.release() 
        