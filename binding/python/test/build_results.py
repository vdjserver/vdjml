#!/usr/bin/env python

'''
vdjml/binding/python/test/build_results.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2014
'''

import unittest
import vdjml

class Test(unittest.TestCase):
    def test_1(self):
        # create results metainformation object
        meta = vdjml.Results_meta()

        # create results factory
        fact = vdjml.Result_factory(meta)

        fact.set_default_aligner(
                                 name='V-QUEST',
                                 version='3.2.32',
                                 parameters='',
                                 run_id=0
                                 )

        fact.set_default_gl_database(
                                     name='IGHV',
                                     version='123-0',
                                     species='Homo Sapiens',
                                     url='http://www.imgt.org'
                                     )

        fact.set_default_num_system(vdjml.Num_system.imgt)

        # create result builder
        rb1 = fact.new_result(read_id='Y14934')

        smb1 = rb1.insert_segment_match(
                                    read_pos0=1,
                                    btop='61AC10A-136',
                                    vdj='V',
                                    seg_name='IGHV3-21*01',
                                    gl_pos0=22,
                                    metric=vdjml.Match_metrics(
                                                               identity=96.0,
                                                               score=264,
                                                               stop_codon=False,
                                                               mutated_invariant=False,
                                                               inverted=False
                                                               )
                                     )
        smb1.insert_aa_substitution(
                                 read_pos0=60,
                                 read_aa='P',
                                 gl_aa='T'
                                 )

        smb1a = rb1.insert_segment_match(
                                    read_pos0=1,
                                    btop='61AC10A-136',
                                    vdj='V',
                                    seg_name='IGHV3-21*02',
                                    gl_pos0=22,
                                    metric=vdjml.Match_metrics(
                                                               identity=96.0,
                                                               score=264
                                                               )
                                     )
        # segment matches smb1 and smb1a have same BTOPs and same read ranges
        # therefore they should be merged automatically
        self.assertEqual(smb1.get().id(), smb1a.get().id())

        smb2 = rb1.insert_segment_match(
                                    read_pos0=275,
                                    btop='20',
                                    vdj='D',
                                    seg_name='IGHD3-22*01',
                                    gl_pos0=11,
                                    metric=vdjml.Match_metrics(100, 22)
                                     )

        smb3 = rb1.insert_segment_match(
                                    read_pos0=311,
                                    btop='5AC35',
                                    vdj='J',
                                    seg_name='IGHJ4*02',
                                    gl_pos0=7,
                                    metric=vdjml.Match_metrics(97.6, 40)
                                     )
                                        
        scb = rb1.insert_segment_combination(
                                          smb1.get().id(),
                                          smb2.get().id(),
                                          smb3.get().id()
                                          )
        scb.insert_region(
                        name='FR1',
                        read_range=vdjml.Interval.first_last1(1, 54),
                        metric=vdjml.Match_metrics(100.0, 54)
                       )
        scb.insert_region(
                        name='CDR1',
                        read_range=vdjml.Interval.first_last1(55, 78),
                        metric=vdjml.Match_metrics(83.3, 24, 4)
                       )
        scb.insert_region(
                        name='FR2',
                        read_range=vdjml.Interval.first_last1(79, 129),
                        metric=vdjml.Match_metrics(98, 59, 1)
                       )

        rrw1 = vdjml.Vdjml_writer('out/temp/py_out_2.vdjml', meta)
        rrw1(rb1.get())

if __name__ == '__main__': unittest.main()
