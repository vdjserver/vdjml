#!/usr/bin/env python

'''
vdjml/binding/python/test/codon_iterator.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2014
'''

import unittest
import vdjml

class Test(unittest.TestCase):
        
    def test_iterate_all(self):
        '''
           012345678910 13 16
           01234567  89
           ..C.G...--GATG... read
           ..A.-...AGC--C... gl
           0123 456789  10
        '''
        b = vdjml.Btop('2CA1G-3-A-GGCA-T-GC3')
        rs = 'acCgGgtaGATGtgc'
        gs = 'acAggtaAGCCtgc'
        
        cl = [c for c in vdjml.codons(b, read_start=0, read_seq=rs)]
        self.assertEqual(len(cl), 5)
        self.assertEqual(cl[0].read_pos(), 0)
        self.assertEqual(cl[2].read_pos(), 6)
        self.assertEqual(cl[2].gl_pos(), 5)
        self.assertEqual(cl[4].read_pos(), 10)
        self.assertEqual(cl[4].gl_pos(), 10)
        
        i = vdjml.codons(b, read_start=1, gl_seq=gs)
        c = i.next()
        self.assertEqual(c.read_pos(), 1)
        self.assertEqual(c.gl_pos(), 1)
        self.assertEqual(c.read_pos(1), 2)
        self.assertEqual(c.gl_pos(2), 3)
        self.assertEqual(c.read_char(0), 'c')
        self.assertEqual(c.gl_char(0), 'c')
        self.assertEqual(c.read_nuc(0), vdjml.Nucleotide.Cytosine)
        self.assertEqual(c.gl_nuc(0), vdjml.Nucleotide.Cytosine)
        self.assertTrue(c.is_read_translatable())
        self.assertTrue(c.is_gl_translatable())
        self.assertEqual(c.translate_read(), vdjml.Aminoacid.Pro)
        self.assertEqual(c.translate_gl(), vdjml.Aminoacid.Gln)
        self.assertFalse(c.is_match())
        self.assertFalse(c.is_silent())

        #Ggt
        #-gt
        c = i.next()
        self.assertEqual(c.read_pos(), 4)
        self.assertEqual(c.gl_pos(), 4)
        self.assertEqual(c.read_pos(1), 5)
        self.assertEqual(c.gl_pos(2), 5)
        self.assertEqual(c.read_char(0), 'G')
        self.assertEqual(c.gl_char(0), '-')
        self.assertEqual(c.read_nuc(0), vdjml.Nucleotide.Guanine)
        self.assertEqual(c.gl_nuc(0), vdjml.Nucleotide.Gap)
        self.assertTrue(c.is_read_translatable())
        self.assertFalse(c.is_gl_translatable())
        self.assertEqual(c.translate_read(), vdjml.Aminoacid.Gly)
        self.assertFalse(c.is_match())
        
    def test_iterate_read(self):
        '''
           012345678910 13 16
           01234567  89
           ..C.G...--GATG... read
           ..A.-...AGC--C... gl
           0123 456789  10
        '''
        b = vdjml.Btop('2CA1G-3-A-GGCA-T-GC3')
        rs = 'acCgGgtaGATGtgc'
        gs = 'acAggtaAGCCtgc'
        
        i = vdjml.codons(b, read_start=1, read_seq=rs, follow_gl=True)
        c = i.next()
        self.assertEqual(c.read_pos(), 1)
        self.assertEqual(c.gl_pos(), 1)
        self.assertEqual(c.read_pos(1), 2)
        self.assertEqual(c.gl_pos(2), 3)
        self.assertEqual(c.read_char(0), 'c')
        self.assertEqual(c.gl_char(0), 'c')
        self.assertEqual(c.read_nuc(0), vdjml.Nucleotide.Cytosine)
        self.assertEqual(c.gl_nuc(0), vdjml.Nucleotide.Cytosine)
        self.assertTrue(c.is_read_translatable())
        self.assertTrue(c.is_gl_translatable())
        self.assertEqual(c.translate_read(), vdjml.Aminoacid.Pro)
        self.assertEqual(c.translate_gl(), vdjml.Aminoacid.Gln)
        self.assertFalse(c.is_match())
        self.assertFalse(c.is_silent())

        #gta
        #gta
        c = i.next()
        self.assertEqual(c.read_pos(), 5)
        self.assertEqual(c.gl_pos(), 4)
        self.assertEqual(c.read_pos(1), 6)
        self.assertEqual(c.gl_pos(2), 6)
        self.assertEqual(c.read_char(0), 'g')
        self.assertEqual(c.gl_char(0), 'g')
        self.assertEqual(c.read_nuc(0), vdjml.Nucleotide.Guanine)
        self.assertEqual(c.gl_nuc(0), vdjml.Nucleotide.Guanine)
        self.assertTrue(c.is_read_translatable())
        self.assertTrue(c.is_gl_translatable())
        self.assertEqual(c.translate_read(), vdjml.Aminoacid.Val)
        self.assertEqual(c.translate_gl(), vdjml.Aminoacid.Val)
        self.assertTrue(c.is_match())
        self.assertTrue(c.is_silent())


if __name__ == '__main__': unittest.main()
