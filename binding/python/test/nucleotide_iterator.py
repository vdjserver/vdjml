#!/usr/bin/env python

'''
vdjml/binding/python/test/nucleotide_iterator.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2014
'''

import unittest, sys
import vdjml

class Test(unittest.TestCase):
        
    def test_mismatches_1(self):
        '''
           0123456789
           .....AA... read
           .....C-... gl
           012345 678
        '''
        b = vdjml.Btop('5ACA-3')
        
        for mism in vdjml.mismatches(b):
            print mism
            
        ml = [m for m in vdjml.mismatches(b)] # list of mismatches
        self.assertEqual(len(ml), 2)
        self.assertEqual(ml[0].read_pos(), 5)
        self.assertEqual(ml[0].gl_pos(), 5)
        self.assertFalse(ml[0].is_insertion())
        self.assertFalse(ml[0].is_deletion())
        
        self.assertEqual(ml[1].read_pos(), 6)
        self.assertEqual(ml[1].gl_pos(), 6)
        self.assertEqual(ml[1].read_pos(), 6)
        self.assertTrue(ml[1].is_insertion())
        self.assertFalse(ml[1].is_deletion())
        
    def test_mismatches_2(self):
        '''
           01234567  89
           ..C.G...--GATG... read
           ..A.-...AGC--C... gl
           0123 45 678  9
        '''
        b = vdjml.Btop('2CA1G-3-A-GGCA-T-GC3')
        ml = [m for m in vdjml.mismatches(b)] # list of mismatches
        self.assertEqual(len(ml), 8)
        self.assertFalse(ml[0].is_insertion())
        self.assertTrue(ml[1].is_insertion())
        
        mi = [m for m in vdjml.mismatches(b) if m.is_insertion()] # list of insertions
        self.assertEqual(len(mi), 3)
        
        md = [m for m in vdjml.mismatches(b) if m.is_deletion()] # list of deletions
        self.assertEqual(len(md), 2)
        
        # list of insertions from read 6 to gl 11 
        mi = [
              m for m in vdjml.mismatches(b) 
              if m.is_insertion() and m.read_pos() > 6 and m.gl_pos() < 11 
              ] 
        self.assertEqual(len(mi), 2)

    def test_nucleotides(self):
        '''
           012345678910 13 16
           01234567  89
           ..C.G...--GATG... read
           ..A.-...AGC--C... gl
           0123 456789  10
        '''
        b = vdjml.Btop('2CA1G-3-A-GGCA-T-GC3')
        ml = [m for m in vdjml.nucleotides(b)] # list of mismatches
        self.assertEqual(len(ml), 17)
        self.assertTrue(ml[0].is_match())
        self.assertTrue(ml[1].is_match())
        self.assertTrue(ml[5].is_match())
        self.assertTrue(ml[6].is_match())
        self.assertTrue(ml[7].is_match())
        self.assertTrue(ml[16].is_match())
        self.assertFalse(ml[2].is_insertion())
        self.assertFalse(ml[2].is_match())
        self.assertTrue(ml[4].is_insertion())
        
        mi = [m for m in vdjml.nucleotides(b) if m.is_insertion()] # list of insertions
        self.assertEqual(len(mi), 3)
        
        md = [m for m in vdjml.nucleotides(b) if m.is_deletion()] # list of deletions
        self.assertEqual(len(md), 2)
        
        # list of insertions from read 6 to gl 11 
        mi = [
              m for m in vdjml.nucleotides(b) 
              if m.is_insertion() and m.read_pos() > 6 and m.gl_pos() < 11 
              ] 
        self.assertEqual(len(mi), 2)

    def test_nucleotides_error(self):
        b = vdjml.Btop('1ACA-2')
        ml = [m for m in vdjml.nucleotides(b, read_seq='gAAtc')]
        try:
            [m for m in vdjml.nucleotides(b, gl_seq='gAAtc')]
        except RuntimeError as e:
            return
#         self.assertRaises(
#                           RuntimeError, 
#                           [m for m in vdjml.nucleotides(b, gl_seq='gAAtc')]
#                           )
        self.fail('error not thrown')

    def test_nucleotides_s(self):
        '''
           012345678910 13 16
           01234567  8910 13
           ..C.G...--GATG... read
           ..A.-...AGC--C... gl
           0123 456789  10
        '''
        b = vdjml.Btop('2CA1G-3-A-GGCA-T-GC3')
        rs = 'acCgGgtaGATGtgc'
        gs = 'acAggtaAGCCtgc'
        ml = [m for m in vdjml.nucleotides(b, read_seq=rs)]
        self.assertEqual(len(ml), 17)
        
        ml = [m for m in vdjml.nucleotides(b, gl_seq=gs)]
        self.assertEqual(len(ml), 17)
        self.assertEqual(str(ml[0]), '0aa0')
        self.assertEqual(str(ml[2]), '2CA2')
        self.assertEqual(str(ml[12]), '10T-10')
        self.assertEqual(str(ml[16]), '14cc13')

if __name__ == '__main__': unittest.main()
