#!/usr/bin/env python

'''
vdjml/binding/python/test/vdjml_reader.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2014
'''

import unittest
import vdjml

class Test(unittest.TestCase):
    def test_1(self):
        vr = vdjml.Vdjml_reader('sample_data/sample_02.vdjml')
        self.assertEqual(vr.version(), 1000)
        self.assertEqual(vr.version_str(), '1.0')
        meta = vr.meta()
        while vr.has_result():
            res = vr.result();
            print res.id()
            self.assertGreaterEqual(len(res.segment_matches()), 3)
            self.assertGreaterEqual(len(res.segment_combinations()), 1)
            seg_comb = res.segment_combinations()[0]
            self.assertGreater(len(seg_comb.segments()), 2)
            self.assertGreater(len(seg_comb.regions()), 4)
            for sm in seg_comb.segments():
                print res[sm]
            for gene_region in seg_comb.regions():
                print meta[gene_region.region_type()]
            vr.next()

        for al_info in meta.aligner_map(): print al_info.version()


if __name__ == '__main__': unittest.main()
