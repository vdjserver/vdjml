#!/usr/bin/env python

'''
vdjml/binding/python/test/read_result.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2014
'''

import unittest
import vdjml
from sample_result_generator import *

class Test(unittest.TestCase):
    def test_1(self):
        meta = vdjml.Results_meta()
        result = get_results(meta, 1).next();
        for sc in result.segment_combinations():
            for id in sc.segments():
                segment_match = result[id]
                for gls_match in segment_match.gl_segments():
                    print meta[gls_match.gl_segment()].name()
        
if __name__ == '__main__': unittest.main()
