#!/usr/bin/env python

'''
vdjml/binding/python/test/gl_segment_match.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Mikhail K Levin 2013-4
'''

import unittest
import vdjml

class Test(unittest.TestCase):
    def test_1(self):
        meta = vdjml.Results_meta()
        db_id = meta.insert(
                            vdjml.Gl_db_info(
                                            'IGHV', '123-0', 'Homo Sapiens',
                                            'http://www.imgt.org'
                                            )
                            )

        al_id = meta.insert(vdjml.Aligner_info('V-QUEST', '3.2.32'))

        seg_id1 = meta.insert(
                              vdjml.Gl_segment_info(db_id, 'V', 'IGHV3-21*01')
                              )

        glsm = vdjml.Gl_segment_match(
                                      num_system=vdjml.Num_system.imgt,
                                      aligner=al_id,
                                      germline_segment=seg_id1,
                                      gl_pos0=22
                                      )

        self.assertEqual(vdjml.segment_name(glsm, meta), 'IGHV3-21*01')
        self.assertEqual(vdjml.numbering_system(glsm, meta), 'IMGT')
        self.assertEqual(vdjml.segment_type(glsm, meta), 'V')

if __name__ == '__main__': unittest.main()
