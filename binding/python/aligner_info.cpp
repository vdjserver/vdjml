/** @file "/vdjml/binding/python/aligner_info.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/aligner_info.hpp"
using vdjml::Aligner_info;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_aligner_info() {

   /**
    * Aligner_info
    */
   bp::class_<Aligner_info>(
      "Aligner_info",
      "Info about aligner software",
      bp::init<
         std::string const&,
         std::string const&,
         std::string const&,
         std::string const&,
         const unsigned
      >(
               (
                        bp::arg("name"),
                        bp::arg("version"),
                        bp::arg("parameters") = "",
                        bp::arg("uri") = "",
                        bp::arg("run_id") = 0
               )
      )
   )
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   .def("id", &Aligner_info::id, "Aligner software ID")
   .def("name", &Aligner_info::name, return_ref(), "Aligner software name")
   .def("version", &Aligner_info::version, return_ref(), "Aligner software version")
   .def("parameters", &Aligner_info::parameters, return_ref(), "Parameters used for the alignment")
   .def("uri", &Aligner_info::uri, return_ref(), "Aligner software URI")
   .def("run_id", &Aligner_info::run_id, "Aligner software run id")
   ;
}
