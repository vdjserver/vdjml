/** @file "/vdjml/binding/python/germline_db_info.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/germline_db_info.hpp"
using vdjml::Gl_db_info;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_germline_db_info() {

   /**
    * Gl_db_info
    */
   bp::class_<Gl_db_info>(
      "Gl_db_info",
      "Info about germline sequences database",
      bp::init<
      std::string const&,
      std::string const&,
      std::string const&,
      std::string const&
      >(
               (
                        bp::arg("name"),
                        bp::arg("version"),
                        bp::arg("species"),
                        bp::arg("url")
               )
      )
   )
   .def("id", &Gl_db_info::id, "germline database ID")
   .def(
      "name", &Gl_db_info::name,
      return_ref(),
      "germline database name"
   )
   .def(
      "version",
      &Gl_db_info::version,
      return_ref(),
      "germline database version"
   )
   .def(
      "species",
      &Gl_db_info::species,
      return_ref(),
      "germline database species"
   )
   .def(
      "uri",
      &Gl_db_info::uri,
      return_ref(),
      "germline database URI"
   )
   ;

}

