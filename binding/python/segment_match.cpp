/** @file "/vdjml/binding/python/segment_match.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/segment_match_map.hpp"
#include "vdjml/results_meta.hpp"
using vdjml::Aa_substitution;
using vdjml::Aligned_pos;
using vdjml::aminoacid::Aa;
using vdjml::Btop;
using vdjml::Gl_seg_match_id;
using vdjml::Gl_segment_match;
using vdjml::Interval;
using vdjml::Match_metrics;
using vdjml::Nucleotide_match;
using vdjml::Results_meta;
using vdjml::Segment_match_map;
using vdjml::Segment_match;
typedef Segment_match::germline_segment_map gl_segment_map;
typedef Segment_match::aa_substitution_set aa_substitutions;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

void export_segment_match() {

   /**
    * Aa_substitution
    */
   bp::class_<Aa_substitution>(
      "Aa_substitution",
      "amino acid substitution",
      bp::init<const unsigned, const Aa, const Aa>(
       (bp::arg("read_pos0"), bp::arg("read_aa"), bp::arg("gl_aa"))
      )
   )
   .def(
         bp::init<const unsigned, std::string const&, std::string const&>(
               (bp::arg("read_pos0"), bp::arg("read_aa"), bp::arg("gl_aa"))
         )
   )
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   .def(
            "read_position",
            &Aa_substitution::read_position,
            "0-based read position of the codone's first nucleotide"
   )
   .def(
            "read_aa",
            &Aa_substitution::read_aa,
            "amino acid encoded by the read sequence"
   )
   .def(
            "gl_aa",
            &Aa_substitution::gl_aa,
            "amino acid encoded by the germline sequence"
   )
   ;

   /**
    * Segment_match::aa_substitution_set
    */
   bp::class_<aa_substitutions>(
      "Aa_substitutions_set",
      "Set of amino acid substitutions",
      bp::no_init
   )
   .def("__len__", &aa_substitutions::size, "todo:")
   .def("empty", &aa_substitutions::empty, "Indicates if there are AA substitutions or not")
   .def(
      "__getitem__",
      static_cast<
         Aa_substitution const&(aa_substitutions::*)(const std::size_t) const
      >(&aa_substitutions::operator[]),
      bp::return_internal_reference<>(),
      "amino acid substitution"
   )
   .def(
      "__iter__",
      bp::range<return_ref>(
         static_cast<
            aa_substitutions::const_iterator (aa_substitutions::*)() const
         >(&aa_substitutions::begin),
         static_cast<
            aa_substitutions::const_iterator (aa_substitutions::*)() const
         >(&aa_substitutions::end)
      )
   )
   ;

   /**
    * Gl_segment_match
    */
   bp::class_<Gl_segment_match>(
      "Gl_segment_match",
      "Alignment to germline segment",
      bp::init<
         const vdjml::Numsys_id,
         const vdjml::Aligner_id,
         const vdjml::Gl_seg_id,
         const unsigned
      >(
               (
                        bp::arg("num_system"),
                        bp::arg("aligner"),
                        bp::arg("germline_segment"),
                        bp::arg("gl_pos0")
               )
      )
   )
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   .def("id", &Gl_segment_match::id, "germline segment match ID")
   .def(
            "num_system",
            &Gl_segment_match::num_system,
            "germline segment numbering system ID"
   )
   .def(
            "aligner",
            &Gl_segment_match::aligner,
            "ID of the software that aligned the germline segment"
   )
   .def(
            "gl_segment",
            &Gl_segment_match::gl_segment,
            "germline segment ID"
   )
   .def(
            "gl_position",
            &Gl_segment_match::gl_position,
            "first aligned position index of the germline segment"
   )
   ;

   /**
    *
    */
   bp::def(
            "segment_name",
            &vdjml::segment_name,
            return_ref(),
            (
                     bp::arg("gl_segment_match"),
                     bp::arg("meta")
            ),
            "return segment name"
   );

   /**
    *
    */
   bp::def(
            "numbering_system",
            &vdjml::numbering_system,
            return_ref(),
            (
                     bp::arg("gl_segment_match"),
                     bp::arg("meta")
            ),
            "return numbering system name"
   );

   /**
    *
    */
   bp::def(
            "segment_type",
            static_cast<
               char(*)(Gl_segment_match const&, Results_meta const&)
            >(&vdjml::segment_type),
            (
                     bp::arg("gl_segment_match"),
                     bp::arg("meta")
            ),
            "return numbering system name"
   );

   /**
    * Segment_match::germline_segment_map
    */
   bp::class_<gl_segment_map>(
      "Gl_segment_map",
      "Map of germline segments aligned to read interval",
      bp::no_init
   )
   .def("__len__", &gl_segment_map::size, "todo:")
   .def("empty", &gl_segment_map::empty, "Indicates if there are germline segments or not")
   .def(
         "__getitem__",
         static_cast<
            Gl_segment_match const&
            (gl_segment_map::*)
            (const Gl_seg_match_id) const
         >(&gl_segment_map::operator[]),
         bp::return_internal_reference<>(),
         "germline segment match"
   )
   .def(
      "__iter__",
      bp::range<bp::return_value_policy<bp::copy_const_reference> >(
         static_cast<
            gl_segment_map::const_iterator (gl_segment_map::*)() const
         >(&gl_segment_map::begin),
         static_cast<
            gl_segment_map::const_iterator (gl_segment_map::*)() const
         >(&gl_segment_map::end)
      )
   )
   ;

   /**
    * Segment_match
    */
   bp::class_<Segment_match>(
      "Segment_match",
      "Alignment results for a read segment",
      bp::init<
         const std::size_t,
         vdjml::Btop const&,
         Match_metrics const&
      >(
         (
            bp::arg("read_pos0"),
            bp::arg("btop"),
            bp::arg("match_metrics") = Match_metrics()
         )
      )
   )
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   .def("id", &Segment_match::id, "segment match ID")
   .def(
      "btop",
      &Segment_match::btop,
      bp::return_internal_reference<>(),
      "BTOP alignment description"
   )
   .def(
      "read_range",
      &Segment_match::read_range,
      bp::return_internal_reference<>(),
      "read sequence nucleotide range that matches to germline segment"
   )
   .def(
      "gl_range",
      static_cast<
         Interval (Segment_match::*)() const
      >(&Segment_match::gl_range),
      "nucleotide range of the first germline sequence that matches the read sequence"
   )
   .def(
      "gl_range",
      static_cast<
         Interval (Segment_match::*)(Gl_segment_match const&) const
      >(&Segment_match::gl_range),
      "nucleotide range of the specified germline sequence that matches the read sequence"
   )
   .def(
      "match_metrics",
      &Segment_match::match_metrics,
      bp::return_internal_reference<>(),
      "alignment metrics"
   )
   .def(
      "gl_segments",
      &Segment_match::gl_segments,
      bp::return_internal_reference<>(),
      "germline segment map"
   )
   .def(
      "aa_substitutions",
      &Segment_match::aa_substitutions,
      bp::return_internal_reference<>(),
      "amino acid substitutions"
   )
   .def(
      "gl_length",
      &Segment_match::gl_length,
      "length of the aligned germline segment(s)"
   )
   .def(
      "insert",
      static_cast<
      Gl_seg_match_id (Segment_match::*)(Gl_segment_match const&)
      >(&Segment_match::insert),
      "insert germline segment match"
   )
   .def(
      "insert",
      static_cast<
      void (Segment_match::*)(Aa_substitution const&)
      >(&Segment_match::insert),
      "insert amino acid substitution"
   )
/*
   .def(
      "gl_position",
      static_cast<
         Segment_match::length_t (Segment_match::*)(const unsigned) const
      >(&Segment_match::gl_position),
      "return nucleotide position for first germline segment match that aligns "
      "to read_pos0",
      (bp::arg("read_pos0"))
   )
   .def(
      "gl_position",
      static_cast<
         Segment_match::length_t (Segment_match::*)
         (const unsigned, Gl_segment_match const&) const
      >(&Segment_match::gl_position),
      "return germline gene nucleotide position that aligns to read_pos",
      (bp::arg("read_pos0"), bp::arg("gl_segment_match"))
   )
*/
   ;

   /**
    * nucleotide_match()
    */
   bp::def(
      "nucleotide_match",
      static_cast<Nucleotide_match (*)(
               Segment_match const&, Aligned_pos, Gl_segment_match const&)>
      (&vdjml::nucleotide_match),
      "Generate :class:`Nucleotide_match`, information about two aligned ",
      (
               bp::arg("sm"),
               bp::arg("pos0"),
               bp::arg("gsm")
      )
   );

   /**
    * nucleotide_match()
    */
   bp::def(
      "nucleotide_match",
      static_cast<Nucleotide_match (*)(
               Segment_match const&, Aligned_pos const&)>
      (&vdjml::nucleotide_match),
      "Generate :class:`Nucleotide_match`, information about two aligned ",
      (
               bp::arg("sm"),
               bp::arg("pos0")
      )
   );

}
