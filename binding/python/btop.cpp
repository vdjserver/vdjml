/** @file "/vdjml/binding/python/btop.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "boost/python.hpp"
namespace bp = boost::python;

#include "vdjml/btop.hpp"
#include "vdjml/nucleotide_match.hpp"
#include "vdjml/trim_complement.hpp"
using vdjml::Btop_stats;
using vdjml::Btop;
using vdjml::Nucleotide_match;
using vdjml::seq::GL;
using vdjml::seq::Read;
using vdjml::seq::Seq;
using vdjml::Sequence_match;
using vdjml::sequence_match;
typedef bp::return_value_policy<bp::copy_const_reference> return_ref;

namespace vdjml{ namespace{

/**
*******************************************************************************/
template<typename T> struct Array_to_tuple {
   static PyObject* convert(boost::array<T,2> const& a) {
      return bp::incref(
               bp::make_tuple(a[0], a[1]).ptr()
      );
   }
};

/**
*******************************************************************************/
Nucleotide_match call_nucleotide_match(
         Btop const& btop,
         const std::size_t read_pos0,
         const std::size_t gl_pos0,
         std::string const& read_seq,
         std::string const& gl_seq,
         char const match_char
) {
   Aligned_pos pos = Aligned_pos::read(0);
   if( read_pos0 != Btop::unset ) pos = Aligned_pos::read(read_pos0);
   else if( gl_pos0 != Btop::unset ) pos = Aligned_pos::gl(gl_pos0);
   if( read_seq.size() ) {
      return vdjml::nucleotide_match(btop, pos, Aligned_seq::read(read_seq));
   }
   if( gl_seq.size() ) {
      return vdjml::nucleotide_match(btop, pos, Aligned_seq::gl(gl_seq));
   }
   return vdjml::nucleotide_match(btop, pos, match_char);
}

/*
*******************************************************************************/
Sequence_match call_sequence_match(
         Btop const& btop,
         const std::size_t read_start,
         const std::size_t read_end,
         const std::size_t gl_start,
         const std::size_t gl_end,
         std::string const& read_seq,
         std::string const& gl_seq,
         char const match_char
) {
   Aligned_pos start;
   if( read_start != Btop::unset ) start = Aligned_pos::read(read_start);
   else if( gl_start != Btop::unset ) start = Aligned_pos::gl(gl_start);

   Aligned_pos end;
   if( read_end != Btop::unset ) end = Aligned_pos::read(read_end);
   else if( gl_end != Btop::unset ) end = Aligned_pos::gl(gl_end);

   if( read_seq.size() ) {
      return vdjml::sequence_match(btop, start, end, Aligned_seq::read(read_seq));
   }
   if( gl_seq.size() ) {
      return vdjml::sequence_match(btop, start, end, Aligned_seq::gl(gl_seq));
   }

   return sequence_match(btop, start, end, match_char);
}

/**
*******************************************************************************/
std::string call_trim_complement(
         std::string const& seq,
         vdjml::Interval const& si,
         const bool reverse
) {
   return trim_complement(seq, si, reverse);
}

}//anonymous namespace
}//namespace vdjml
/**
*******************************************************************************/
void export_btop() {

   /**
    * Nucleotide_match
    */
   bp::class_<Nucleotide_match>(
      "Nucleotide_match",
      "Information about a pair of aligned nucleotides, which may be a match, "
      "substitution, insertion, or deletion",
      bp::no_init
   )
   .def(str(bp::self))
   .def(
            "read_pos",
            &Nucleotide_match::read_pos,
            "return position of nucleotide in read sequence; "
            "if a deletion, return position of the first "
            "nucleotide to the right, if at end of sequence, return sequence "
            "length"
   )
   .def(
            "gl_pos",
            &Nucleotide_match::gl_pos,
            "return position of nucleotide in germline sequence; "
            "if an insertion, return position of the first "
            "nucleotide to the right, if at end of sequence, return sequence "
            "length"
   )
   .def(
            "read_nuc",
            &Nucleotide_match::read_char,
            "return nucleotide character in read sequence; "
            "if the mismatch is a deletion, return '-'"
   )
   .def(
            "gl_nuc",
            &Nucleotide_match::gl_char,
            "return nucleotide character in germline sequence; "
            "if the mismatch is an insertion, return '-'"
   )
   .def(
            "is_insertion",
            &Nucleotide_match::is_insertion,
            "return True if insertion (in read sequence)"
   )
   .def(
            "is_deletion",
            &Nucleotide_match::is_deletion,
            "return True if a deletion (from read sequence)"
   )
   .def(
            "is_match",
            &Nucleotide_match::is_match,
            "return True if mismatch"
   )
   ;

   /**
    * Btop
    */
   bp::class_<Btop>(
      "Btop",
      "Blast trace-back operations, alignment description",
      bp::init<std::string const&>("create Btop from string")
   )
   .def(str(bp::self))
   .def(boost::python::self < boost::python::self)
   .def(boost::python::self > boost::python::self)
   .def(boost::python::self <= boost::python::self)
   .def(boost::python::self >= boost::python::self)
   .def(boost::python::self == boost::python::self)
   .def(boost::python::self != boost::python::self)
   .def("empty", &Btop::empty, "return True if Btop is empty")
   ;

   /**
    * Btop_stats
    */
   bp::class_<Btop_stats>(
      "Btop_stats",
      "Btop_statistics",
      bp::init<Btop const&>("btop")
   )
   .def_readonly(
            "substitutions_",
            &Btop_stats::substitutions_,
            "number of substitutions"
   )
   .def_readonly(
            "insertions_",
            &Btop_stats::insertions_,
            "number of insertions (in read sequence)"
   )
   .def_readonly(
            "deletions_",
            &Btop_stats::deletions_,
            "number of deletions (from read sequence)"
   )
   .def_readonly(
            "read_len_",
            &Btop_stats::read_len_,
            "length of the aligned read sequence"
   )
   .def_readonly(
            "gl_len_",
            &Btop_stats::gl_len_,
            "length of the aligned germline sequence"
   )
   .def(
            "matches",
            &Btop_stats::matches,
            "number of matches in the alignment"
   )
   ;


   /**
    * nucleotide_match()
    */
   bp::def(
      "nucleotide_match",
      &vdjml::call_nucleotide_match,
      (
               bp::arg("btop"),
               bp::arg("read_pos0")    = Btop::unset,
               bp::arg("gl_pos0")      = Btop::unset,
               bp::arg("read_seq")     = "",
               bp::arg("gl_seq")       = "",
               bp::arg("match_char")   = '.'
      ),
      "Provides information about a pair of aligned nucleotides"
      "\n\n"
      ":param btop: :class:`Btop` BTOP structure" "\n"
      ":param read_pos0: 0-based position relative to read sequence\n"
      ":param gl_pos0: 0-based position relative to germline sequence\n"
      ":param read_seq: read sequence\n"
      ":param gl_seq: germline sequence\n"
      ":param match_char: :class:`char`, character to indicate matching "
      "nucleotides" "\n"
      ":return: :class:`Nucleotide_match` information about two aligned nucleotides"
   );

   /**
    * Sequence_match
    */
   bp::to_python_converter<
      boost::array<std::size_t,2>,
      vdjml::Array_to_tuple<std::size_t>
   >();

   bp::to_python_converter<
      boost::array<std::string,2>,
      vdjml::Array_to_tuple<std::string>
   >();

   bp::class_<Sequence_match>(
      "Sequence_match",
      "Aligned sequences, start and end indices",
      bp::no_init
   )
   .def(str(bp::self))
   .add_property(
            "start_",
            bp::make_getter(
                     &Sequence_match::start_,
                     bp::return_value_policy<bp::return_by_value>()
            ),
            "0-based starting position for aligned read and germline sequences"
   )
   .add_property(
            "end_",
            bp::make_getter(
                     &Sequence_match::end_,
                     //return by value to use the converter
                     bp::return_value_policy<bp::return_by_value>()
            ),
            "last plus one position for aligned read and germline sequences"
   )
   .add_property(
            "seq_",
            bp::make_getter(
                     &Sequence_match::seq_,
                     //return by value to use the converter
                     bp::return_value_policy<bp::return_by_value>()
            ),
            "aligned read and germline sequences"
   )
   ;

   /**
    * sequence_match()
    */
   bp::def(
      "sequence_match",
      &vdjml::call_sequence_match,
      (
               bp::arg("btop"),
               bp::arg("read_start")   = Btop::unset,
               bp::arg("read_end")     = Btop::unset,
               bp::arg("gl_start")     = Btop::unset,
               bp::arg("gl_end")       = Btop::unset,
               bp::arg("read_seq")     = "",
               bp::arg("gl_seq")       = "",
               bp::arg("match_char")   = '.'
      ),
      "Generate a pair of aligned sequences with positions for start and end"
      "\n\n"
      ":param btop: :class:`Btop`, BTOP structure" "\n"
      ":param read_start: position for alignment start "
      "(0-based, relative to read sequence) \n"
      ":param read_end: position for alignment end "
      "(0-based, relative to read sequence) \n"
      ":param gl_start: position for alignment start "
      "(0-based, relative to germline sequence) \n"
      ":param gl_end: position for alignment end "
      "(0-based, relative to germline sequence) \n"
      ":param read_seq: read sequence\n"
      ":param gl_seq: germline sequence\n"
      ":param match_char: :class:`char`, character to indicate matching "
      "nucleotides" "\n"
      ":return:  :class:`Sequence_match`"
   );

   /**
    * trim_complement()
    */
   bp::def(
      "trim_complement",
      &vdjml::call_trim_complement,
      "Trim and optionally reverse-complement a sequence",
      (
               bp::arg("seq"),
               bp::arg("interval"),
               bp::arg("reverse")
      )
   );

}
