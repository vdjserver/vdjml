<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xs="http://www.w3.org/2001/XMLSchema" version="1.0">

<xsl:output method="text" indent="no" />

<xsl:param name="output_types" />
<xsl:param name="linked_doc" />

<xsl:template match="text()|@*" mode="attributes" />
<xsl:template match="text()|@*" mode="elements" />
<xsl:template match="text()|@*" mode="links" />

<xsl:template match="/">
   <xsl:text>
digraph VDJML {
graph [nodesep=0.2, size="7.5 11", concentrate=true]
node [shape=record, fontname="DejaVuSans",fontsize=14, penwidth=1, rankdir="TB"];
edge [fontname="DejaVuSans",fontsize=12,splines=line,labeldistance=2.5,
penwidth=1, labelangle=-30];
</xsl:text>
   <xsl:apply-templates mode="elements"/>
   <xsl:text>

edge [dir=forward, arrowhead=diamond]   
</xsl:text>   
   <xsl:apply-templates mode="links"/>
   <xsl:text>}
</xsl:text>
</xsl:template>

<xsl:template match="xs:element[@name]" mode="elements" >
   <xsl:value-of select="@name"/>
   <xsl:text>[label=&lt;{&lt;b>\N</xsl:text>
   <xsl:text>&lt;/b>&lt;br align="left"/>|</xsl:text>
   <xsl:apply-templates mode="attributes" />
   <xsl:if test=".//xs:any"><xsl:text>...</xsl:text></xsl:if>
   <xsl:text>}></xsl:text>
   <xsl:if test="$linked_doc">
      <xsl:text>, URL="./</xsl:text>
      <xsl:value-of select="$linked_doc" />
      <xsl:text>#element-</xsl:text><xsl:value-of select="@name"/>
      <xsl:text>", target="_top", tooltip="</xsl:text>
      <xsl:value-of select="@name" />
      <xsl:text>"</xsl:text>
   </xsl:if>
   <xsl:text>]

</xsl:text>
   <xsl:apply-templates mode="elements" />
</xsl:template>

<xsl:template match="xs:attribute[@name]" mode="attributes" >
   <xsl:choose>
      <xsl:when test="@use='required'">
         <xsl:text>+</xsl:text>
      </xsl:when>
      <xsl:otherwise>
         <xsl:text>&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
      </xsl:otherwise>
   </xsl:choose>
   <xsl:value-of select="@name" />
   <xsl:if test="$output_types">
      <xsl:text> : </xsl:text>
      <xsl:value-of select="@type" />
   </xsl:if>
   <xsl:text>&lt;br align="left"/>
</xsl:text>
</xsl:template>

<xsl:template match="xs:attributeGroup[@ref]" mode="attributes" >
   <xsl:param name="id" select="substring-after(@ref, ':')" />
   <xsl:apply-templates select="//xs:attributeGroup[@name=$id]" 
    mode="attributes" />
</xsl:template>

<xsl:template match="xs:element[@name]" mode="links" >
   <xsl:apply-templates mode="links">
      <xsl:with-param name="ancestor" select="@name" />
   </xsl:apply-templates>
</xsl:template>

<xsl:template match="xs:element//xs:element" mode="links" >
   <xsl:param name="ancestor" />
   <xsl:call-template name="make-link">
      <xsl:with-param name="to" select="$ancestor" />
      <xsl:with-param name="from" >
         <xsl:choose>
            <xsl:when test="@ref">
               <xsl:value-of select="substring-after(@ref, ':')" />
            </xsl:when>
            <xsl:when test="@name">
               <xsl:value-of select="@name" />
            </xsl:when>
         </xsl:choose>
      </xsl:with-param>
      <xsl:with-param name="min" >
         <xsl:choose>
            <xsl:when test="@minOccurs">
               <xsl:value-of select="@minOccurs" />
            </xsl:when>
            <xsl:when test="../@minOccurs">
               <xsl:value-of select="../@minOccurs" />
            </xsl:when>
            <xsl:when test="parent::xs:sequence">
               <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:when test="parent::xs:choice">
               <xsl:text>0</xsl:text>
            </xsl:when>
            <xsl:otherwise>
               <xsl:text>1</xsl:text>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:with-param>
      <xsl:with-param name="max" >
         <xsl:choose>
            <xsl:when test="@maxOccurs = 'unbounded'">
               <xsl:text>*</xsl:text>
            </xsl:when>
            <xsl:when test="../@maxOccurs = 'unbounded'">
               <xsl:text>*</xsl:text>
            </xsl:when>
            <xsl:when test="@maxOccurs">
               <xsl:value-of select="@maxOccurs" />
            </xsl:when>
            <xsl:when test="../@maxOccurs">
               <xsl:value-of select="../@maxOccurs" />
            </xsl:when>
            <xsl:when test="parent::xs:sequence">
               <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:when test="parent::xs:choice">
               <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
               <xsl:text>1</xsl:text>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:with-param>
   </xsl:call-template>
</xsl:template>

<xsl:template name="make-link">
   <xsl:param name="from" />
   <xsl:param name="to" />
   <xsl:param name="min" select="'42'" />
   <xsl:param name="max" select="'666'" />
   <xsl:value-of select="$from"/>
   <xsl:text> -> </xsl:text>
   <xsl:value-of select="$to"/>
   <xsl:text>[taillabel="</xsl:text>
   <xsl:choose>
      <xsl:when test="$min = $max">
         <xsl:value-of select="$min"/>
      </xsl:when>
      <xsl:otherwise>
         <xsl:value-of select="concat($min, '..', $max)"/>
      </xsl:otherwise>
   </xsl:choose>
   <xsl:text>"]</xsl:text>
   <xsl:text>
</xsl:text>
</xsl:template>
 
</xsl:stylesheet>