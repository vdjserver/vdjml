#!/usr/bin/env python

'''
Run XSL transformation, pass optional parameters
use:
xslt_harness <source XML> <XSLT> <output file> [<parameter name> <value>]*
'''

import sys, itertools, os
from lxml import etree

i = iter(itertools.islice(sys.argv, 4, None))
d = dict((n,v) for n, v in itertools.izip(i, i))

dom = etree.parse(sys.argv[1])
xsl = etree.parse(sys.argv[2])
tr = etree.XSLT(xsl)
out  = sys.argv[3]
if not os.path.exists(os.path.dirname(out)): os.makedirs(os.path.dirname(out))
open(out, 'w').write(str(tr(dom, **d)))
# print tr.error_log
