<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xs="http://www.w3.org/2001/XMLSchema" 
   xmlns:vdj="http://vdjserver.org/vdjml/xsd/1/"
   xmlns="http://www.w3.org/1999/xhtml"
   version="1.0"
>

<xsl:output method="xml" indent="yes" 
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" 
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	encoding="UTF-8" omit-xml-declaration="yes" />

<xsl:template match="text()|@*" mode="elements"/>
<xsl:template match="text()|@*" mode="attributes"/>

<xsl:template match="xs:schema">
  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <title>VDJML | XML Schema</title>
      <meta http-equiv="content-type" content="text/html;charset=utf-8" />
      <link rel="stylesheet" href="xsd.css" type="text/css" />      
    </head>
    <body>
      <h1>VDJML XML Schema</h1>
	   <p class="graphviz">
	   <embed type="image/svg+xml" src="xsd_dot.svg" alt="VDJML Diagram" width="800" />
	   </p>
      <xsl:copy-of 
         select="xs:annotation[xs:documentation][1]/xs:documentation/child::node()" 
         copy-namespaces="no"
      />
	   <div id="xs_elements">
         <xsl:apply-templates mode="elements" />
	   </div>
    </body>
  </html>
</xsl:template>

<xsl:template match="xs:element[@name]" mode="elements">
   <xsl:text>
</xsl:text>
   <xsl:element name="div" >
      <xsl:attribute name="id">
         <xsl:text>element-</xsl:text><xsl:value-of select="@name" />
      </xsl:attribute>
      <xsl:attribute name="class">element</xsl:attribute>
      <span class="element-name"><xsl:value-of select="@name" /></span>
      <xsl:apply-templates select="xs:annotation" />
      <xsl:if test=".//xs:attribute | .//xs:attributeGroup" >
         <div class="element-attributes">
            <div class="attribute-header">
               <span>attribute</span><span>type</span><span>required</span>
               <span>description</span>
            </div>
            <xsl:apply-templates mode="attributes">
               <xsl:with-param name="element-name" select="@name" />
            </xsl:apply-templates>
         </div>
      </xsl:if>
      <xsl:apply-templates mode="elements"/>
   </xsl:element>
</xsl:template>

<xsl:template match="xs:element/xs:annotation/xs:documentation">
   <div class="element-comment"><xsl:copy-of select="child::node()" /></div>
</xsl:template>

<xsl:template match="xs:attribute/xs:annotation/xs:documentation">
   <div class="attribute-comment"><xsl:copy-of select="child::node()" /></div>
</xsl:template>

<xsl:template match="xs:attribute[@name]" mode="attributes" >
   <xsl:param name="element-name" />
   <xsl:text>
</xsl:text>
   <xsl:element name="div">
      <xsl:attribute name="id">
         <xsl:value-of select="$element-name" /><xsl:text>-attribute-</xsl:text
         ><xsl:value-of select="@name" />
      </xsl:attribute>
      <xsl:attribute name="class">attribute</xsl:attribute>
      <span class="attribute-name"><xsl:value-of select="@name" /></span>
      <span class="attribute-type"><xsl:value-of select="@type" /></span>
      <span class="attribute-is-required"><xsl:if test="@use='required'">
         <xsl:text>&#10003;</xsl:text>
      </xsl:if></span>
      <xsl:apply-templates select="xs:annotation" />
   </xsl:element>
</xsl:template>

<xsl:template match="xs:attributeGroup[@ref]" mode="attributes" >
   <xsl:param name="element-name" />
   <xsl:param name="id" select="substring-after(@ref, ':')" />
   <xsl:apply-templates select="//xs:attributeGroup[@name=$id]" 
      mode="attributes">
      <xsl:with-param name="element-name" select="$element-name" />
    </xsl:apply-templates>
</xsl:template>

</xsl:stylesheet>