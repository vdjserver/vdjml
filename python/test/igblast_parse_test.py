#!/usr/bin/env python

'''
vdjml/python/test/igblast_parse_test.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Edward A. Salinas 2014
'''

import unittest
import vdjml

input_igBLAST = "sample_data/human.IG.fna.igblast.kabat.out"

class Test(unittest.TestCase):

    def setUp(self):
        '''
        setup the point to the input file
        '''
        self.fileToParse = input_igBLAST
        # print "Using IgBLAST input file ",self.fileToParse



    def test_FR1(self):
        '''
        make sure that FR1s are as expected
        this can extract some FR1 data
        grep -Pi '^FR1\s' ../../sample_data/human.IG.fna.igblast.kabat.out |cut -f2,3|tr "\n" ","|tr "\t" ","
        '''
        fr1s = [3, 92, 3, 93, 3, 92, -1, -1, 2, 49]
        rid = 0
        mf = vdjml.makeDummyMetaAndFactory()
        meta = mf[0]
        for read_result_obj in vdjml.makeVDJMLRead_ResultsFromIGBLAST(self.fileToParse, None):
            if(rid != 3):
                segment_combinations = read_result_obj.segment_combinations()
                segment_combo = segment_combinations[0]  # from the 1 and only combination
                regions = segment_combo.regions()
                for region in regions:
                    # print "looking in a region...."
                    # print region.region_type()
                    region_type = region.region_type()
                    if(region_type == vdjml.Gene_region_type.fr1):
                        # print "got it"
                        # print "rid",rid
                        expected_start = fr1s[rid * 2]
                        # print "es",expected_start
                        expected_end = fr1s[rid * 2 + 1]
                        extracted_range = region.read_range()
                        # verify that the extracted FR1 start is the same as the expected FR1 start
                        # verify that the extracted FR1 end   is the same as the expected FR1 end
                        self.assertEqual(extracted_range.pos1(), expected_start)
                        self.assertEqual(extracted_range.last1(), expected_end)

            else:
                # 3 has no hit, so skip it!
                pass
            rid += 1



    def test_btops(self):
        '''
        make sure that the BTOPs are equal
        this little awk command can get some btops...
        awk -F "\t"  '{if(NF>10) {print $0}}' ../../../sample_data/human.IG.fna.igblast.kabat.out |cut -f35|head -9
        '''
        btopid = 0
        btops = ["126CA1AG3GCCTTG79GA78",
            "14GA111CA1AG3GCCTTG79GA78",
            "126CA1AG3GCCTTG19AT59GA76",
            "24",
            "22",
            "10GC12",
            "46",
            "20GA25",
            "20GA2AG22"
            ]
        rid = 0
        for extracted_rr in vdjml.makeVDJMLRead_ResultsFromIGBLAST(self.fileToParse, None):
            if(rid == 0):
                segment_matches = extracted_rr.segment_matches()
                for segment_match in segment_matches:
                    for gls_match in segment_match.gl_segments():
                        extracted_btop = str(segment_match.btop())
                        # print "extracted a btop",extracted_btop
                        expected_btop = btops[btopid]
                        self.assertEqual(expected_btop, extracted_btop)
                        btopid += 1
            rid += 1



    # test that read_results are extracted and that the
    # number extracted is equal to the number expected
    def test_num_read_result(self):
        num_rr_expected = 5
        num_rr_extracted = 0
        for extracted_rr in vdjml.makeVDJMLRead_ResultsFromIGBLAST(
                                                            self.fileToParse,
                                                            None
                                                                ):
            num_rr_extracted += 1
        self.assertEqual(num_rr_expected, num_rr_extracted)

    def test_query_names(self):
        '''
        verify that the extracted read names are the same as the expected read names
        this little grep can be used to extract the read names ;
        maybe useful if the input file is later changed
        grep -P '^#\s*Query' ../../sample_data/human.IG.fna.igblast.kabat.out \\
        |grep -Po ':.*$'|sed -r "s/:\ //g"|tr "\n" "\t"|sed -r "s/\t/\",\"/g"
        '''
        expected_names = [
                        "AY671579.1", "AY671579.1_two_insertions", "REV_COMP",
                        "HCQLJHQ01B13E9.nohit", "H4RW71N02GF9PE"
                        ]
        rid = 0
        for extracted_rr in vdjml.makeVDJMLRead_ResultsFromIGBLAST(
                                                            self.fileToParse,
                                                            None
                                                                ):
            extracted_name = extracted_rr.id()
            self.assertEqual(extracted_name, expected_names[rid])
            rid += 1

    def test_top_combo_test(self):
        '''
        basic verification about the top combinations
        this little grep command extracts the top combos ; maybe useful if the input file is later changed
        grep -A 1 -Pi 'rearrangement\s+summary' \\
        ../sample_data/human.IG.fna.igblast.kabat.out \\
        |grep -Pv '^[#\-]'|cut -f1,2,3|sed -r "s/\t/\",\"/g"
        '''
        combos = [
            "IGHV4-34*01", "IGHD3-3*01", "IGHJ4*02",
            "IGHV4-34*01", "IGHD3-3*01", "IGHJ4*02",
            "IGHV4-34*01", "IGHD3-3*01", "IGHJ4*02",
            "", "", "",  # no hits here! so no combination!
            "IGHV3-33*01", "IGHD2-15*01", "IGHJ4*02"
            ]
        rr_num = 0
        mf = vdjml.makeDummyMetaAndFactory()
        meta = mf[0]
        for  read_result_obj in vdjml.makeVDJMLRead_ResultsFromIGBLAST(
                                                            self.fileToParse,
                                                            mf
                                                                    ):
            topVDJExpected = combos[rr_num * 3:rr_num * 3 + 3]

            segment_combinations = read_result_obj.segment_combinations()
            num_combos = len(segment_combinations)
            # make sure that the expected number of combinations is equal to the extracted number of combinations
            if(rr_num == 3):
                self.assertEqual(0, num_combos)
            else:
                self.assertEqual(1, num_combos)
            seg_type_counts = dict()
            seg_names = list()
            for s in range(len(segment_combinations)):
                segment_combination = segment_combinations[s]
                for segment in segment_combination.segments():
                    segment_match = read_result_obj[segment]
                    for gls_match in segment_match.gl_segments():
                        vdjml_look_seg_type = vdjml.segment_type(gls_match, meta)
                        if(not(vdjml_look_seg_type in seg_type_counts)):
                            seg_type_counts[vdjml_look_seg_type] = 1
                        else:
                            seg_type_counts[vdjml_look_seg_type] += 1
                        vdjml_segment = meta[gls_match.gl_segment()].name()
                        seg_names.append(vdjml_segment)
                for vdjml_segment in seg_type_counts:
                    # make sure V appears exactly once, D appears exactly once, J appears exactly once
                    self.assertEquals(seg_type_counts[vdjml_segment], 1)
                for i in range(3):
                    # make sure that expected is extracted
                    self.assertEquals(seg_names[i], topVDJExpected[i])
            rr_num += 1

if __name__ == '__main__':
    unittest.main()



