#!/usr/bin/env python

'''
vdjml/python/igblast_parse.py is part of VDJML project
Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
Copyright, The University of Texas Southwestern Medical Center, 2014
Author Edward A. Salinas 2014
'''

import vdjml
import re
import argparse
from Bio import SeqIO

def extractAsItemOrFirstFromList(t):
    '''
    If an item is a list, return the first item if it's found
    if the item is not a list, just return the item
    '''
    if(type(t) == list):
        if(len(t) > 0):
            return t[0]
    else:
        return t
    return None

def getSubstitutionsInsertionsDeletionsFromBTOP(btop):
    '''
    from a BTOP string extract the numbers of insertions, deletions, and
    substitutions
    in the BTOP for the pairs, the first character belongs to the READ(query),
    the second to the GERMLINE(subject)
    '''

    res_map = dict()
    btop_stats = vdjml.Btop_stats(vdjml.Btop(btop))
    res_map['substitutions']=btop_stats.substitutions_
    res_map['deletions']=btop_stats.deletions_
    res_map['insertions']=btop_stats.insertions_

    return res_map

def comp_dna(dna, allowIUPAC=False):
    '''
    complement a string (of DNA)
    allow IUPAC complementing if desired
    '''
# see http://droog.gs.washington.edu/parc/images/iupac.html
# IUPAC Code     Meaning     Complement
# A     A     T
# C     C     G
# G     G     C
# T/U     T     A
# M     A or C     K
# R     A or G     Y
# W     A or T     W
# S     C or G     S
# Y     C or T     R
# K     G or T     M
# V     A or C or G     B
# H     A or C or T     D
# D     A or G or T     H
# B     C or G or T     V
# N     G or A or T or C     N
    comped = ""
    comp = dict()
    comp['A'] = 'T'
    comp['T'] = 'A'
    comp['G'] = 'C'
    comp['C'] = 'G'
    comp['N'] = 'N'
    if(allowIUPAC):
        comp['U'] = 'A'
        comp['M'] = 'K'
        comp['R'] = 'Y'
        comp['W'] = 'W'
        comp['S'] = 'S'
        comp['Y'] = 'R'
        comp['K'] = 'M'
        comp['V'] = 'B'
        comp['H'] = 'D'
        comp['D'] = 'H'
        comp['B'] = 'V'
    for b in range(len(dna)):
        if(dna[b].upper() in comp):
            comped += comp[dna[b].upper()]
        else:
            comped += dna[b].upper()
    return comped
 

# reverse complement DNA
def rev_comp_dna(dna, allowIUPAC=False):
    vdjml_comped=vdjml.trim_complement(dna,vdjml.Interval.pos0_len(0, len(dna)), True)
    return vdjml_comped

# reverse a string (of DNA)
def rev_dna(dna):
    reversed_dna = ""
    for ub in range(len(dna)):
        b = len(dna) - ub - 1
        reversed_dna += dna[b]
    return reversed_dna

def extractSubSeq(interval, seq, is_inverted):
    '''
    given a seq record, an interval into it, and an inverted flag (telling
    whether interval is in the opposite strand or not)
    return the subsequence (inclusive) indicated by the interval
    NOTE that the interval has 1-based indices
    '''
    if(is_inverted):
        rc_inteval = getRevCompInterval(interval, len(seq))
        return extractSubSeq(interval, rev_comp_dna(seq), False)
    else:
        s = seq[interval[0] - 1:interval[1]]
        return s

def getRevCompInterval(i, seq_len_in_bp):
    '''
    given an interval (in list form [from,to])
    with 1-based indesing AND given a sequence length in BP
    return the same interval but on the reverse strand

    form an interval in one strand
    find the interval in the reverse complement strand
    '''
    i_s = i[0]
    i_e = i[1]
    int_len = abs(i_s - i_e)
    ne_s = seq_len_in_bp - i_e + 1
    ne_e = ne_s + int_len
    return [ne_s, ne_e]

def printMap(m):
    '''
    little utility to print a map
    '''
    i = 0
    for k in m:
        print "#" + str(i) + "\t", k, "->", m[k]
        i += 1

def makeMap(col_list, val_tab_str):
    '''
    from a column list (keys) and tab-separated values
    make a dict/map
    '''
    val_list = val_tab_str.split("\t")
    kvmap = dict()
    for c in range(len(col_list)):
        if(c < len(val_list)):
            kvmap[col_list[c]] = val_list[c]
        else:
            kvmap[col_list[c]] = None
    return kvmap

def makeMetricFromCharMap(charMap):
    '''
    given a characterization map for a region, make a metrics object
    '''
    pct_id = float(charMap['pct_id'])
    substitutions = charMap['base_sub']
    insertions = charMap['insertions']
    deletions = charMap['deletions']
    aln_metric = vdjml.Match_metrics(
        pct_id,
        substitutions=substitutions,
        insertions=insertions,
        deletions=deletions
        )
    return aln_metric

def extractJunctionRegionSeq(jRegion, query_rec):
    '''
    given a pyVDJML junction region, return the sequece as it would appear in
    IGBLAST output
    '''
    jIntervalStart = jRegion.range_.pos1()
    jIntervalEnd = jIntervalStart + jRegion.range_.length() - 1
    inverted_flag = jRegion.inverted()
    jSeq = extractSubSeq(
                        [jIntervalStart, jIntervalEnd],
                        str(query_rec.seq),
                        inverted_flag
                        )
    return jSeq

def compareIGBlastJuncDataWithQueryJuncData(
                                        query_rec,
                                        igblastSeq,
                                        igBlastInterval,
                                        inverted_flag
                                        ):
    '''
    The basic idea of this suburoutine is as follows:
    1) Assuming the query read is given proceed to step #2
    2) Recive the junction interval passed in (computed by the code) AND
    receive the junction sequence passed in (given by IGBLAST)
    3) compare the sequence given by IgBLAST with the sequence extracted from
    the read (using the computed interval taking into account inversion or not)
    4) if the SEQUENCE extracted from the read from the computed interval does
    NOT match the sequence as given by IgBLAST, then print out an error message
    '''
    if(query_rec is None):
        return
    if(not(igblastSeq is None)):
        igblastSeq = igblastSeq.upper()
    interval_first = igBlastInterval.pos1()
    interval_last = igBlastInterval.last1()
    r_seq = str(query_rec.seq)
    r_seq = r_seq.upper()
    r_j_seq = from_read_seq = extractSubSeq([interval_first, interval_last], r_seq, inverted_flag)
    if(r_j_seq != igblastSeq):
        print "UNEQUAL!"
        print "IGBLAST INTERVAL :", interval_first, " <-> ", interval_last
        print "READ NAME ", query_rec.id
        print "COMPUTED FROM QUERY : ", r_j_seq
        print "IGBLAST string : ", igblastSeq
        # sys.exit(0)
    else:
        # print "EQUAL!"
        pass

def obtainJuncIntervalAndSeq(
                             juncMapKey,
                             juncMap,
juncFirstStartSegMap,
juncFirstEndSegMap,
isVJJunc=False
):
    '''
    Analyze the junction sequence and look at the areas surrounding the junction 
    (anchors on each end VJ, DJ, VD)
    Based on the analysis compute a read interval and declare it to be the junction.
    Return that interval as well as the sequence as a package via an array.
    Testing of this code with real data (millions of reads) has shown that when 
    intervals and sequences
    are returned that when the interval is used with the actual read that the 
    returned sequences from here
    match the sequences retrieved from the read using the computed interval.
    '''
    juncSeq=juncMap[juncMapKey]
    if(not(juncSeq=="N/A" and len(juncSeq)>0)):
        juncSeq=re.sub(r'[^A-Z]','',juncSeq)
        juncSeqLen=len(juncSeq)
        junc_interval=None
        if(not(juncFirstStartSegMap==None or juncFirstEndSegMap==None) ):
            start_seg_end=int(juncFirstStartSegMap['q. end'])
            end_seg_bgn=int(juncFirstEndSegMap['q. start'])
            ref_junc_start=start_seg_end+1
            ref_junc_end=end_seg_bgn-1
            comp_len=ref_junc_end-ref_junc_start+1   #computed junction length
            igblast_len=juncSeqLen                   #junction length as computed from length of 
            if(not(comp_len==igblast_len)):
                junc_interval=vdjml.Interval.first_last1(
                                         min(ref_junc_start,ref_junc_end)+1,
                                         max(ref_junc_start,ref_junc_end)-1
                                                         )
            else:
                junc_interval=vdjml.Interval.first_last1(int(ref_junc_start),int(ref_junc_end))
        elif((juncFirstStartSegMap!=None and juncFirstEndSegMap==None) and isVJJunc):
            junc_start=int(juncFirstStartSegMap['q. end'])+1
            junc_end=junc_start+juncSeqLen-1
            junc_interval=vdjml.Interval.first_last1(junc_start,junc_end)
        else:
            return None
        return [junc_interval,juncSeq]
    else:
        #return None for junc seqs of N/A
        return None


# Receive packaged data from an IgBLAST result for a single read/query.
# Extract data and repackage it into a pyVDJML object
def vdjml_read_serialize(
        current_query,  # current query name
        fact,  # handle to VDJML factory
        hit_vals_list,  # hit values (list of lists)
        hit_fields,  # list of hit fields
        summary_fields,  # summary fields
        summary_vals_list,  # summary values list (list of lists)
        # rearrangment summary (fields (list) and values (list of lists)) :
        # top 3, chain, stop, frame, prod. strnd
        rearrangement_summary_fields, vdjr_vals_list,
        # junction (fields (list) and values (list of lists))
        junction_fields, junction_vals_list,
        # biopython seq record (currently for comparing data with what's in
        # original query)
        seq_record,
        dom_class  # the domain classification for the record
        ):
    # print "*******************************************\n"
    # print "NEED TO SERIALIZE FOR QUERY=",current_query
    if(dom_class == "imgt"):
        # print "setting imgt for factory...."
        fact.set_default_num_system(vdjml.Num_system.imgt)
    elif(dom_class == "kabat"):
        fact.set_default_num_system(vdjml.Num_system.kabat)
    rb1 = fact.new_result(current_query)
    # print "the base is ",imgtdb_obj.getDirBase()
    firstV = None
    firstD = None
    firstJ = None
    vaseq = None
    qvaseq = None
    daseq = None
    qdaseq = None
    jaseq = None
    qjaseq = None
    top_btop = dict()
    valn = None
    firstVMap = None
    firstDMap = None
    firstJMap = None
    firstVLine = None
    firstDLine = None
    firstJLine = None
    ############################################################################
    # if there are no hits, return an empty object.....
    # that contains just the read name! :(
    if(len(hit_vals_list) == 0):
        return rb1
    ################################################################
    # this is where the hits are picked up and put in VDJML objects
    inverted_flag = False
    for h in range(len(hit_vals_list)):

        # extract the key-value map from the hit list data.  supply the hit
        # fields to use as the keys
        kvmap = makeMap(hit_fields, hit_vals_list[h])

        # use the resulting map to extract information about the h'th hit
        btop_to_add = kvmap['BTOP']
        query_interval_to_add = vdjml.Interval.first_last1(
                                                        int(kvmap['q. start']),
                                                        int(kvmap['q. end'])
                                                        )
        if(int(kvmap['q. start']) > int(kvmap['q. end'])):
            query_interval_to_add = vdjml.Interval.first_last1(
                                                        int(kvmap['q. end']),
                                                        int(kvmap['q. start'])
                                                            )
        segment_type_to_add = kvmap['segment']
        hit_name_to_add = kvmap['subject ids']
        read_interval_to_add = vdjml.Interval.first_last1(
                                                        int(kvmap['q. start']),
                                                        int(kvmap['q. end'])
                                                        )
        if(int(kvmap['s. start']) < int(kvmap['s. end'])):
            hit_interval_to_add = vdjml.Interval.first_last1(
                                                        int(kvmap['s. start']),
                                                        int(kvmap['s. end'])
                                                            )
        else:
            # strand!
            hit_interval_to_add = vdjml.Interval.first_last1(
                                                        int(kvmap['s. end']),
                                                        int(kvmap['s. start'])
                                                            )
        # printMap(kvmap)
        read_name = kvmap['query id']
        if(read_name.find("reversed|") != (-1)):
            inverted_flag = True
        btop_metric_map = getSubstitutionsInsertionsDeletionsFromBTOP(
                                                                    btop_to_add
                                                                    )
        metrics_to_add = vdjml.Match_metrics(
            identity=float(kvmap['% identity']),
            score=int(kvmap['score']),
            substitutions=btop_metric_map['substitutions'],
            insertions=btop_metric_map['insertions'],
            deletions=btop_metric_map['deletions'],
            inverted=inverted_flag
            )


        # this is where the hit is added to the general list
        smb1 = rb1.insert_segment_match(
            read_pos0=(query_interval_to_add.pos1() - 1),
            btop=btop_to_add,
            vdj=segment_type_to_add,
            seg_name=hit_name_to_add,
            gl_pos0=hit_interval_to_add.pos1() - 1 ,
            metric=metrics_to_add
            )
        #################################################################
        # this is where the top hits are retrieved for the
        # combination/rearrangement
        if(firstV == None and segment_type_to_add == 'V'):
            firstV = smb1
            firstVMap = kvmap
            vaseq = kvmap['subject seq']
            qvaseq = kvmap['query seq']
            top_btop['V'] = kvmap['BTOP']
            alnDebugFlag = False
        elif(firstD == None and segment_type_to_add == 'D'):
            firstD = smb1
            firstDMap = kvmap
            daseq = kvmap['subject seq']
            qdaseq = kvmap['query seq']
            top_btop['D'] = kvmap['BTOP']
        elif(firstJ == None and segment_type_to_add == 'J'):
            firstJMap = kvmap
            firstJ = smb1
            jaseq = kvmap['subject seq']
            qjaseq = kvmap['query seq']
            top_btop['J'] = kvmap['BTOP']


    ############################################################################
    # this is where the combination is added based on the firstV,firstD,firstJ
    # from above
    scb = None
    vjdcomb_productive_flag = False
    # where the stop codon is put???
    vjdcomb_match_metrics = vdjml.Match_metrics
    if(len(vdjr_vals_list) > 0 and len(rearrangement_summary_fields) > 0):
        kvmap = makeMap(rearrangement_summary_fields, vdjr_vals_list[0])
        prod_str = kvmap['Productive']
        prod_str = prod_str.strip()
        prod_str = prod_str.lower()
        if(prod_str == "yes"):
            vjdcomb_productive_flag = True
        elif(prod_str == "no"):
            vjdcomb_productive_flag = False
        else:
            # NOTE there is no 'N/A' for the combination currently
            # possible....add later?
            vjdcomb_productive_flag = False
    if(not(firstV == None)):
        if(firstD == None):
            # V, no D so maybe a light chain?
            if(not(firstJ == None)):
                # V and J only
                scb = rb1.insert_segment_combination(
                                                    firstV.get().id(),
                                                    firstJ.get().id()
                # ,productive=vjdcomb_productive_flag)
                                                    )
            else:
                # V only
                scb = rb1.insert_segment_combination(firstV.get().id())
                # ,productive=vjdcomb_productive_flag)
        else:
            # firstD not none
            if(not(firstJ == None)):
                scb = rb1.insert_segment_combination(
                                                    firstV.get().id(),
                                                    firstD.get().id(),
                                                    firstJ.get().id()
                                                    )
                # ,productive=vjdcomb_productive_flag)
            else:
                scb = rb1.insert_segment_combination(
                                                    firstV.get().id(),
                                                    firstD.get().id()
                                                    )
                # ,productive=vjdcomb_productive_flag)
    else:
        # firstV is none
        pass



    ###############################################################
    # alignment summary (regions)
    for r in range(len(summary_vals_list)):
        kvmap = makeMap(summary_fields, summary_vals_list[r])
        region = kvmap['region'].strip()
        #skip CDR3 because it's only over the V segment and doens't go over D/J
        #skip over the 'total' because that's just aggrgated over the regions
        #and isn't actually a region.
        if(not(region.startswith("Total")) and not(region.startswith("CDR3"))):
            reg_from = kvmap['from']
            reg_to = kvmap['to']
            gaps = kvmap['gaps']
            length = kvmap['length']
            matches = kvmap['matches']
            pct_id = kvmap['percent_identity']
            try:
                reg_name = region
                reg_interval = vdjml.Interval.first_last1(
                                                        int(reg_from),
                                                        int(reg_to)
                                                        )
                mm = vdjml.Match_metrics(identity=float(pct_id))
                if(dom_class == "imgt"):
                    vdjml_num_sys = vdjml.Num_system.imgt
                else:
                    vdjml_num_sys = vdjml.Num_system.kabat
                scb.insert_region(
                                name=reg_name,
                                read_range=reg_interval,
                                metric=mm,
                                num_system=vdjml_num_sys
                                )
            except:
                pass
    ################################################
    # JUNCTIONs, VJ, DJ, VD
    empty_mm = vdjml.Match_metrics()
    if(len(junction_vals_list) > 0 and len(junction_fields) > 0 and scb!=None  ):
        juncMap = makeMap(junction_fields, junction_vals_list[0])

        #VJ JUNCTION
        if('V_J_junction' in juncMap):
            vjJuncIntervalAndSeq=obtainJuncIntervalAndSeq(
                                                          'V_J_junction',
                                                          juncMap,
                                                          firstVMap,
                                                          firstJMap,
                                                          True
                                                          )
            if(vjJuncIntervalAndSeq!=None):
                vj_junc_interval=vjJuncIntervalAndSeq[0]
                vjJuncSeq=vjJuncIntervalAndSeq[1]
                scb.insert_region(
                                name='vj_junction',
                                read_range=vj_junc_interval,
                                metric=empty_mm
                                )
                compareIGBlastJuncDataWithQueryJuncData(
                                                    seq_record,
                                                    vjJuncSeq,
                                                    vj_junc_interval,
                                                    inverted_flag
                                                    )
        #DJ JUNCTION
        if('D_J_junction' in juncMap):
            djJuncIntervalAndSeq=obtainJuncIntervalAndSeq(
                                                          'D_J_junction',
                                                          juncMap,
                                                          firstDMap,
                                                          firstJMap,
                                                          False
                                                          )
            if(djJuncIntervalAndSeq!=None):
                dj_junc_interval=djJuncIntervalAndSeq[0]
                djJuncSeq = djJuncIntervalAndSeq[1]
                scb.insert_region(
                                name='dj_junction',
                                read_range=dj_junc_interval,
                                metric=empty_mm
                                )
                compareIGBlastJuncDataWithQueryJuncData(
                                                seq_record,
                                                djJuncSeq,
                                                dj_junc_interval,
                                                inverted_flag
                                                )
        #VD JUNCTION
        if('V_D_junction' in juncMap):
            vdJuncIntervalAndSeq=obtainJuncIntervalAndSeq(
                                                          'V_D_junction',
                                                          juncMap,
                                                          firstVMap,
                                                          firstDMap,
                                                          False
                                                          )
            if(vdJuncIntervalAndSeq!=None):
                vd_junc_interval=vdJuncIntervalAndSeq[0]
                vdJuncSeq=vdJuncIntervalAndSeq[1]
                scb.insert_region(
                                name='vd_junction',
                                read_range=vd_junc_interval,
                                metric=empty_mm
                                )
                compareIGBlastJuncDataWithQueryJuncData(
                                                    seq_record,
                                                    vdJuncSeq,
                                                    vd_junc_interval,
                                                    inverted_flag
                                                    )
    return rb1

#apply trim to all the items of alist
def trimList(l):
    for idx, val in enumerate(l):
        # print idx, val
        l[idx] = l[idx].strip()
    return l


#clean up field/column headers
def prepHeaders(headers,forHitFields=False):
    if(not(forHitFields)):
        headers = re.compile('\s*,\s*').sub(',', headers, 0)
        headers = re.compile('\ ').sub('_', headers, 0)
        headers = re.compile('\-').sub('_', headers, 0)
        fields=headers.split(',')
        fields = trimList(fields)   
    else:
        headers=re.compile('^#\s*Fields:\s*').sub('', headers, 0)
        fields=headers.split(',')
        fields=trimList(fields)
    return fields


def scanOutputToVDJML(input_file, fact, fasta_query_path=None):
    '''
    Scan lines of IgBLAST output
    Use `#` at the beginning of lines to identify IgBLAST sections of output.
    Based on those sections interpret/classify the output (as alignment summary or hit
    data for example) and package/accumulate the data.
    Then, once the end of a record is reached (as indicated by processing the
    number of hits as it said it got)
    Send the accumulated/aggreagated/packaged data to vdjml_read_serialize
    to turn the data into a PyVDJML object. 
    And then return that created/serialized object
    '''
    INPUT = open(input_file, 'r')
    line_num = 0
    current_query = None
    current_query_id = (-1)
    firstV = None
    firstJ = None
    igblast_rec_id = 0
    biopy_seq_rec = None
    fasta_records = None
    dom_class = "imgt"
    if(fasta_query_path is not None):
        fasta_records = SeqIO.parse(fasta_query_path, "fasta")
    for igblast_line in INPUT:
        line_num += 1
        igblast_line = igblast_line.strip()
        # print igblast_line
        if(re.compile('^#').match(igblast_line)):
            rem = re.search('^#\s+IGBLASTN\s([^\s]+)\s*$', igblast_line)
            if rem:
                igblast_version = rem.group(1)
                mode = "IGB_VERSION"
            rem = re.search('^#\s+Query:\s(.*)', igblast_line)
            if(rem):  # or ref):
                # reset values for new IGBLAST set of results
                current_query = rem.group(1)
                vdjr_vals_list = list()
                junction_vals_list = list()
                summary_vals_list = list()
                summary_fields = list()
                hit_vals_list = list()
                hit_fields = list()
                vdjr_vals_list = list()
                junction_fields = list()
                junction_vals_list = list()
                rearrangement_summary_fields = list()
                mode = "query_retrieve"
                current_query_id += 1
                igblast_rec_id += 1
                if(fasta_records is not None):
                    biopy_seq_rec = fasta_records.next()
            rem = re.search('^#\s+Database:\s(.*)$', igblast_line)
            if rem:
                igblast_databases = rem.group(1)
                mode = "dbparse"
            rem = re.search('^#\sDomain\sclassification\srequested:\s(.*)$', igblast_line)
            if rem:
                dom_class = rem.group(1)
                mode = "dom_class"
            rem = re.search('^#\s(Note.*)$', igblast_line)
            if rem:
                note_text = rem.group(1)
            rem = re.search('^#\sV\-?\(D\)\-?J\sjunction', igblast_line)
            if rem:
                mode = "vdj_junction"
                srem = re.search('\((\s*V\send,[^\)]+)\)', igblast_line)
                if srem:
                    headers = srem.group(1)
                    junction_fields=prepHeaders(headers)
            rem = re.search('^#\sBLAST\sprocessed\s(\d+)\squer/', igblast_line)
            if rem:
                reported_queries_processed = rem.group(1)
            rem = re.search('^#\sAlignment\ssummary', igblast_line)
            if rem:
                mode = "vdj_alignment_summary"
                #obtain the alignment summary info with a regex
                srem = re.search('\(([^\)]+)\)', igblast_line)
                if srem:
                    headers = srem.group(1)
                    summary_fields=prepHeaders(headers)
                    summary_fields.insert(0, 'region')
            rem = re.search('^#\sHit\stable', igblast_line)
            if rem:
                mode = "hit_table_start"
            rem = re.search('^#\sV\-?\(D\)\-?J\srearrangement', igblast_line)
            if rem:
                # obtain the rearrangment info with the regex
                mode = "vdj_rearrangement"
                srem = re.search('\(([^\)]+)\)[:\.]', igblast_line)
                if srem:
                    # print "got field names..."
                    headers = srem.group(1)
                    rearrangement_summary_fields=prepHeaders(headers)
                else:
                    print "didn't get field names..."
            rem = re.search('#\sFields:', igblast_line)
            if rem:
                mode = "hit_fields"
                headers = igblast_line
                hit_fields=prepHeaders(headers,True)
                hit_fields.insert(0, "segment")
            rem = re.search('^#\s(\d+)\shits\sfound\s*', igblast_line)
            if rem:
                mode = "hit_table"
                num_hits_found = int(rem.group(1))
                current_num_hits = num_hits_found
                if(num_hits_found == 0):
                    # since there are zero hits, do a serialization now.
                    # there's no hit data to pickup
                    # CALL SERIALIZER as the hits for this have been picked up
                    getMapToo = True
                    serialized = vdjml_read_serialize(
                        current_query,  # current query name
                        fact,  # handle to VDJML factory
                        hit_vals_list,  # hit values (list of lists)
                        hit_fields,  # list of hit fields
                        summary_fields,  # summary fields
                        summary_vals_list,  # summary values list (list of lists)
                        # rearrangment values (list and list-of-lists)
                        rearrangement_summary_fields, vdjr_vals_list,  
                        # junction fields/values (list and list of lists)
                        junction_fields, junction_vals_list,  
                        biopy_seq_rec,
                        dom_class
                        )
                    rb1 = serialized
                    yield rb1.release()
        elif (not (re.compile('^\s*$').match(igblast_line))):
            if(mode == "vdj_rearrangement"):
                vdjr_vals_list.append(igblast_line)
            elif(mode == "vdj_junction"):
                junction_vals_list.append(igblast_line)
            elif(mode == "vdj_alignment_summary"):
                summary_vals_list.append(igblast_line)
            elif(mode == "hit_table"):
                hit_vals_list.append(igblast_line)
                # print "appended hits for read =",current_query," 
                # whose current_num_hits=",current_num_hits," and 
                # len(hit_vals_list)=",len(hit_vals_list)
                if(len(hit_vals_list) == current_num_hits):
                    # CALL SERIALIZER as the hits for this read/query been picked up
                    getMapToo = True
                    serialized = vdjml_read_serialize(
                        current_query,  # current query name
                        fact,  # handle to VDJML factory
                        hit_vals_list,  # hit values (list of lists)
                        hit_fields,  # list of hit fields
                        summary_fields,  # summary fields
                        summary_vals_list,  # summary values list (list of lists)
                        # rearrangment values (list and list-of-lists)
                        rearrangement_summary_fields, vdjr_vals_list,  
                        # junction fields/values (list and list of lists)
                        junction_fields, junction_vals_list,  
                        biopy_seq_rec,  # seq record
                        dom_class
                        )
                    rb1 = serialized
                    yield rb1.release()
            else:
                print "unhandled mode=", mode



# generate a factory from the arguments
def makeVDJMLDefaultMetaAndFactoryFromArgs(args):
    meta = vdjml.Results_meta()
    fact = vdjml.Result_factory(meta)
    fact.set_default_aligner(
            "IgBLAST",  # aligner name
            extractAsItemOrFirstFromList(args.igblast_version),  # aligner ver
            extractAsItemOrFirstFromList(args.igblast_params),  # aligner params
            extractAsItemOrFirstFromList(args.igblast_uri),  # aligner URI
            extractAsItemOrFirstFromList(args.igblast_runid)  # run id
            )
    fact.set_default_gl_database(
        extractAsItemOrFirstFromList(args.db_name_igblast),  # db name
        extractAsItemOrFirstFromList(args.db_ver),  # db ver
        extractAsItemOrFirstFromList(args.db_species_vdjml),  # db species
        extractAsItemOrFirstFromList(args.db_uri)  # db uri
        )

    return [meta, fact]

# given the arguments, make a VDJML
def makeVDJMLFileFromArgs(args):
    # build a factory for initialization
    mf = makeVDJMLDefaultMetaAndFactoryFromArgs(args)
    meta = mf[0]
    fact = mf[1]
    #begin to write output (using the meta and fact)
    rrw = vdjml.Vdjml_writer(str(args.vdjml_out[0]), meta)
    for read_result in scanOutputToVDJML(args.igblast_in[0], fact):
        rrw(read_result)


# generate dummy meta and factory with placeholders
def makeDummyMetaAndFactory():
    meta = vdjml.Results_meta()
    fact = vdjml.Result_factory(meta)
    fact.set_default_aligner(
            "igblast",  # aligner name
            "igblast_version",  # aligner ver
            "igblast_params",  # aligner params
            "igblast_uri",  # aligner URI
            0  # run id
            )
    fact.set_default_gl_database(
        "igblast_db",  # db name
        "igblast_db_ver",  # db ver
        "igblast_db_species",  # db species
        "igblast_db_uri"  # db uri
        )
    return [meta, fact]

# if meta and fact is none, use dummy with placeholders
# otherwise, expect meta and fact
# THEN yield read_results as a "generator"
def makeVDJMLRead_ResultsFromIGBLAST(igblastFile, metaAndFact=None):
    if(metaAndFact == None):
        mf = makeDummyMetaAndFactory()
    else:
        mf = metaAndFact
    meta = mf[0]
    fact = mf[1]
    for read_result in scanOutputToVDJML(igblastFile, fact):
        yield read_result

# return an argument parser
def makeParserArgs():
    parser = argparse.ArgumentParser(
description='Parse IGBLAST output, write a VDJML output file.'
)
    h = '''path to igblast analysis file of results, hits, segments, etc.
*** CRITICAL NOTE *** SHOULD BE RUN WITH OUTFMT AS SEEN HERE -outfmt
\"7 qseqid qgi qacc qaccver qlen sseqid sallseqid sgi sallgi sacc saccver
sallacc slen qstart qend sstart send qseq sseq evalue bitscore score length
pident nident mismatch positive gapopen gaps ppos frames qframe sframe btop\"'''
    parser.add_argument('igblast_in', type=str, nargs=1, help=h)
    
    h = 'the path to the output vdjml XML'
    parser.add_argument('vdjml_out', type=str, nargs=1, help=h)
    
    h = 'the version of igblast used placed in the VDJML output'
    parser.add_argument('-igblast_version', type=str, nargs=1, default="", help=h)
    
    h = 'the parameters passed to igblast placed in the VDJML output'
    parser.add_argument('-igblast_params', type=str, nargs=1, default="", help=h)
    
    h = 'the ID assigned to the run placed in the VDJML output'
    parser.add_argument('-igblast_runid', type=int, nargs=1, default=1, help=h)
    
    h = 'a URI string for the IgBLAST aligner to be placed in the VDJML output'
    parser.add_argument('-igblast_uri', type=str, nargs=1, default="", help=h)
    
    h = 'the name of the IgBLAST database placed in the VDJML output'
    parser.add_argument('-db_name_igblast', type=str, nargs=1, default="", help=h)
    
    h = 'a version string associated with the database placed in the VDJML output'
    parser.add_argument('-db_ver', type=str, nargs=1, default="", help=h)
    
    h = 'db organism for VDJML output file(s) placed in the VDJML output'
    parser.add_argument('-db_species_vdjml', type=str, nargs=1, default="", help=h)
    
    h = 'a URI associated with the database placed in the VDJML output'
    parser.add_argument('-db_uri', type=str, nargs=1, default="", help=h)
    
    return parser

if (__name__ == "__main__"):
    parser = makeParserArgs()
    args = parser.parse_args()
    if(args):
        #proceed using the arguments set
        makeVDJMLFileFromArgs(args)
    else:
        #error
        parser.print_help()

