/** @file "/vdjml/lib/segment_match.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/segment_match.hpp"

#include "boost/foreach.hpp"

#include "utils_io.hpp"
#include "vdjml/aa_substitution.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {
typedef vocab::vdjml<>::read_results::read read_vocab;
typedef read_vocab::alignment::segment_match segment_vocab;

/*
*******************************************************************************/
Segment_match::Segment_match(
         const std::size_t read_pos0,
         Btop const& btop,
         Match_metrics const& mm
)
: btop_(btop),
  read_range_(Interval::unset()),
  mm_(mm),
  gsv_(),
  aass_(),
  gl_len_()
{
   init(read_pos0);
}

/*
*******************************************************************************/
void Segment_match::init(const std::size_t read_pos0) {
   const Btop_stats bs(btop_);
   read_range_ = Interval::pos0_len(read_pos0, bs.read_len_);
   mm_.insertions_ = bs.insertions_;
   mm_.deletions_ = bs.deletions_;
   mm_.substitutions_ = bs.substitutions_;
   gl_len_ = bs.gl_len_;
}

/*
*******************************************************************************/
Nucleotide_match nucleotide_match(
         Segment_match const& sm,
         Aligned_pos pos0,
         Gl_segment_match const& gsm
) {
   pos0.pos_ -= pos0.is_gl() ? gsm.gl_position() : sm.read_range().pos0();
   Nucleotide_match nm = nucleotide_match(sm.btop(), pos0);
   nm.pos_[seq::Read] += sm.read_range().pos0();
   nm.pos_[seq::GL] += gsm.gl_position();
   return nm;
}

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Segment_match const& sm,
         Results_meta const& rm,
         const unsigned v
) {
   xw.open(segment_vocab::id_(), ELEM);
   write_id(xw, sm.id(), rm, v);
   xw.node(segment_vocab::read_pos0::id_(), ATTR, sm.read_range().pos0());
   xw.node(segment_vocab::read_len::id_(), ATTR, sm.read_range().length());
   xw.node(segment_vocab::gl_len::id_(), ATTR, sm.gl_length());
   write(xw, sm.match_metrics(), v);

   xw.node(segment_vocab::btop::id_(), ELEM, sm.btop().str());
   BOOST_FOREACH(Gl_segment_match const& gsm, sm.gl_segments()) {
      write(xw, gsm, rm, v);
   }
   BOOST_FOREACH(Aa_substitution const& aas, sm.aa_substitutions()) {
      write(xw, aas, v);
   }
   xw.close(); //read_segment_match, ELEM
}

/*
*******************************************************************************/
void merge(Segment_match& sm1, Segment_match const& sm2) {
   typedef Segment_match::Err Err;
   if( sm1 != sm2 ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t(
         "segment matches with different BTOP or range cannot be merged"
      )
   );

   BOOST_FOREACH(Gl_segment_match const& gsm, sm2.gl_segments()) {
      sm1.insert(gsm);
   }

   BOOST_FOREACH(Aa_substitution const& aas, sm2.aa_substitutions()) {
      sm1.insert(aas);
   }
}

}//namespace vdjml
