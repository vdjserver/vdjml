/** @file "/vdjml/lib/aa_index.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/aa_index.hpp"

#include <map>
#include "boost/algorithm/string/case_conv.hpp"
namespace ba = boost::algorithm;
#include "boost/assign/list_of.hpp"
namespace bass = boost::assign;

namespace vdjml {

/*
*******************************************************************************/
aminoacid::Aa aminoacid_index(std::string aa_str) {
   using namespace aminoacid;
   typedef std::map<std::string, Aa> map_t;
   static const map_t cm =
            bass::map_list_of
            ("ALA", Ala)   ("ALANINE",       Ala)("A", Ala)
            ("CYS", Cys)   ("CYSTEINE",      Cys)("C", Cys)
	    		("ASP", Asp)   ("ASPARTIC ACID", Asp)("D", Asp)
            ("GLU", Glu)   ("GLUTAMIC ACID", Glu)("E", Glu)
            ("PHE", Phe)   ("PHENYLALANINE", Phe)("F", Phe)
            ("GLY", Gly)   ("GLYCINE",       Gly)("G", Gly)
            ("HIS", His)   ("HISTIDINE",     His)("H", His)
            ("ILE", Ile)   ("ISOLEUCINE",    Ile)("I", Ile)
            ("LYS", Lys)   ("LYSINE",        Lys)("K", Lys)
            ("LEU", Leu)   ("LEUCINE",       Leu)("L", Leu)
            ("MET", Met)   ("METHIONINE",    Met)("M", Met)
            ("ASN", Asn)   ("ASPARAGINE",    Asn)("N", Asn)
            ("PRO", Pro)   ("PROLINE",       Pro)("P", Pro)
            ("GLN", Gln)   ("GLUTAMINE",     Gln)("Q", Gln)
            ("ARG", Arg)   ("ARGININE",      Arg)("R", Arg)
            ("SER", Ser)   ("SERINE",        Ser)("S", Ser)
            ("THR", Thr)   ("THREONINE",     Thr)("T", Thr)
            ("VAL", Val)   ("VALINE",        Val)("V", Val)
            ("TRP", Trp)   ("TRYPTOPHAN",    Trp)("W", Trp)
            ("TYR", Tyr)   ("TYROSINE",      Tyr)("Y", Tyr)
            ("STOP", Stop) ("*",             Stop)
            ;
   boost::algorithm::to_upper(aa_str);
   map_t::const_iterator i = cm.find(aa_str);
   if( i == cm.end() ) BOOST_THROW_EXCEPTION(
      base_exception()
      << base_exception::msg_t("unknown amino acid code")
      << base_exception::str1_t(aa_str)
   );
   return i->second;
}

}//namespace vdjml
