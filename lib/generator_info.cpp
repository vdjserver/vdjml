/** @file "/vdjml/lib/generator_info.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/generator_info.hpp"

#include "boost/date_time/posix_time/posix_time.hpp"

#include "vdjml/lib_info.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {

/*
*******************************************************************************/
Vdjml_generator_info::Vdjml_generator_info(
         std::string const& name,
         std::string const& version,
         boost::posix_time::ptime const& time_gmt

)
: name_(name.size() ? name : Lib_info::name()),
  version_(version.size() ? version : Lib_info::version()),
  time_gmt_(
           time_gmt.is_not_a_date_time() ?
                    boost::posix_time::second_clock::universal_time() :
                    time_gmt
  )
{}

/*
*******************************************************************************/
Vdjml_generator_info::Vdjml_generator_info(
         std::string const& name,
         std::string const& version,
         std::string const& time_gmt

)
: name_(name.size() ? name : Lib_info::name()),
  version_(version.size() ? version : Lib_info::version()),
  time_gmt_(
           time_gmt.size() ?
                    boost::date_time::parse_delimited_time<boost::posix_time::ptime>(time_gmt, 'T') :
                    boost::posix_time::second_clock::universal_time()
  )
{}

/*
*******************************************************************************/
std::string Vdjml_generator_info::datetime_str() const {
   return to_iso_extended_string(time_gmt_);
}

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Vdjml_generator_info const& gi,
         const unsigned version
) {
   typedef vocab::vdjml<>::meta::generator vocab_t;
   xw.open(vocab_t::id_(), ELEM);

   xw.node(vocab_t::name::id_(), ATTR, gi.name());
   xw.node(vocab_t::version::id_(), ATTR, gi.version());
   xw.node(vocab_t::time_gmt::id_(), ATTR, gi.datetime_str());

   xw.close(); //vocab_t::id_() ELEM
}

}//namespace vdjml
