/** @file "/vdjml/lib/vdjml_writer.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/vdjml_writer.hpp"

#include "boost/iostreams/filter/bzip2.hpp"
#include "boost/iostreams/filter/gzip.hpp"
#include "boost/iostreams/filter/zlib.hpp"

#include "vdjml/read_result.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vdjml_version.hpp"
#include "vdjml/vocabulary.hpp"

namespace vdjml {

/*
*******************************************************************************/
std::ofstream* Vdjml_writer::make_ofstream(
            std::string const& path,
            Compression& compr
) {
   static const std::ios_base::openmode bin_mode =
            std::ios_base::out | std::ios_base::trunc | std::ios_base::binary;

   static const std::ios_base::openmode txt_mode =
            std::ios_base::out | std::ios_base::trunc;

   if( compr == Unknown_compression ) compr = guess_compression_ext(path);

   const std::ios_base::openmode mode =
            (compr == Uncompressed) ? txt_mode : bin_mode;

   return new std::ofstream(path.c_str(), mode);
}

/*
*******************************************************************************/
Vdjml_writer::fosb_t*
Vdjml_writer::make_fosb(const Compression compr, std::ostream& os) {
   fosb_t* fosb = 0;
   switch (compr) {
      case Uncompressed:
         break;
      case bzip2:
         fosb = new fosb_t;
         fosb->push(boost::iostreams::bzip2_compressor());
         fosb->push(os);
         break;
      case gzip:
         fosb = new fosb_t;
         fosb->push(boost::iostreams::gzip_compressor());
         fosb->push(os);
         break;
      case zlib:
         fosb = new fosb_t;
         fosb->push(boost::iostreams::zlib_compressor());
         fosb->push(os);
         break;
      default:
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unsupported compression")
                  << Err::int1_t(compr)
         );
   }
   return fosb;
}

/*
*******************************************************************************/
void Vdjml_writer::init_xml() {
   if( ! is_supported(version_) ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unsupported VDJML version")
            << Err::str1_t(version(version_))
   );

   typedef vocab::vdjml<> vocab_t;

   xw_.open(vocab_t::id_(), ELEM, vocab_t::namespace_iri());
   xw_.node(vocab_t::version::id_(), ATTR, version(version_));
   write(xw_, rm_, version_);
   xw_.open(vocab_t::read_results::id_(), ELEM);
}

/*
*******************************************************************************/
void Vdjml_writer::operator() (Read_result const& rr) {
   if( need_init_ ) {
      init_xml();
      need_init_ = false;
   }
   write(xw_, rr, rm_, version_);
}

}//namespace vdjml
