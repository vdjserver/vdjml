/** @file "/vdjml/lib/result_builder.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/result_builder.hpp"

#include "boost/foreach.hpp"

#include "vdjml/results_meta.hpp"

namespace vdjml{ namespace detail {

/*
*******************************************************************************/
Gl_db_id Result_factory_impl::set_default_gl_database(
         std::string const& name,
         std::string const& version,
         std::string const& species,
         std::string const& url
) {
   def_gldb_ =
            rm_.insert(Gl_db_info(name, version, species, url));
   return def_gldb_;
}

/*
*******************************************************************************/
Aligner_id Result_factory_impl::set_default_aligner(
         std::string const& name,
         std::string const& version,
         std::string const& parameters,
         std::string const& uri,
         const unsigned run_id
) {
   def_aligner_ =
            rm_.aligner_map().insert(
                     Aligner_info(name, version, parameters, uri, run_id)
            );
   return def_aligner_;
}

/*
*******************************************************************************/
Numsys_id Result_factory_impl::set_default_num_system(std::string const& name) {
   def_numsys_ = rm_.num_system_map().insert(name);
   return def_numsys_;
}

/*
*******************************************************************************/
Gl_db_id Result_factory_impl::get_default_gl_database() const {
   if( def_gldb_ ) return def_gldb_;
   if( rm_.gl_db_map().size() == 1U ) {
      return rm_.gl_db_map().front().id();
   }
   return Gl_db_id();
}

/*
*******************************************************************************/
Aligner_id Result_factory_impl::get_default_aligner() const {
   if( def_aligner_ ) return def_aligner_;
   if( rm_.aligner_map().size() == 1U ) {
      return rm_.aligner_map().front().id();
   }
   return Aligner_id();
}

}//namespace detail

/*
*******************************************************************************/
Segment_combination_builder::Segment_combination_builder(
         detail::Result_factory_impl& rf,
         Read_result& rr,
         Segment_combination const& sc
)
: detail::Result_factory_impl(rf),
  rr_(rr),
  n_(rr_.segment_combinations().size())
{
   rr_.insert(sc);
}

/*
*******************************************************************************/
void Segment_combination_builder::insert_region(
         std::string const& name,
         Interval const& read_range,
         Match_metrics const& mm,
         const Numsys_id num_system,
         const Aligner_id aligner
) {
   const Region_id rid = meta().gene_region_map().insert(name);
   insert_region(rid, read_range, mm, num_system, aligner);
}

/*
*******************************************************************************/
void Segment_combination_builder::insert_region(
         const Region_id region,
         Interval const& read_range,
         Match_metrics const& metric,
         const Numsys_id num_system,
         Aligner_id aligner
) {
   if( ! aligner ) aligner = get_default_aligner();
   if( ! aligner ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("set default or specify the aligner")
   );
   get().insert(
      Gene_region(
         region, num_system, aligner, read_range, metric
      )
   );
}

/*
*******************************************************************************/
Segment_match_builder::Segment_match_builder(
         detail::Result_factory_impl& rf,
         Read_result& rr,
         const Seg_match_id sm_id
)
: detail::Result_factory_impl(rf),
  rr_(rr),
  sm_id_(sm_id),
  last_gl_seg_()
{

}

/*
*******************************************************************************/
Gl_seg_match_id Segment_match_builder::insert_gl_segment_match(
         const Gl_seg_id gl_segment_id,
         const unsigned pos0,
         Numsys_id num_system,
         Aligner_id aligner
) {
   if( ! num_system ) num_system = get_default_num_system();
   if( ! num_system ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("set default or specify the numbering system")
   );
   if( ! aligner ) aligner = get_default_aligner();
   if( ! aligner ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("set default or specify the aligner")
   );

   last_gl_seg_ = get().insert(
            Gl_segment_match(num_system, aligner, gl_segment_id, pos0)
   );
   return last_gl_seg_;
}

/*
*******************************************************************************/
Gl_seg_match_id Segment_match_builder::insert_gl_segment_match(
         const char vdj,
         std::string const& seg_name,
         unsigned pos0,
         Gl_db_id gl_database,
         Numsys_id num_system,
         Aligner_id aligner
) {
   if( ! gl_database ) gl_database = get_default_gl_database();

   if( ! num_system ) num_system = get_default_num_system();
   if( ! num_system ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("set default or specify the numbering system")
   );

   if( ! aligner ) aligner = get_default_aligner();
   if( ! aligner ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("set default or specify the aligner")
   );

   Gl_seg_id gs_id;
   if( gl_database ) {
      gs_id = meta().insert(Gl_segment_info(gl_database, vdj, seg_name));
   } else { //guess germline segment ID
      const Gl_segment_map::name_range nr =
               meta().gl_segment_map().find(seg_name);
      if( boost::distance(nr) == 1U ) gs_id = nr.front().id();
      else BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("set default or specify germline DB")
      );
   }
   return insert_gl_segment_match(
            gs_id,
            pos0,
            num_system,
            aligner
   );
}

/*
*******************************************************************************/
Result_builder::Result_builder(
         detail::Result_factory_impl& rf,
         std::string const& id
)
: detail::Result_factory_impl(rf), r_(new Read_result(id))
{}

/*
*******************************************************************************/
Result_builder::Result_builder(Results_meta& rm, std::string const& id)
: detail::Result_factory_impl(rm), r_(new Read_result(id))
{}

/*
*******************************************************************************/
Segment_match_builder Result_builder::insert_segment_match(
         const unsigned read_pos0,
         std::string const& btop,
         const char vdj,
         std::string const& seg_name,
         const unsigned gl_pos0,
         Match_metrics const& mm,
         Gl_db_id gl_database,
         Numsys_id num_system,
         Aligner_id aligner
) {
   const Seg_match_id id = get().insert(
            Segment_match(
                     read_pos0,
                     Btop(btop),
                     mm
            )
   );
   Segment_match_builder smb(*this, get(), id);
   smb.insert_gl_segment_match(
            vdj,
            seg_name,
            gl_pos0,
            gl_database,
            num_system,
            aligner
   );
   return smb;
}

/*
*******************************************************************************/
Segment_combination_builder Result_builder::insert_segment_combination(
         const Seg_match_id id1,
         const Seg_match_id id2,
         const Seg_match_id id3,
         const Seg_match_id id4,
         const Seg_match_id id5
) {
   return Segment_combination_builder(
            *this,
            get(),
            Segment_combination(id1, id2, id3, id4, id5)
   );
}

/*
*******************************************************************************/

}//namespace vdjml
