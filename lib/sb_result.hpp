/** @file "/vdjml/lib/sb_result.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef SB_RESULT_HPP_
#define SB_RESULT_HPP_
#include <string>
#include "boost/optional.hpp"
#include "proto_elements.hpp"
#include "proto_read_result.hpp"
#include "proto_segment_match.hpp"
#include "vdjml/aa_substitution.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/gene_region.hpp"
#include "vdjml/gl_segment_match.hpp"
#include "vdjml/object_ids.hpp"
#include "vdjml/read_result_fwd.hpp"
#include "vdjml/segment_combination.hpp"
#include "vdjml/vocabulary.hpp"

namespace vdjml{
class Btop;
class Segment_combination;
class Segment_match;

namespace detail{
class Sax_builder;

/**@brief set match metrics value
@return true if value belongs to match metrics
*******************************************************************************/
bool parse(std::string const& name, std::string const& val, Match_metrics& mm);

/**@brief SAX builder for read result structure
*******************************************************************************/
class Sb_read {
public:
   typedef vocab::vdjml<>::read_results::read vocab_t;
   struct Err : public base_exception {};
   explicit Sb_read(Sax_builder* sb): sb_(sb) {}

   void value(std::string const& name, std::string const& val);
   void open(std::string const& name);
   void close(std::string const& name) {}

private:
   Sax_builder* sb_;
};

/**@brief SAX builder for read result structure
*******************************************************************************/
class Sb_alignment {
public:
   typedef Sb_read::vocab_t::alignment vocab_t;
   struct Err : public base_exception {};
   explicit Sb_alignment(Sax_builder* sb): sb_(sb) {}

   void value(std::string const& name, std::string const& val) {
      if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected text")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
      );

      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected attribute")
               << Err::str1_t(name + "=\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
      );
   }

   void open(std::string const& name);
   void close(std::string const& name) {}

   void insert(Proto_segment_match const& psm);
   void insert(Proto_combination const& pc);

private:
   Sax_builder* sb_;
};

/**@brief SAX builder for read segment match structure
*******************************************************************************/
class Sb_seg_match {
public:
   typedef Sb_alignment::vocab_t::segment_match vocab_t;
   struct Err : public base_exception {};

   Sb_seg_match(Sax_builder* sb, Sb_alignment* sba);

   void value(std::string const& name, std::string const& val);
   void open(std::string const& name);
   void close(std::string const& name);
   void insert_btop(std::string const& btop) {psm_.btop_ = btop;}
   void insert(Proto_gl_segment_match const& pglsm) {psm_.insert(pglsm);}
   void insert(Aa_substitution const& aas) {psm_.insert(aas);}

private:
   Sax_builder* sb_;
   Sb_alignment* sba_;
   Proto_segment_match psm_;
};

/**@brief SAX builder for BTOP
*******************************************************************************/
class Sb_btop {
public:
   typedef Sb_seg_match::vocab_t::btop vocab_t;
   struct Err : public base_exception {};

   Sb_btop(Sb_seg_match* sm) : sm_(sm), str_() {}

   void value(std::string const& name, std::string const& val);

   void open(std::string const& name) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected element")
               << Err::str1_t(name)
               << Err::str2_t(vocab_t::id_())
      );
   }

   void close(std::string const& name) {sm_->insert_btop(str_);}

private:
   Sb_seg_match* sm_;
   std::string str_;
};

/**@brief SAX builder for germline segment match structure
*******************************************************************************/
class Sb_gl_segment {
public:
   typedef Sb_seg_match::vocab_t::gl_seg_match vocab_t;
   struct Err : public base_exception {};

   Sb_gl_segment(Sb_seg_match* sm) : sm_(sm) {}

   void value(std::string const& name, std::string const& val);

   void open(std::string const& name) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected element")
               << Err::str1_t(name)
               << Err::str2_t(vocab_t::id_())
      );
   }

   void close(std::string const& name);

private:
   Sb_seg_match* sm_;
   Proto_gl_segment_match pgsm_;

   void check() const;
};

/**@brief SAX builder for amino acid substitution
*******************************************************************************/
class Sb_aa_sub {
public:
   typedef Sb_seg_match::vocab_t::aa_substitution vocab_t;
   struct Err : public base_exception {};

   Sb_aa_sub(Sb_seg_match* sm) : sm_(sm) {}

   void value(std::string const& name, std::string const& val);

   void open(std::string const& name) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected element")
               << Err::str1_t(name)
               << Err::str2_t(vocab_t::id_())
      );
   }

   void close(std::string const& name);

private:
   Sb_seg_match* sm_;
   boost::optional<unsigned> read_pos0_;
   boost::optional<aminoacid::Aa> read_aa_;
   boost::optional<aminoacid::Aa> gl_aa_;
};

/**@brief SAX builder for read segment combination structure
*******************************************************************************/
class Sb_combination {
public:
   typedef Sb_alignment::vocab_t::combination vocab_t;
   struct Err : public base_exception {};

   Sb_combination(Sax_builder* sb, Sb_alignment* sba)
   : sb_(sb), sba_(sba)
   {}

   void value(std::string const& name, std::string const& val);
   void open(std::string const& name);
   void close(std::string const& name) {sba_->insert(pc_);}
   void insert(Proto_gene_region const& pgr) {pc_.pgrv_.push_back(pgr);}
private:
   Sax_builder* sb_;
   Sb_alignment* sba_;
   Proto_combination pc_;
};

/**@brief SAX builder for gene region structure
*******************************************************************************/
class Sb_region {
public:
   typedef Sb_combination::vocab_t::region vocab_t;
   struct Err : public base_exception {};

   Sb_region(Sb_combination* sbc): sbc_(sbc) {}

   void value(std::string const& name, std::string const& val);

   void open(std::string const& name)  {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected element")
               << Err::str1_t(name)
               << Err::str2_t(vocab_t::id_())
      );
   }

   void close(std::string const& name);

private:
   Sb_combination* sbc_;
   Proto_gene_region pgr_;
};

}//namespace detail
}//namespace vdjml
#endif /* SB_RESULT_HPP_ */
