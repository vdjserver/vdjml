/** @file "/vdjml/lib/vdjml_reader.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/vdjml_reader.hpp"

#include "boost/iostreams/filter/bzip2.hpp"
#include "boost/iostreams/filter/gzip.hpp"
#include "boost/iostreams/filter/zlib.hpp"
#include "boost/iostreams/filtering_streambuf.hpp"

#include "sax_handler.hpp"
#include "sax_builder.hpp"
#include "vdjml/vdjml_version.hpp"

namespace vdjml {

/*
*******************************************************************************/
std::ifstream* Vdjml_reader::make_ifstream(
            std::string const& path,
            Compression& compr
) {
   static const std::ios_base::openmode bin_mode =
            std::ios_base::in | std::ios_base::binary;

   static const std::ios_base::openmode txt_mode = std::ios_base::in;

   Compression c2 = guess_compression_magic(path);
   if( c2 != Unknown_compression ) compr = c2;

   const std::ios_base::openmode mode =
            (compr == Uncompressed) ? txt_mode : bin_mode;

   return new std::ifstream(path.c_str(), mode);
}

/*
*******************************************************************************/
Vdjml_reader::fisb_t* Vdjml_reader::make_fisb(
         std::istream& is,
         const Compression compr
) {
   fisb_t* fisb = 0;
   switch(compr) {
   case Uncompressed:
      break;
   case bzip2:
      fisb = new fisb_t;
      fisb->push(is);
      fisb->push(boost::iostreams::bzip2_decompressor());
      break;
   case gzip:
      fisb = new fisb_t;
      fisb->push(is);
      fisb->push(boost::iostreams::gzip_decompressor());
      break;
   case zlib:
      fisb = new fisb_t;
      fisb->push(is);
      fisb->push(boost::iostreams::zlib_decompressor());
      break;
   default:
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unsupported compression")
               << Err::int1_t(compr)
      );
   }
   return fisb;
}

/*
*******************************************************************************/
Vdjml_reader::Vdjml_reader(std::istream& is, Compression compr)
: ifs_(0),
  fisb_(make_fisb(is, compr)),
  fisb_is_(fisb_ ? new std::istream(&(*fisb_)) : 0),
  is_(fisb_is_ ? *fisb_is_ : is),
  builder_(new detail::Sax_builder()),
  handler_(new detail::Sax_handler(builder_.get(), is_))
{
   next(); //parse meta
   next(); //parse first result
}

/*
*******************************************************************************/
Vdjml_reader::Vdjml_reader(std::string const& path, Compression compr)
: ifs_(make_ifstream(path, compr)),
  fisb_(make_fisb(*ifs_, compr)),
  fisb_is_(fisb_ ? new std::istream(&(*fisb_)) : 0),
  is_(fisb_is_ ? *fisb_is_ : *ifs_),
  builder_(new detail::Sax_builder()),
  handler_(new detail::Sax_handler(builder_.get(), is_))
{
   next(); //parse meta
   next(); //parse first result
}

/*
*******************************************************************************/
unsigned Vdjml_reader::version() const {return builder_->version();}

/*
*******************************************************************************/
std::string Vdjml_reader::version_str() const {
   return vdjml::version(Vdjml_reader::version());
}

/*
*******************************************************************************/
Vdjml_reader::meta_ptr_t Vdjml_reader::meta() const {return builder_->meta();}

/*
*******************************************************************************/
void Vdjml_reader::next() {
   builder_->result_release();
   try{
      handler_->parse();
   } catch(...) {
      std::cerr << boost::current_exception_diagnostic_information() << std::endl;
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::nested_t(boost::current_exception())
      );
   }
}

/*
*******************************************************************************/
Vdjml_reader::result_ptr_t Vdjml_reader::result_release() const {
   return builder_->result_release();
}

/*
*******************************************************************************/
Read_result const& Vdjml_reader::result() const {return builder_->result();}

/*
*******************************************************************************/
Read_result& Vdjml_reader::result() {return builder_->result();}

/*
*******************************************************************************/
bool Vdjml_reader::has_result() const {return builder_->has_result();}

/*
*******************************************************************************/
Vdjml_generator_info const& Vdjml_reader::generator_info() const {
   return builder_->generator_info();
}

}//namespace vdjml
