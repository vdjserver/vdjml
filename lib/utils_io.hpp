/** @file "/vdjml/lib/utils_io.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef UTILS_IO_HPP_
#define UTILS_IO_HPP_
#include <string>
#include "boost/lexical_cast.hpp"
#include "boost/numeric/conversion/cast.hpp"
#include "boost/algorithm/string.hpp"
#include "vdjml/object_ids.hpp"
#include "vdjml/percent.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vdjml_current_version.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml{

/**@brief
*******************************************************************************/
template<typename T> inline T parse_unsigned(std::string const& str) {
   return boost::numeric_cast<T>(boost::lexical_cast<std::ptrdiff_t>(str));
}

/**@brief
*******************************************************************************/
inline bool parse_bool(std::string const& str) {
   if( boost::iequals(str, "true") ) return true;
   if( boost::iequals(str, "false") ) return false;
   return boost::lexical_cast<bool>(str);
}

/**@brief
*******************************************************************************/
inline Percent parse_percent(std::string const& str) {
   const float f =
            (*str.rbegin() == '%') ?
                     boost::lexical_cast<float>(str.c_str(), str.size() - 1) :
                     boost::lexical_cast<float>(str) * 100.0 ;
   return Percent(f);
}

/**
*******************************************************************************/
template<class Id> inline Id parse_id(
         std::string const& str,
         const unsigned = VDJML_CURRENT_VERSION
) {
   return Id(
            boost::numeric_cast<typename Id::value_type>(
                     boost::lexical_cast<std::ptrdiff_t>(str)
            )
   );
}


/*
*******************************************************************************/
inline void write_id(
         Xml_writer& xw,
         const Numsys_id numsys_id,
         Results_meta const& rm,
         const unsigned v
) {
   xw.node(
            vocab::vdjml<>::num_system::id_(),
            ATTR,
            rm.num_system_map()[numsys_id]
   );
}

/*
*******************************************************************************/
inline void write_id(
         Xml_writer& xw,
         const Aligner_id al_id,
         Results_meta const& rm,
         const unsigned v
) {
   xw.node(
            vocab::vdjml<>::meta::aligner::aligner_id::id_(),
            ATTR,
            al_id
   );
}

/*
*******************************************************************************/
inline void write_id(
         Xml_writer& xw,
         const Seg_match_id sm_id,
         Results_meta const& rm,
         const unsigned v
) {
   xw.node(
            vocab::vdjml<>::read_results::read::alignment::segment_match::
            segment_match_id::id_(),
            ATTR,
            sm_id
   );
}

/*
*******************************************************************************/
inline void write_id(
         Xml_writer& xw,
         const Gl_seg_match_id glsm_id,
         Results_meta const& rm,
         const unsigned v
) {
   xw.node(
            vocab::vdjml<>::read_results::read::alignment::segment_match::
            gl_seg_match::gl_seg_match_id::id_(),
            ATTR,
            glsm_id
   );
}

/*
*******************************************************************************/
inline void write_id(
         Xml_writer& xw,
         const Gl_db_id gldb_id,
         Results_meta const& rm,
         const unsigned v
) {
   xw.node(
            vocab::vdjml<>::meta::germline_db::gl_db_id::id_(),
            ATTR,
            gldb_id
   );
}

}//namespace vdjml
#endif /* UTILS_IO_HPP_ */
