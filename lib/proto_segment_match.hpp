/** @file "/vdjml/lib/proto_segment_match.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef PROTO_SEGMENT_MATCH_HPP_
#define PROTO_SEGMENT_MATCH_HPP_
#include <vector>
#include "boost/optional.hpp"
#include "vdjml/segment_match.hpp"
#include "proto_elements.hpp"

namespace vdjml{
class Results_meta;
namespace detail{
class Proto_results_meta;

/**@brief
*******************************************************************************/
class Proto_segment_match {
public:
   typedef Segment_match::Err Err;
   explicit Proto_segment_match(Proto_results_meta& prm) : prm_(prm) {}

   std::string id_;
   boost::optional<unsigned> read_pos0_;
   boost::optional<unsigned> read_len_;
   boost::optional<unsigned> gl_len_;
   boost::optional<std::string> btop_;
   Match_metrics mm_;
   Segment_match make() const;
   void insert(Proto_gl_segment_match const& pglsm);
   void insert(Aa_substitution const& aas) {aasv_.push_back(aas);}

private:
   Proto_results_meta& prm_;
   std::vector<Gl_segment_match> glsmv_;
   std::vector<Aa_substitution> aasv_;
};

}//namespace detail
}//namespace vdjml
#endif /* PROTO_SEGMENT_MATCH_HPP_ */
