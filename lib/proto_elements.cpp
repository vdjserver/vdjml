/** @file "/vdjml/lib/proto_elements.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "proto_elements.hpp"
#include "proto_results_meta.hpp"


namespace vdjml{ namespace detail{

/*
*******************************************************************************/
Gene_region make(Proto_gene_region const& pgr, Proto_results_meta& prm) {
   const Region_id rid = prm.meta().gene_region_map().insert(pgr.region_name_);
   const Numsys_id nsid = prm.meta().num_system_map().insert(pgr.num_system_);
   const Aligner_id aid = prm.aligner_id(pgr.aligner_id_);
   return Gene_region(
      rid,
      nsid,
      aid,
      Interval::pos0_len(pgr.read_pos_.get(), pgr.read_len_.get()),
      pgr.mm_
   );
}


}//namespace detail
}//namespace vdjml
