/** @file "/vdjml/lib/nucleotide_match.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/nucleotide_match.hpp"

#include "vdjml/btop.hpp"

namespace vdjml{

namespace{
boost::array<std::size_t,2> p_init = {{Btop::unset, Btop::unset}};
boost::array<char,2> c_init =
{{nucleotide::Special::Gap, nucleotide::Special::Gap}};
}

/*
*******************************************************************************/
Nucleotide_match::Nucleotide_match()
: pos_(p_init), chars_(c_init)
{}

}//namespace vdjml
