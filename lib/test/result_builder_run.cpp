/** @file "/vdjml/lib/test/result_builder_run.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE result_builder_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"

#include "boost/foreach.hpp"
#include "vdjml/result_builder.hpp"
#include "vdjml/results_meta.hpp"

namespace vdjml{ namespace test{

/**@test 
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Results_meta rm ;
   Result_factory rf(rm);
   rf.set_default_aligner("V-QUEST", " 3.2.32");
   rf.set_default_gl_database(
            "IGHV",
            "123-0",
            "Homo Sapiens", ""
            "http://www.imgt.org"
   );
   rf.set_default_num_system(Num_system::imgt);
   Result_builder rb = rf.new_result("readID");
   Segment_match_builder smb1 =rb.insert_segment_match(
            1,
            "61AC10A-136",
            'V',
            "IGHV3-21*01",
            22,
            Match_metrics(96.0, 264, 0, 0, 11)
   );

   Segment_match_builder smb2 =
            rb.insert_segment_match(
                     275,
                     "20",
                     'D', "IGHD3-22*01",
                     11,
                     Match_metrics(100, 22, 0, 0, 0)
   );

   Segment_match_builder smb3 =
            rb.insert_segment_match(
                     311,
                     "5AC35",
                     'J', "IGHJ4*02",
                     7,
                     Match_metrics(97.6, 40, 0, 0, 1)
   );

   Segment_combination_builder scb =
            rb.insert_segment_combination(
                     smb1.get().id(),
                     smb2.get().id(),
                     smb3.get().id()
            );

   scb.insert_region(
            "FR1",
            Interval::first_last1(1,54),
            Match_metrics(100, 54, 0, 0, 0),
            Num_system::imgt
   );

   scb.insert_region(
            "CDR1",
            Interval::first_last1(55,78),
            Match_metrics(83.3, 24, 0, 0, 4),
            Num_system::imgt
   );

   scb.insert_region(
            Gene_region_type::vd_junction,
            Interval::first_last1(276, 281)
   );

   BOOST_FOREACH(
            Gene_region const& gr,
            rb.get().segment_combinations().front().regions()
   ) {
      if( gr.region_type() == Gene_region_type::vd_junction ) {
         BOOST_CHECK( ! gr.numbering_system() );
      } else {
         BOOST_CHECK( gr.numbering_system() );
      }
   }
}

}//namespace test
}//namespace vdjml
