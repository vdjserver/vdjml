/** @file "/vdjml/lib/test/codon_index_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE codon_index_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdjml/codon_index.hpp"

namespace vdjml{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   using namespace nucleotide;
   BOOST_CHECK( ! is_translatable(Adenine, Gap, Guanine) );

   BOOST_CHECK( ! is_translatable(Any, Thymine, Guanine) );

   BOOST_CHECK(is_translatable(Adenine, Thymine, Guanine));

   BOOST_CHECK_THROW(translate(Adenine, Gap, Guanine), base_exception);

   BOOST_CHECK_THROW(translate(Any, Thymine, Guanine), base_exception);

   BOOST_CHECK_EQUAL(translate(Adenine, Thymine, Guanine), aminoacid::Met);

   BOOST_CHECK_EQUAL(translate(Cytosine, Guanine, Adenine), aminoacid::Arg);

   BOOST_CHECK_EQUAL(to_string(codon_index(Cytosine, Guanine, Adenine)), "CGA");

   BOOST_CHECK_EQUAL(to_string(codon_index(Gap, Guanine, Adenine)), "-GA");

   BOOST_CHECK_EQUAL(to_string(codon_index(Cytosine, Unknown, Unknown)), "C..");
}

}//namespace test
}//namespace vdjml
