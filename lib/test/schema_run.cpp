/** @file "/vdjml/lib/test/schema_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE schema_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "validate_xml.hpp"

namespace vdjml{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   schema_ptr schema = parse_schema("xsd/1/vdjml.xsd");
   BOOST_REQUIRE(schema);

   BOOST_CHECK(is_valid("sample_data/sample_01.vdjml", schema));
   BOOST_CHECK(is_valid("sample_data/sample_02.vdjml", schema));
}

}//namespace test
}//namespace vdjml
