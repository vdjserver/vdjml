/** @file "/vdjml/lib/test/construct_read_result_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE construct_read_result_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include <iostream>
#include "test/sample_data.hpp"
#include "vdjml/vdjml_writer.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/result_builder.hpp"
#include "vdjml/read_result.hpp"
#include "validate_xml.hpp"

namespace vdjml{ namespace test{

/**@test test read result ownership
*******************************************************************************/
BOOST_AUTO_TEST_CASE( ownership ) {
   Results_meta rm ;
   Result_factory rf(rm);
   rf.set_default_aligner("V-QUEST", " 3.2.32", "", "", 0);
   rf.set_default_gl_database(
            "IGHV",
            "123-0",
            "Homo Sapiens",
            "http://www.imgt.org"
   );
   rf.set_default_num_system(Num_system::imgt);

   Result_builder rb1 = rf.new_result("Y14934");

   std::auto_ptr<Read_result> p1 = rb1.release();
   BOOST_CHECK(p1.get());
   BOOST_CHECK_THROW(rb1.get(), Result_builder::Err);
}

/**@test builders and result writer compression
*******************************************************************************/
BOOST_AUTO_TEST_CASE( building ) {
   Results_meta rm ;
   Result_factory rf(rm);
   rf.set_default_aligner("V-QUEST", " 3.2.32");
   rf.set_default_gl_database(
            "IGHV",
            "123-0",
            "Homo Sapiens", ""
            "http://www.imgt.org"
   );
   rf.set_default_num_system(Num_system::imgt);

   Result_builder rb1 = rf.new_result("Y14934");

   Segment_match_builder smb1 =rb1.insert_segment_match(
            1,
            "61AC10A-136",
            'V',
            "IGHV3-21*01",
            22,
            Match_metrics(96.0, 264, 0, 0, 11)
   );
   BOOST_CHECK_EQUAL(smb1.get().gl_length(), 208);
   BOOST_CHECK_EQUAL(smb1.get().read_range().length(), 209);
   smb1.insert_aa_substitution(61, 'P', 'T');
   const Seg_match_id smid1 = smb1.get().id();

   const Seg_match_id smid1a = rb1.insert_segment_match(
            1,
            "61AC10A-136",
            'V',
            "IGHV3-21*02",
            22,
            Match_metrics(96.0, 264, 0, 0, 11)
   ).get().id(); //this entry should be merged with previous

   BOOST_CHECK_EQUAL(rb1.get().segment_matches().size(), 1U);
   BOOST_CHECK_EQUAL(smid1, smid1a);

   Segment_match_builder smb2 =
            rb1.insert_segment_match(
                     275,
                     "20",
                     'D', "IGHD3-22*01",
                     11,
                     Match_metrics(100, 22, 0, 0, 0)
   );

   Segment_match_builder smb3 =
            rb1.insert_segment_match(
                     311,
                     "5AC35",
                     'J', "IGHJ4*02",
                     7,
                     Match_metrics(97.6, 40, 0, 0, 1)
   );

   Segment_combination_builder scb =
            rb1.insert_segment_combination(smid1, smb2.get().id(), smb3.get().id());

   scb.insert_region(
            "FR1",
            Interval::first_last1(1,54),
            Match_metrics(100, 54, 0, 0, 0)
   );

   scb.insert_region(
            "CDR1",
            Interval::first_last1(55,78),
            Match_metrics(83.3, 24, 0, 0, 4)
   );

   scb.insert_region(
            "FR2",
            Interval::first_last1(79,129),
            Match_metrics(98, 59, 0, 0, 1)
   );

   const std::string out = temp_file_path("out2.vdjml");
   Vdjml_writer rrw1(out, rm);
   rrw1(rb1.get());
   rrw1.close();
   BOOST_CHECK(is_valid(out, "xsd/1/vdjml.xsd"));

   Vdjml_writer rrw2(temp_file_path("out3.vdjml.bz2"), rm);
   rrw2(rb1.get());

   Vdjml_writer rrw3(temp_file_path("out4.vdjml.gz"), rm);
   rrw3(rb1.get());
}

}//namespace test
}//namespace vdjml
