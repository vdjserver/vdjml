/** @file "/vdjml/lib/test/validate_xml.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VALIDATE_XML_HPP_
#define VALIDATE_XML_HPP_
#include <string>
#include "boost/shared_ptr.hpp"
#include "libxml/xmlschemas.h"
#include "libxml/xmlreader.h"
#include "vdjml/exception.hpp"

namespace vdjml{ namespace test{

struct Err : public base_exception {};

typedef boost::shared_ptr<xmlSchema> schema_ptr;

/**@brief
*******************************************************************************/
schema_ptr parse_schema(std::string const& xsd_file) {
   boost::shared_ptr<xmlSchemaParserCtxt> ctx(
            xmlSchemaNewParserCtxt(xsd_file.c_str()),
            &xmlSchemaFreeParserCtxt
   );

   return boost::shared_ptr<xmlSchema>(
            xmlSchemaParse(ctx.get()),
            &xmlSchemaFree
   );
}

/**@brief
*******************************************************************************/
bool is_valid(std::string const& xml_file, schema_ptr schema) {
   if( ! schema ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("schema error")
   );

   boost::shared_ptr<xmlSchemaValidCtxt> ctx(
            xmlSchemaNewValidCtxt(schema.get()),
            &xmlSchemaFreeValidCtxt
   );

   if( ! ctx ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("validation context error")
   );

   const int res = xmlSchemaValidateFile(ctx.get(), xml_file.c_str(), 0);

   if( res < 0 ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("internal error")
   );

   return ! res;
}

/**@brief
*******************************************************************************/
bool is_valid(std::string const& xml_file, std::string const& xsd_file) {
   return is_valid(xml_file, parse_schema(xsd_file));
}

}//namespace test
}//namespace vdjml
#endif /* VALIDATE_XML_HPP_ */
