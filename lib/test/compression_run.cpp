/** @file "/vdjml/lib/test/compression_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE compression_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "vdjml/compression.hpp"

namespace vdjml{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   BOOST_CHECK_EQUAL(
            guess_compression_ext(sample_file_path("sample_03.vdjml.bz2")),
            bzip2
   );
   BOOST_CHECK_EQUAL(
            guess_compression_ext(sample_file_path("sample_04.vdjml.gz")),
            gzip
   );
   BOOST_CHECK_EQUAL(
            guess_compression_magic(sample_file_path("sample_03.vdjml.bz2")),
            bzip2
   );
   BOOST_CHECK_EQUAL(
            guess_compression_magic(sample_file_path("sample_04.vdjml.gz")),
            gzip
   );
   BOOST_CHECK_EQUAL(
            guess_compression_magic(sample_file_path("sample_01.vdjml")),
            Uncompressed
   );
}

}//namespace test
}//namespace vdjml
