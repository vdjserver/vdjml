/** @file "/vdjml/lib/test/btop1_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE btop1_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include <iostream>
#include "vdjml/btop.hpp"

namespace vdjml{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   // ...A- read
   // ...CG gl
   Btop bt("3AC-G");
   BOOST_CHECK_THROW(Btop("47AC-GG"), Btop::Err);
   Btop_stats bs1(bt);
   BOOST_CHECK_EQUAL(bs1.insertions_, 0);
   BOOST_CHECK_EQUAL(bs1.deletions_, 1);
   BOOST_CHECK_EQUAL(bs1.substitutions_, 1);
   BOOST_CHECK_EQUAL(bs1.read_len_, 4);
   BOOST_CHECK_EQUAL(bs1.gl_len_, 5);
   BOOST_CHECK_EQUAL(bs1.matches(), 3);

   BOOST_CHECK_EQUAL(nucleotide_match(bt, Aligned_pos::gl(2)).read_pos(), 2);
   BOOST_CHECK_EQUAL(nucleotide_match(bt, Aligned_pos::gl(3)).read_pos(), 3);
   BOOST_CHECK_EQUAL(nucleotide_match(bt, Aligned_pos::gl(4)).read_pos(), 4);
   BOOST_CHECK_EQUAL(nucleotide_match(bt, Aligned_pos::gl(5)).read_pos(), Btop::unset);
   BOOST_CHECK_EQUAL(nucleotide_match(bt, Aligned_pos::read(3)).gl_pos(), 3);
   BOOST_CHECK_EQUAL(nucleotide_match(bt, Aligned_pos::read(4)).gl_pos(), Btop::unset);

   BOOST_CHECK_THROW(sequence_match(bt, Aligned_seq::read("cgtG")), Btop::Err);

   Sequence_match as;

   as = sequence_match(bt, Aligned_seq::read("cgta"));
   BOOST_CHECK_EQUAL(as.start_[seq::Read], 0);
   BOOST_CHECK_EQUAL(as.start_[seq::GL], 0);
   BOOST_CHECK_EQUAL(as.end_[seq::Read], 4);
   BOOST_CHECK_EQUAL(as.end_[seq::GL], 5);
   BOOST_CHECK_EQUAL(as.seq_[seq::Read], "cgtA-");
   BOOST_CHECK_EQUAL(as.seq_[seq::GL], "cgtCG");

   as = sequence_match(bt, Aligned_seq::gl("cgtcg"));
   BOOST_CHECK_EQUAL(as.start_[seq::Read], 0);
   BOOST_CHECK_EQUAL(as.start_[seq::GL], 0);
   BOOST_CHECK_EQUAL(as.end_[seq::Read], 4);
   BOOST_CHECK_EQUAL(as.end_[seq::GL], 5);
   BOOST_CHECK_EQUAL(as.seq_[seq::Read], "cgtA-");
   BOOST_CHECK_EQUAL(as.seq_[seq::GL], "cgtCG");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   Btop bt("100");
   Btop_stats bs(bt);
   BOOST_CHECK_EQUAL(bs.insertions_, 0);
   BOOST_CHECK_EQUAL(bs.deletions_, 0);
   BOOST_CHECK_EQUAL(bs.substitutions_, 0);
   BOOST_CHECK_EQUAL(bs.matches(), 100);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   Btop bt("");
   Btop_stats bs(bt);
   BOOST_CHECK_EQUAL(bs.insertions_, 0);
   BOOST_CHECK_EQUAL(bs.deletions_, 0);
   BOOST_CHECK_EQUAL(bs.substitutions_, 0);
   BOOST_CHECK_EQUAL(bs.matches(), 0);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case04 ) {
   // 012345  678910121416
   // .....A--CG....AA... read
   // .....CAA--....--... gl
   // 01234567  8910  1214
   Btop bt("5AC-A-AC-G-4A-A-3");
   Btop_stats bs(bt);
   BOOST_CHECK_EQUAL(bs.insertions_, 4);
   BOOST_CHECK_EQUAL(bs.deletions_, 2);
   BOOST_CHECK_EQUAL(bs.substitutions_, 1);
   BOOST_CHECK_EQUAL(bs.matches(), 12);

   const std::string read_seq = "ttttta" "cgttttaattt";
   const std::string gl_seq =   "tttttcaa" "tttt""ttt";

   Sequence_match as;

   as = sequence_match(bt, Aligned_seq::read(read_seq));
   BOOST_CHECK_EQUAL(as.start_[seq::Read], 0);
   BOOST_CHECK_EQUAL(as.start_[seq::GL], 0);
   BOOST_CHECK_EQUAL(as.end_[seq::Read], 17);
   BOOST_CHECK_EQUAL(as.end_[seq::GL], 15);
   BOOST_CHECK_EQUAL(as.seq_[seq::Read],  "tttttA--CGttttAAttt");
   BOOST_CHECK_EQUAL(as.seq_[seq::GL],    "tttttCAA--tttt--ttt");

   as = sequence_match(bt, Aligned_seq::gl(gl_seq));
   BOOST_CHECK_EQUAL(as.start_[seq::Read], 0);
   BOOST_CHECK_EQUAL(as.start_[seq::GL], 0);
   BOOST_CHECK_EQUAL(as.end_[seq::Read], 17);
   BOOST_CHECK_EQUAL(as.end_[seq::GL], 15);
   BOOST_CHECK_EQUAL(as.seq_[seq::Read],  "tttttA--CGttttAAttt");
   BOOST_CHECK_EQUAL(as.seq_[seq::GL],    "tttttCAA--tttt--ttt");
   std::cout
   << as.start_[seq::Read] << ' ' << as.seq_[seq::Read] << ' '
   << as.end_[seq::Read] << '\n'
   << as.start_[seq::GL] << ' ' << as.seq_[seq::GL] << ' '
   << as.end_[seq::GL] << std::endl;

   as = sequence_match(
            bt,
            Aligned_pos::read(6),
            Aligned_pos::read(16),
            Aligned_seq::read(read_seq)
   );
   BOOST_CHECK_EQUAL(as.start_[seq::Read], 6);
   BOOST_CHECK_EQUAL(as.start_[seq::GL], 8);
   BOOST_CHECK_EQUAL(as.end_[seq::Read], 16);
   BOOST_CHECK_EQUAL(as.end_[seq::GL], 14);
   BOOST_CHECK_EQUAL(as.seq_[seq::Read],  "CGttttAAtt");
   BOOST_CHECK_EQUAL(as.seq_[seq::GL],    "--tttt--tt");
   std::cout
   << as.start_[seq::Read] << ' ' << as.seq_[seq::Read] << ' '
   << as.end_[seq::Read] << '\n'
   << as.start_[seq::GL] << ' ' << as.seq_[seq::GL] << ' '
   << as.end_[seq::GL] << std::endl;
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case05 ) {
   //-... read
   //A... gl
   Btop bt1("-A3");
   BOOST_CHECK_EQUAL(nucleotide_match(bt1, Aligned_pos::read(0)).gl_pos(), 1);

   Nucleotide_match mpn;
   mpn = nucleotide_match(bt1, Aligned_pos::read(0), '*');
   BOOST_CHECK_EQUAL(mpn.pos_[seq::Read], 0);
   BOOST_CHECK_EQUAL(mpn.pos_[seq::GL], 1);
   BOOST_CHECK_EQUAL(mpn.chars_[seq::Read], '*');
   BOOST_CHECK_EQUAL(mpn.chars_[seq::GL], '*');

   mpn = nucleotide_match(bt1, Aligned_pos::gl(0), '*');
   BOOST_CHECK_EQUAL(mpn.pos_[seq::Read], 0);
   BOOST_CHECK_EQUAL(mpn.pos_[seq::GL], 0);
   BOOST_CHECK_EQUAL(mpn.chars_[seq::Read], '-');
   BOOST_CHECK_EQUAL(mpn.chars_[seq::GL], 'A');

   BOOST_CHECK_EQUAL(nucleotide_match(bt1, Aligned_pos::read(1)).gl_pos(), 2);
   BOOST_CHECK_EQUAL(nucleotide_match(bt1, Aligned_pos::gl(0)).read_pos(), 0);
   BOOST_CHECK_EQUAL(nucleotide_match(bt1, Aligned_pos::gl(1)).read_pos(), 0);
   BOOST_CHECK_EQUAL(nucleotide_match(bt1, Aligned_pos::gl(2)).read_pos(), 1);

   Sequence_match sm = sequence_match(bt1, '.');
   BOOST_CHECK_EQUAL(sm.start_[seq::Read], 0);
   BOOST_CHECK_EQUAL(sm.start_[seq::GL], 0);
   BOOST_CHECK_EQUAL(sm.end_[seq::Read], 3);
   BOOST_CHECK_EQUAL(sm.end_[seq::GL], 4);
   BOOST_CHECK_EQUAL(sm.seq_[seq::Read], "-...");
   BOOST_CHECK_EQUAL(sm.seq_[seq::GL], "A...");

   //  012345
   // -...AA read
   // A...-- gl
   // 0123  4
   Btop bt2("-A3A-A-");
   BOOST_CHECK_EQUAL(nucleotide_match(bt2, Aligned_pos::read(3)).gl_pos(), 4);
   BOOST_CHECK_EQUAL(nucleotide_match(bt2, Aligned_pos::read(4)).gl_pos(), 4);
   BOOST_CHECK_EQUAL(nucleotide_match(bt2, Aligned_pos::read(5)).gl_pos(), Btop::unset);

   sm = sequence_match(bt2, '.');
   BOOST_CHECK_EQUAL(sm.start_[seq::Read], 0);
   BOOST_CHECK_EQUAL(sm.start_[seq::GL], 0);
   BOOST_CHECK_EQUAL(sm.end_[seq::Read], 5);
   BOOST_CHECK_EQUAL(sm.end_[seq::GL], 4);
   BOOST_CHECK_EQUAL(sm.seq_[seq::Read], "-...AA");
   BOOST_CHECK_EQUAL(sm.seq_[seq::GL], "A...--");

   sm = sequence_match(bt2, Aligned_pos::read(1), Aligned_pos::read(4), '.');
   BOOST_CHECK_EQUAL(sm.start_[seq::Read], 1);
   BOOST_CHECK_EQUAL(sm.start_[seq::GL], 2);
   BOOST_CHECK_EQUAL(sm.end_[seq::Read], 4);
   BOOST_CHECK_EQUAL(sm.end_[seq::GL], 4);
   BOOST_CHECK_EQUAL(sm.seq_[seq::Read], "..A");
   BOOST_CHECK_EQUAL(sm.seq_[seq::GL], "..-");

   sm = sequence_match(bt2, Aligned_pos::gl(1), Aligned_pos::gl(4), '.');
   BOOST_CHECK_EQUAL(sm.start_[seq::Read], 0);
   BOOST_CHECK_EQUAL(sm.start_[seq::GL], 1);
   BOOST_CHECK_EQUAL(sm.end_[seq::Read], 5);
   BOOST_CHECK_EQUAL(sm.end_[seq::GL], 4);
   BOOST_CHECK_EQUAL(sm.seq_[seq::Read], "...AA");
   BOOST_CHECK_EQUAL(sm.seq_[seq::GL], "...--");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case06 ) {
   // 0123456789
   // .....AA... read
   // .....C-... gl
   // 012345 678
   Btop bt("5ACA-3");
   Sequence_match as;

   as = sequence_match(bt, Aligned_pos::read(2), Aligned_pos::read(6), '.');
   BOOST_CHECK_EQUAL(as.start_[seq::Read], 2);
   BOOST_CHECK_EQUAL(as.start_[seq::GL], 2);
   BOOST_CHECK_EQUAL(as.end_[seq::Read], 6);
   BOOST_CHECK_EQUAL(as.end_[seq::GL], 6);
   BOOST_CHECK_EQUAL(as.seq_[seq::Read], "...A");
   BOOST_CHECK_EQUAL(as.seq_[seq::GL], "...C");

   as = sequence_match(bt, Aligned_pos::gl(2), Aligned_pos::gl(6), '.');
   BOOST_CHECK_EQUAL(as.start_[seq::Read], 2);
   BOOST_CHECK_EQUAL(as.start_[seq::GL], 2);
   BOOST_CHECK_EQUAL(as.end_[seq::Read], 7);
   BOOST_CHECK_EQUAL(as.end_[seq::GL], 6);
   BOOST_CHECK_EQUAL(as.seq_[seq::Read], "...AA");
   BOOST_CHECK_EQUAL(as.seq_[seq::GL], "...C-");
}

}//namespace test
}//namespace vdjml
