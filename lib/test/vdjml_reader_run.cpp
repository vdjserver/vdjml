/** @file "/vdjml/lib/test/vdjml_reader_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE vdjml_reader_run
#include "boost/test/unit_test.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "vdjml/read_result.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vdjml_reader.hpp"
#include "vdjml/generator_info.hpp"

namespace vdjml{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   const std::string path = sample_file_path("sample_01.vdjml");
   Vdjml_reader vr(path);

   BOOST_CHECK_EQUAL(vr.version(), VDJML_CURRENT_VERSION);
   BOOST_CHECK_NO_THROW(vr.meta());

   Results_meta const& rm = *vr.meta();
   BOOST_CHECK_EQUAL(vr.generator_info().name(), "libVDJML");
   BOOST_CHECK_EQUAL(
            vr.generator_info().datetime(),
            boost::posix_time::time_from_string("2000-Jan-01 00:00:42")
   );
   BOOST_CHECK_EQUAL(vr.generator_info().version(), "0.1.0");
   BOOST_CHECK_EQUAL(rm.aligner_map().size(), 1);
   BOOST_CHECK_EQUAL(rm.aligner_map().front().id(), Aligner_id(1));
   BOOST_CHECK_EQUAL(rm.aligner_map().front().name(), "V-QUEST");
   BOOST_CHECK_EQUAL(rm.aligner_map().front().version(), "3.2.32");

   Gl_db_info const& db1 = *rm.gl_db_map().begin();
   BOOST_CHECK_EQUAL(rm.gl_db_map().size(), 2);
   BOOST_CHECK_EQUAL(db1.id(), Gl_db_id(1));
   BOOST_CHECK_EQUAL(db1.name(), "IGHV");
   BOOST_CHECK_EQUAL(db1.species(), "Mus musculus");
   BOOST_CHECK_EQUAL(db1.version(), "123-0");
   BOOST_CHECK_EQUAL(db1.uri(), "http://www.imgt.org");

   Gl_db_info const& db2 = *++rm.gl_db_map().begin();
   BOOST_CHECK_EQUAL(db2.id(), Gl_db_id(2));
   BOOST_CHECK_EQUAL(db2.name(), "IGHV");
   BOOST_CHECK_EQUAL(db2.species(), "Homo Sapiens");
   BOOST_CHECK_EQUAL(db2.version(), "123-0");
   BOOST_CHECK_EQUAL(db2.uri(), "http://www.imgt.org");

   BOOST_CHECK(vr.has_result());
   Read_result const& rr1 = vr.result();
   BOOST_CHECK_EQUAL(rr1.id(), "Y14934");
   BOOST_CHECK_EQUAL(rr1.segment_matches().size(), 3);

   vr.next();
   BOOST_CHECK( ! vr.has_result());
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   const std::string path = sample_file_path("sample_02.vdjml");
   unsigned n = 0;
   for( Vdjml_reader vr(path); vr.has_result(); vr.next() ) {
      Read_result const& rr = vr.result();
      std::cout << rr.id() << std::endl;
      ++n;
      BOOST_CHECK_GE(rr.segment_matches().size(), 3);
      BOOST_CHECK(rr.segment_combinations().size());
   }

   BOOST_CHECK_EQUAL(n, 4);
}

}//namespace test
}//namespace vdjml
