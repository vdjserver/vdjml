/** @file "/vdjml/lib/test/btop2_run.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE btop2_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdjml/btop.hpp"

namespace vdjml{ namespace test{

/**@test 
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   const Aligned_seq read_seq = Aligned_seq::read(
            "GGGGGAGGCCTGGTCAAGCCTGGGGGGTCCCTGAGACTCTCCTGTGCAGCCTCTGGATTCCCCTTC"
            "AGTAACTACACCATGCACTGGGTCCGCCAGGCTCCAGGGAAGGGGCTGGAGTGGGTCTCATCCATT"
            "ACTAGTAGTAGTAGTTACAGATATTACGCAGACTCAGTGGAGGGCCGATTCACCATCTCCAGAGAC"
            "AACGCCAAGAACTCACTGTATCTGCAAATGAACAGCCTGAGAGCCGAGGACACGGCTGTGTATTTC"
            "TGTGTGAGAGA"
            )
            ;

   Btop b("60CA9AG3CT1CG4CA51CG17GT3TC15GA90TA5TC6");

   BOOST_CHECK(check(b, read_seq));
   BOOST_CHECK_NO_THROW(validate(b, read_seq));
   BOOST_CHECK_THROW(validate(b, Aligned_seq::gl(read_seq.seq())), base_exception);
   BOOST_CHECK(
            is_open_frame(
                     b,
                     Aligned_pos::read(0),
                     read_seq
            )
   );

   BOOST_CHECK(
            ! is_open_frame(
                     b,
                     Aligned_pos::read(1),
                     read_seq
            )
   );

   BOOST_CHECK_EQUAL(find_open_frame(b, Aligned_pos::read(0), read_seq), 0);
}

}//namespace test
}//namespace vdjml
