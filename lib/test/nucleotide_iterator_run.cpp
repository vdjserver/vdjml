/** @file "/vdjml/lib/test/nucleotide_iterator_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE nucleotide_iterator_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdjml/nucleotide_iterator.hpp"

namespace vdjml{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( move_beyond ) {
   // .A- read
   // .CG gl
   Btop bt("1AC-G");
   nucleotide_iter i = nucleotide_iterator(bt);
   i.move_to(Aligned_pos::read(10));
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 3);
   BOOST_CHECK( i.at_end() );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( mismatch_iter_1 ) {
   //  012345
   // -...AA read
   // A...-- gl
   // 0123  4
   Btop b("-A3A-A-");
   Mismatch_iter i(b);
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 0);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 0);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK( i.is_gap(seq::Read) );
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'A');
   BOOST_CHECK( ! i.is_gap(seq::GL) );

   i.next();
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 3);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 4);
   BOOST_REQUIRE( ! i.at_end() );
   BOOST_CHECK( ! i.is_gap(seq::Read) );
   BOOST_CHECK( i.is_gap(seq::GL) );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 4);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 4);
   BOOST_REQUIRE( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 5);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 4);
   BOOST_REQUIRE( i.at_end() );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( move_mismatch_iter_1 ) {
   //  012345
   // -...AA read
   // A...-- gl
   // 0123  4
   Btop b("-A3A-A-");
   Mismatch_iter i(b);
   BOOST_REQUIRE( ! i.at_end() );
   i.move_to(Aligned_pos::gl(1));
   BOOST_REQUIRE( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 3);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 4);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( move_mismatch_iter_2 ) {
   // 0   1234
   // A---.a. read
   // Cggg.c. gl
   // 01234567
   Btop b("AC-g-g-g1ac1");
   Mismatch_iter i(b);
   i.move_to(Aligned_pos::read(1));
   BOOST_REQUIRE( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 5);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'C');
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop1_no_seq ) {
   // .A- read
   // .CG gl
   Btop bt("1AC-G");
   nucleotide_iter i = nucleotide_iterator(bt, '.');
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '.');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '.');
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'C');
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'G');
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 2);
   i.next();
   BOOST_CHECK( i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 3);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop1_read_seq1 ) {
   // .A- read
   // .CG gl
   Btop bt("1AC-G");
   nucleotide_iter_s i = nucleotide_iterator(bt, Aligned_seq::read("ca"));
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'c');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'c');
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'C');
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'G');
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 2);
   i.next();
   BOOST_CHECK( i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 3);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop1_read_throw ) {
   // .A- read
   // .CG gl
   Btop bt("1AC-G");
   nucleotide_iter_s i = nucleotide_iterator(bt, Aligned_seq::read("cG"));
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 0);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 0);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'c');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'c');
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 1);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 1);
   BOOST_CHECK_THROW(i.letters(), Btop::Err);
   BOOST_CHECK_THROW(i.get(), Btop::Err);
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 2);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'G');
   i.next();
   BOOST_CHECK( i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 3);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop1_gl_throw ) {
   // .A- read
   // .CG gl
   Btop bt("1AC-G");
   nucleotide_iter_s i = nucleotide_iterator(bt, Aligned_seq::gl("cTG"));
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 0);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 0);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'c');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'c');
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 1);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 1);
   BOOST_CHECK_THROW(i.letters(), Btop::Err);
   BOOST_CHECK_THROW(i.get(), Btop::Err);
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 2);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'G');
   i.next();
   BOOST_CHECK( i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 3);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop1_gl_seq ) {
   // .A- read
   // .CG gl
   Btop bt("1AC-G");
   nucleotide_iter_s i = nucleotide_iterator(bt, Aligned_seq::gl("gcg"));
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'g');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'g');
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'C');
   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'G');
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 2);
   i.next();
   BOOST_CHECK( i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 3);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop2_no_seq ) {
   // 012  3456789
   // ..A--CG.AA... read
   // ..CAA--.--... gl
   // 01234  5  678
   Btop bt("2AC-A-AC-G-1A-A-3");
   nucleotide_iter i = nucleotide_iterator(bt, '*');
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '*');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '*');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '*');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '*');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'C');
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 2);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'A');
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 3);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 3);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 3);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 4);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'A');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 3);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 5);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'C');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 4);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 5);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'G');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 5);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 5);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '*');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '*');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 6);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 6);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 7);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 6);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 8);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 6);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '*');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '*');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 9);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 7);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '*');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '*');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 10);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 8);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '*');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '*');

   i.next();
   BOOST_CHECK( i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 11);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 9);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop2_gl_seq ) {
   // 012  3456789
   // ..A--CG.AA... read
   // ..CAA--.--... gl
   // 01234  5  678
   Btop bt("2AC-A-AC-G-1A-A-3");
   nucleotide_iter_s i = nucleotide_iterator(bt, Aligned_seq::gl("ttcaatttt"));
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'C');
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 2);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 2);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'A');
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 3);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 3);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 3);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 4);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], '-');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 'A');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 3);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 5);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'C');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 4);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 5);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'G');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 5);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 5);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 6);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 6);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 7);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 6);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 8);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 6);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 9);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 7);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');

   i.next();
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 10);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 8);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');

   i.next();
   BOOST_CHECK( i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 11);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 9);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop3_gl_seq ) {
   // 012345  678910121416
   // .....A--CG....AA... read
   // .....CAA--....--... gl
   // 01234567  8910  1214
   Btop bt("5AC-A-AC-G-4A-A-3");
   const std::string read_seq = "ttttta" "cgttttaattt";

   nucleotide_iter_s i = nucleotide_iterator(bt, Aligned_seq::read(read_seq));
   i.next(); //1
   i.next(); //2
   i.next(); //3
   i.next(); //4
   i.next(); //5
   i.next(); //6
   i.next(); //6
   i.next(); //6
   i.next(); //7
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 7);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 8);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'G');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');
   i.next(); //8
   i.next(); //9
   i.next(); //10
   i.next(); //11
   i.next(); //12
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 12);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 12);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');
   i.next(); //13
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 13);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 12);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 'A');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], '-');
   i.next(); //14
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 14);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 12);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');
   i.next(); //15
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 15);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 13);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');
   i.next(); //16
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 16);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 14);
   BOOST_CHECK_EQUAL(i.letters()[seq::Read], 't');
   BOOST_CHECK_EQUAL(i.letters()[seq::GL], 't');
   i.next(); //17
   BOOST_CHECK( i.at_end() );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( btop4_iter ) {
   //-... read
   //A... gl
   Btop b("-A3");
   nucleotide_iter i = nucleotide_iterator(b);
   i.move_to(Aligned_pos::read(0));
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 0);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 1);
   i.move_to(Aligned_pos::read(0));
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 0);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 1);

   i = nucleotide_iterator(b);
   i.move_to(Aligned_pos::gl(0));
   BOOST_CHECK( ! i.at_end() );
   BOOST_CHECK_EQUAL(i.positions()[seq::Read], 0);
   BOOST_CHECK_EQUAL(i.positions()[seq::GL], 0);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( mismatch_iter ) {
   // 0123456789
   // .....AA... read
   // .....C-... gl
   // 012345 678
   Btop bt("5ACA-3");
   Mismatch_iter mi(bt);
   BOOST_CHECK( ! mi.at_end());
   Nucleotide_match m = mi.get();
   BOOST_CHECK_EQUAL(m.read_pos(), 5);
   BOOST_CHECK_EQUAL(m.gl_pos(), 5);
   BOOST_CHECK_EQUAL(m.read_char(), 'A');
   BOOST_CHECK_EQUAL(m.gl_char(), 'C');

   mi.next();
   BOOST_CHECK( ! mi.at_end());
   m = mi.get();
   BOOST_CHECK_EQUAL(m.read_pos(), 6);
   BOOST_CHECK_EQUAL(m.gl_pos(), 6);
   BOOST_CHECK_EQUAL(m.read_char(), 'A');
   BOOST_CHECK_EQUAL(m.gl_char(), '-');

   mi.next();
   BOOST_CHECK(mi.at_end());
}

}//namespace test
}//namespace vdjml
