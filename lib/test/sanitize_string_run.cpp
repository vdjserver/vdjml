/** @file "/vdjml/lib/test/sanitize_string_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE sanitize_string_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"

#include "vdjml/sanitize_string.hpp"

namespace vdjml{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   std::string s1 = "blah";
   BOOST_CHECK_EQUAL(sanitize(s1, 50), "\"" "blah" "\"");

   s1.push_back(10);
   BOOST_CHECK_EQUAL(sanitize(s1, 50), "\"" "blah\\n" "\"");

   s1.push_back(0);
   BOOST_CHECK_EQUAL(sanitize(s1, 50), "\"" "blah\\n\\x0" "\"");

   s1.push_back(200);
   BOOST_CHECK_EQUAL(sanitize(s1, 50), "\"" "blah\\n\\x0\\xc8" "\"");

   BOOST_CHECK_EQUAL(sanitize(s1, 4), "\"" "blah..." "\"");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   BOOST_CHECK_EQUAL(sanitize('c'), "\"" "c" "\"");
   BOOST_CHECK_EQUAL(sanitize('\''), "\"" "\\'" "\"");
   BOOST_CHECK_EQUAL(sanitize(0), "\"" "\\x0" "\"");
}

}//namespace test
}//namespace vdjml
