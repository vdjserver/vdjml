/** @file "/vdjml/lib/test/interval_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE interval_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdjml/interval.hpp"

namespace vdjml{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Interval i1 = Interval::first_last0(10, 20);
   BOOST_CHECK_EQUAL(i1.pos0(), 10U);
   BOOST_CHECK_EQUAL(i1.pos1(), 11U);
   BOOST_CHECK_EQUAL(i1.length(), 11U);
   BOOST_CHECK_EQUAL(i1.last0(), 20U);
   BOOST_CHECK_EQUAL(i1.last1(), 21U);

   Interval i2 = Interval::first_last1(10, 20);
   BOOST_CHECK_EQUAL(i2.pos0(), 9U);
   BOOST_CHECK_EQUAL(i2.pos1(), 10U);
   BOOST_CHECK_EQUAL(i2.length(), 11U);
   BOOST_CHECK_EQUAL(i2.last0(), 19U);
   BOOST_CHECK_EQUAL(i2.last1(), 20U);

   BOOST_CHECK(i1 != i2);
   BOOST_CHECK(i1 > i2);

   BOOST_CHECK_THROW(Interval::first_last0(10, 9), Interval::Err);
   BOOST_CHECK_THROW(Interval::first_last1(0, 10), Interval::Err);
}

}//namespace test
}//namespace vdjml
