/** @file "/vdjml/lib/test/codon_iterator_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE codon_iterator_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdjml/codon_iterator.hpp"

namespace vdjml{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( codon_iter ) {
   std::string s1 = "GGGGGAGGCCTGGTCAA";
   Codon_seq_iter csi(s1);
   BOOST_CHECK_EQUAL(csi.codon(), codon::GGG);
   BOOST_CHECK_EQUAL(translate(csi.codon()), aminoacid::Gly);
   csi.next();
   BOOST_CHECK( ! csi.at_end() );
   BOOST_CHECK_EQUAL(csi.codon(), codon::GGA);
   csi.next();
   BOOST_CHECK( ! csi.at_end() );
   BOOST_CHECK_EQUAL(csi.codon(), codon::GGC);
   csi.next();
   BOOST_CHECK( ! csi.at_end() );
   BOOST_CHECK_EQUAL(csi.codon(), codon::CTG);
   csi.next();
   BOOST_CHECK( ! csi.at_end() );
   BOOST_CHECK_EQUAL(csi.codon(), codon::GTC);
   csi.next();
   BOOST_CHECK( csi.at_end() );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( aligned_codon_iter_all ) {
   // 012345  678910121416
   // .....A--CG....AA... read
   // .....CAA--....--... gl
   // 01234567  8910  1214
   Btop b("5AC-A-AC-G-4A-A-3");
   const std::string rs = "ttttta" "cgttttaattt";
   const std::string gls ="tttttcaa" "tttt""ttt";

   typedef Aligned_codon_iter<In_seq_ref, All_positions> iter_t;
   iter_t i(
            b,
            Aligned_pos::read(0),
            In_seq_ref(Aligned_seq::read(rs)),
            All_positions()
   );
   BOOST_CHECK( ! i.at_end() );
   Codon_match cm;

   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 0);
   BOOST_CHECK_EQUAL(cm.codon(seq::Read), codon::TTT);
   BOOST_CHECK_EQUAL(cm.codon(seq::GL), codon::TTT);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 3);
   BOOST_CHECK_EQUAL(cm.codon(seq::Read), codon::TTA);
   BOOST_CHECK_EQUAL(cm.codon(seq::GL), codon::TTC);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 6);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 6);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "--C");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "AA-");

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 7);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 8);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "GTT");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "-TT");

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 10);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 10);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "TTA");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "TT-");

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 13);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 12);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "ATT");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "-TT");

   i.next();
   BOOST_CHECK( i.at_end() );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( aligned_codon_iter_read ) {
   // 012345  678910121416
   // .....A--CG....AA... read
   // .....CAA--....--... gl
   // 01234567  8910  1214
   Btop b("5AC-A-AC-G-4A-A-3");
   const std::string rs = "ttttta" "cgttttaattt";
   const std::string gls ="tttttcaa" "tttt""ttt";

   typedef Aligned_codon_iter<In_seq_val, Skip_seq_gaps> iter_t;
   iter_t i(
            b,
            Aligned_pos::read(0),
            In_seq_val(seq::Read, rs),
            Skip_seq_gaps(seq::Read)
   );
   BOOST_CHECK( ! i.at_end() );
   Codon_match cm;

   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 0);
   BOOST_CHECK_EQUAL(cm.codon(seq::Read), codon::TTT);
   BOOST_CHECK_EQUAL(cm.codon(seq::GL), codon::TTT);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 3);
   BOOST_CHECK_EQUAL(cm.codon(seq::Read), codon::TTA);
   BOOST_CHECK_EQUAL(cm.codon(seq::GL), codon::TTC);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 6);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 8);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "CGT");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "--T");

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 9);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 9);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "TTT");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "TTT");

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 12);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 12);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "AAT");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "--T");

   i.next();
   BOOST_CHECK( i.at_end() );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( aligned_codon_iter_gl ) {
   // 012345  678910121416
   // .....A--CG....AA... read
   // .....CAA--....--... gl
   // 01234567  8910  1214
   Btop b("5AC-A-AC-G-4A-A-3");
   const std::string rs = "ttttta" "cgttttaattt";
   const std::string gls ="tttttcaa" "tttt""ttt";

   typedef Aligned_codon_iter<In_seq_ref, Skip_seq_gaps> iter_t;
   iter_t i(
            b,
            Aligned_pos::read(0),
            In_seq_ref(Aligned_seq::read(rs)),
            Skip_seq_gaps(seq::GL)
   );
   BOOST_CHECK( ! i.at_end() );
   Codon_match cm;

   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 0);
   BOOST_CHECK_EQUAL(cm.codon(seq::Read), codon::TTT);
   BOOST_CHECK_EQUAL(cm.codon(seq::GL), codon::TTT);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 3);
   BOOST_CHECK_EQUAL(cm.codon(seq::Read), codon::TTA);
   BOOST_CHECK_EQUAL(cm.codon(seq::GL), codon::TTC);

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 6);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 6);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "--T");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "AAT");

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 9);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 9);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "TTT");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "TTT");

   i.next();
   BOOST_CHECK( ! i.at_end() );
   cm = i.get();
   BOOST_CHECK_EQUAL(cm.position(seq::Read), 14);
   BOOST_CHECK_EQUAL(cm.position(seq::GL), 12);
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::Read)), "TTT");
   BOOST_CHECK_EQUAL(to_string(cm.codon(seq::GL)), "TTT");

   i.next();
   BOOST_CHECK( i.at_end() );
}

}//namespace test
}//namespace vdjml
