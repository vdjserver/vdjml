/** @file "/vdjml/lib/test/istream_chunker_run.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#define BOOST_TEST_MODULE istream_chunker_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "istream_chunker.hpp"
#include <sstream>
namespace vdjml{ namespace test{

std::string xml1 =
         "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" "\n"
         "<vdjml version=\"0.0\" xmlns=\"http://vdjserver.org/xml/schema/vdjml/\">" "\n"
         "   <meta>"
         ;

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   std::istringstream iss(xml1);
   vdjml::detail::Istream_chunker<> ic(iss);
   vdjml::detail::Chunk c;
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "\n<vdjml version=\"0.0\" xmlns=\"http://vdjserver.org/xml/schema/vdjml/\">");
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   std::istringstream iss(xml1);
   vdjml::detail::Istream_chunker<20> ic(iss);
   vdjml::detail::Chunk c;
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "<?xml version=\"1.0\" ");
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "encoding=\"UTF-8\"?>");
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "\n<");
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "vdjml version=\"0.0\" ");
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "xmlns=\"http://vdjser");
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "ver.org/xml/schema/v");
   ic.next(c);
   BOOST_CHECK(! c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "djml/\">");
   ic.next(c);
   BOOST_CHECK(c.at_end_);
   BOOST_CHECK_EQUAL(c.str_, "\n   <meta>");
}

}//namespace test
}//namespace vdjml
