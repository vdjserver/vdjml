/** @file "/vdjml/lib/aligner_info.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/aligner_info.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Aligner_info const& ai,
         Results_meta const& rm,
         const unsigned version
) {
   typedef vocab::vdjml<>::meta::aligner vocab_t;
   xw.open(vocab_t::id_(), ELEM);

   xw.node(vocab_t::aligner_id::id_(), ATTR, ai.id());
   xw.node(vocab_t::name::id_(), ATTR, ai.name());
   xw.node(vocab_t::version::id_(), ATTR, ai.version());
   xw.node(vocab_t::uri::id_(), ATTR, ai.uri());
   xw.node(vocab_t::run_id::id_(), ATTR, ai.run_id());
   if( ai.parameters().size() ) xw.node(vocab_t::parameters::id_(), ELEM, ai.parameters());

   xw.close(); //aligner ELEM
}

}//namespace vdjml
