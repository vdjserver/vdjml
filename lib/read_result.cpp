/** @file "/vdjml/lib/read_result.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/read_result.hpp"

#include "boost/foreach.hpp"

#include "vdjml/results_meta.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {

/*
*******************************************************************************/
void Read_result::insert(Segment_combination const& sc) {
   BOOST_FOREACH(const Seg_match_id id, sc.segments()) {
      if( ! segment_matches().find(id) ) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("invalid segment match ID")
                  << Err::int1_t(id())
         );
      }
   }
   scm_.push_back(sc);
}

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Read_result const& rr,
         Results_meta const& rm,
         const unsigned version
) {
   typedef vocab::vdjml<>::read_results::read vocab_t;

   xw.open(vocab_t::id_(), ELEM);
   xw.node(vocab_t::read_id::id_(), ATTR, rr.id());

   xw.open(vocab_t::alignment::id_(), ELEM);

   BOOST_FOREACH(Segment_match const& sm, rr.segment_matches()) {
      write(xw, sm, rm, version);
   }

   BOOST_FOREACH(Segment_combination const& sc, rr.segment_combinations()) {
      write(xw, sc, rm, version);
   }

   xw.close(); //vocab_t::alignment::id_() ELEM

   xw.close(); //vocab_t::id_() ELEM
}

}//namespace vdjml
