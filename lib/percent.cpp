/** @file "/vdjml/lib/percent.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/percent.hpp"
#include <limits>

namespace vdjml {

/*
*******************************************************************************/
const float Percent::unset = std::numeric_limits<float>::quiet_NaN();

}//namespace vdjml
