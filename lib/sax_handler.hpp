/** @file "/vdjml/lib/sax_handler.hpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef SAX_HANDLER_HPP_
#define SAX_HANDLER_HPP_
#include <string>
#include <istream>
#include <iostream>
#include "boost/shared_ptr.hpp"
#include "libxml/parser.h"
#include "istream_chunker.hpp"
#include "vdjml/exception.hpp"

namespace vdjml{ namespace detail{
class Sax_builder;

/**@brief 
*******************************************************************************/
class Sax_handler {
public:
   struct Err : public base_exception {};

   Sax_handler(
            Sax_builder* builder,
            std::istream& is,
            char const* filename = 0
   );

   ~Sax_handler() {xmlFreeParserCtxt(ctx_);}
   void stop() {stop_requested_ = true;}
   void parse();
   boost::exception_ptr error() const {return err_;}

private:
   Sax_builder* builder_;
   Istream_chunker<> ic_;
   _xmlSAXHandler sh_;
   xmlParserCtxtPtr ctx_;
   bool stop_requested_;
   boost::exception_ptr err_;

   static _xmlSAXHandler init_handler();
   static void start_doc(void * ctx);
   static void end_doc(void * ctx);
   static void error(void* data, xmlErrorPtr err);

   static void start_element_ns(
            void * ctx,
            const xmlChar * localname,
            const xmlChar * prefix,
            const xmlChar * URI,
            int nb_namespaces,
            const xmlChar ** namespaces,
            int nb_attributes,
            int nb_defaulted,
            const xmlChar ** attributes
   );

   static void end_element_ns(void * ctx,
            const xmlChar * localname,
            const xmlChar * prefix,
            const xmlChar * URI
   );

   static void text(
            void *ctx,
            const xmlChar *ch,
            int len
   );

   static Sax_builder* get_builder(void* ctx);
   static Sax_handler* get_handler(void* ctx);
};

}//namespace detail
}//namespace vdjml
#endif /* SAX_HANDLER_HPP_ */
