/** @file "/vdjml/lib/segment_combination.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/segment_combination.hpp"

#include "boost/foreach.hpp"

#include "vdjml/results_meta.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Segment_combination const& sc,
         Results_meta const& rm,
         const unsigned version
) {
   typedef vocab::vdjml<>::read_results::read::alignment::combination
            vocab_t;

   xw.open(vocab_t::id_(), ELEM);
   xw.open(vocab_t::segments::id_(), ATTR);
   if( sc.segments().size() ) {
      xw.value(sc.segments()[0]);
      for(std::size_t n = 1; n != sc.segments().size(); ++n) {
         xw.value(' ');
         xw.value(sc.segments()[n]);
      }
   }
   xw.close(); //vocab_t::segments::id_(), ATTR

   BOOST_FOREACH(Gene_region const& gr, sc.regions()) {
      write(xw, gr, rm, version);
   }

   xw.close(); //combination, ELEM
}

}//namespace vdjml
