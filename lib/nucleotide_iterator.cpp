/** @file "/vdjml/lib/nucleotide_iterator.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/nucleotide_iterator.hpp"

#include <cctype>
#include "boost/foreach.hpp"
#include "boost/lexical_cast.hpp"

namespace vdjml{

namespace{ boost::array<std::size_t,2> p_init = {{0,0}}; }

/*
*******************************************************************************/
Mismatch_iter::Mismatch_iter(Btop const& bt)
: curr_(bt.val_.data()),
  end_(curr_ + bt.val_.size()),
  pos_(p_init)
{
   step_num();
}

/*
*******************************************************************************/
void Mismatch_iter::step_num() {
   int i = 0;
   for( ; (i != end_ - curr_) && std::isdigit(curr_[i]); ++i);
   if( i ) {
      const std::size_t n = boost::lexical_cast<std::size_t>(curr_, i);
      pos_[0] += n;
      pos_[1] += n;
      curr_ += i;
   }
}

/*
*******************************************************************************/
void Mismatch_iter::step_char() {
   if( is_gap(seq::Read) ) {
      ++pos_[seq::GL];
   } else {
      ++pos_[seq::Read];
      if( ! is_gap(seq::GL) ) ++pos_[seq::GL];
   }
   curr_ += 2;
}

namespace detail{

/**@brief
*******************************************************************************/
Aligned_nuc_iter_impl::Aligned_nuc_iter_impl(Btop const& btop)
: mi_(btop), pos_(p_init)
{}

}//namespace detail
}//namespace vdjml
