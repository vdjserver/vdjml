/** @file "/vdjml/lib/sb_meta.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef SB_META_HPP_
#define SB_META_HPP_
#include <vector>
#include "boost/optional.hpp"
#include "boost/shared_ptr.hpp"
#include "proto_elements.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/generator_info.hpp"
#include "vdjml/object_ids.hpp"
#include "vdjml/vocabulary.hpp"

namespace vdjml{
class Aligner_info;
class Gl_db_info;

namespace detail{
class Sax_builder;

/**@brief SAX builder for results meta structure
*******************************************************************************/
class Sb_meta {
public:
   typedef vocab::vdjml<>::meta vocab_t;
   struct Err : public base_exception {};
   explicit Sb_meta(Sax_builder* sb) : sb_(sb) {}
   void value(std::string const& name, std::string const& val);
   void open(std::string const& name);
   void close(std::string const& name) {}


   void insert(Vdjml_generator_info const& gi);
   void insert(Proto_aligner_info const& ai);
   void insert(Proto_gl_db_info const& gl_db);

private:
   Sax_builder* sb_;
};

/**@brief SAX builder for generator info structure
*******************************************************************************/
class Sb_generator {
public:
   typedef Sb_meta::vocab_t::generator vocab_t;
   struct Err : public base_exception {};
   Sb_generator(Sb_meta* sm) : sm_(sm) {}

   void value(std::string const& name, std::string const& val);

   void open(std::string const& name) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected element")
               << Err::str1_t(name)
               << Err::str2_t(vocab_t::id_())
      );
   }

   void close(std::string const& name);

private:
   Sb_meta* sm_;
   std::string name_;
   std::string version_;
   boost::optional<boost::posix_time::ptime> time_gmt_;
};

/**@brief SAX builder for aligner info structure
*******************************************************************************/
class Sb_aligner {
public:
   typedef Sb_meta::vocab_t::aligner vocab_t;
   struct Err : public base_exception {};

   Sb_aligner(Sax_builder* sb, Sb_meta* sm)
   : sb_(sb), sm_(sm)
   {}

   void value(std::string const& name, std::string const& val);
   void open(std::string const& name);
   void close(std::string const& name) {sm_->insert(pai_);}
   void set_parameter(std::string const& param) {pai_.parameters_ = param;}
private:
   Sax_builder* sb_;
   Sb_meta* sm_;
   Proto_aligner_info pai_;
};

/**@brief SAX builder for generator info structure
*******************************************************************************/
class Sb_aligner_params {
public:
   typedef Sb_aligner::vocab_t::parameters vocab_t;
   struct Err : public base_exception {};
   Sb_aligner_params(Sb_aligner* sa) : sa_(sa) {}

   void value(std::string const& name, std::string const& val);

   void open(std::string const& name) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected element")
               << Err::str1_t(name)
               << Err::str2_t(vocab_t::id_())
      );
   }

   void close(std::string const& name) {sa_->set_parameter(parameters_);}

private:
   Sb_aligner* sa_;
   std::string parameters_;
};

/**@brief SAX builder for germline database info structure
*******************************************************************************/
class Sb_germline_db {
public:
   typedef Sb_meta::vocab_t::germline_db vocab_t;
   struct Err : public base_exception {};

   Sb_germline_db(Sb_meta* sm) : sm_(sm)
   {}

   void value(std::string const& name, std::string const& val);

   void open(std::string const& name) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected element")
               << Err::str1_t(name)
               << Err::str2_t(vocab_t::id_())
      );
   }

   void close(std::string const& name);

private:
   Sb_meta* sm_;
   Proto_gl_db_info pgdi_;
};

}//namespace detail
}//namespace vdjml
#endif /* SB_META_HPP_ */
