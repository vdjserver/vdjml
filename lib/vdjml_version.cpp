/** @file "/vdjml/lib/vdjml_version.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/vdjml_version.hpp"

#include <limits>
#include "boost/spirit/include/qi.hpp"
#include "boost/spirit/include/phoenix_core.hpp"
#include "boost/spirit/include/phoenix_operator.hpp"

namespace vdjml { namespace{
template <class Iter>
bool parse_version(Iter first, Iter last, unsigned& major, unsigned& minor)
{
    using boost::spirit::qi::uint_;
    using boost::spirit::qi::_1;
    using boost::spirit::qi::phrase_parse;
    using boost::spirit::ascii::space;
    using boost::phoenix::ref;

    bool r = phrase_parse(
             first,
             last,
             (uint_[ref(major) = _1] >> '.' >> uint_[ref(minor) = _1]),
             space
    );

    return r && first == last;
}

}//anonymous namespace


/*
*******************************************************************************/
unsigned version(std::string const& v) {
   unsigned major, minor;
   parse_version(v.begin(), v.end(), major, minor);
   return version(major, minor);
}


}//namespace vdjml
