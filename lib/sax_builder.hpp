/** @file "/vdjml/lib/sax_builder.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef SAX_BUILDER_HPP_
#define SAX_BUILDER_HPP_
#include <string>
#include <deque>
#include <map>
#include "boost/algorithm/string/classification.hpp"
#include "boost/algorithm/string/predicate.hpp"
#include "boost/assert.hpp"
#include "boost/optional.hpp"
#include "boost/variant.hpp"

#include "proto_results_meta.hpp"
#include "sb_meta.hpp"
#include "sb_result.hpp"
#include "vdjml/exception.hpp"
#include "vdjml/read_result_fwd.hpp"
#include "vdjml/vdjml_reader.hpp"
#include "vdjml/vdjml_version.hpp"
#include "vdjml/vocabulary.hpp"

namespace vdjml{ namespace detail{
class Sax_builder;

/**@brief Process (and ignore) any element
*******************************************************************************/
class Sb_any{
public:
   struct Err : public base_exception {};
   explicit Sb_any(Sax_builder* sb, std::string const& name)
   : sb_(sb), name_(name) {}

   void open(std::string const& name);
   void close(std::string const& name) {}
   void value(std::string const& name, std::string const& val) {}
   std::string const& name() const {return name_;}

private:
   Sax_builder* sb_;
   std::string name_;
};

/**@brief Process top-level vdjml element
*******************************************************************************/
class Sb_vdjml{
public:
   struct Err : public base_exception {};
   typedef vocab::vdjml<> vocab_t;
   explicit Sb_vdjml(Sax_builder* sb) : sb_(sb) {}
   void open(std::string const& name);
   void close(std::string const& name) {}
   void value(std::string const& name, std::string const& val);

private:
   Sax_builder* sb_;
};

/**@brief Process read_result element
*******************************************************************************/
class Sb_read_results{
public:
   struct Err : public base_exception {};
   typedef vocab::vdjml<>::read_results vocab_t;
   explicit Sb_read_results(Sax_builder* sb) : sb_(sb) {}
   void open(std::string const& name);
   void close(std::string const& name) {}
   void value(std::string const& name, std::string const& val);

private:
   Sax_builder* sb_;
};

/**@brief
*******************************************************************************/
class Open_visitor : public boost::static_visitor<> {
public:
   explicit Open_visitor(std::string const& name)
   : local_name_(name)
   {}

   template<class Builder> void operator() (Builder& b) const {
      b.open(local_name_);
   }

private:
   std::string const& local_name_;
};

/**@brief
*******************************************************************************/
class Close_visitor : public boost::static_visitor<> {
public:
   explicit Close_visitor(std::string const& name)
   : local_name_(name)
   {}

   template<class Builder> void operator() (Builder& b) const {
      b.close(local_name_);
   }

private:
   std::string const& local_name_;
};

/**@brief
*******************************************************************************/
class Value_visitor : public boost::static_visitor<> {
public:
   Value_visitor(std::string const& name, std::string const& val)
   : name_(name), val_(val)
   {}

   template<class Builder> void operator() (Builder& b) const {
      b.value(name_, val_);
   }

private:
   std::string const& name_;
   std::string const& val_;
};

/**@brief
*******************************************************************************/
struct Name_visitor : public boost::static_visitor<std::string const&> {
   template<class Builder> std::string const& operator() (Builder const&) const {
      return Builder::vocab_t::id_();
   }

   std::string const& operator() (Sb_any const& sba) const {return sba.name();}
};

/**@brief Assemble objects from SAX calls
*******************************************************************************/
class Sax_builder {
public:
   typedef boost::variant<
            Sb_any,
            Sb_vdjml,
            Sb_meta,
            Sb_generator, Sb_aligner, Sb_germline_db, Sb_aligner_params,
            Sb_read_results,
            Sb_read,
            Sb_alignment,
            Sb_seg_match, Sb_btop, Sb_gl_segment, Sb_aa_sub,
            Sb_combination, Sb_region
            > builder_variant;

   //use deque to preserve pointers
   typedef std::deque<builder_variant> bv_vector;
private:
   friend class Sb_vdjml;
   friend class Sb_meta;
   friend class Sb_read;

public:
   struct Err : public base_exception {};
   typedef Vdjml_reader::meta_ptr_t meta_ptr_t;
   typedef Vdjml_reader::result_ptr_t result_ptr_t;

   static bool is_top_element(std::string const& name) {
      return
               name == vocab::vdjml<>::meta::id_() ||
               name == vocab::vdjml<>::read_results::read::id_();
   }

   Sax_builder()
   : v_(),
     prm_(),
     pr_(prm_),
     bvv_()
   {}

   explicit Sax_builder(boost::shared_ptr<Results_meta> rm)
   : v_(),
     prm_(rm),
     pr_(prm_),
     bvv_()
   {}

   void open(std::string const& name) {
      if( bvv_.empty() ) {
         if( name == Sb_vdjml::vocab_t::id_() ) {
            push(Sb_vdjml(this));
            BOOST_ASSERT( ! bvv_.empty() );
            return;
         }
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected top element")
                  << Err::str1_t(name)
         );
      }
      boost::apply_visitor(Open_visitor(name), bvv_.back());
   }

   void close(std::string const& name) {
      if( bvv_.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("mismatched closing element")
               << Err::str1_t(name)
      );
      std::string const& name0 = apply_visitor(Name_visitor(), bvv_.back());
      if( name0 != name ) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("mismatched closing element")
                  << Err::str1_t(name0)
                  << Err::str2_t(name)
         );
      }
      boost::apply_visitor(Close_visitor(name), bvv_.back());
      bvv_.pop_back();
   }

   void value(std::string const& name, std::string const& val) {
      if(
               name.empty() &&
               ( val.empty() || all(val, boost::algorithm::is_space()) )
      ) return;

      if( bvv_.empty() ) {
         if( name.empty() ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected root text")
                  << Err::str1_t("\"" + val + "\"")
         );

         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected root attribute")
                  << Err::str1_t(name + "=\"" + val + "\"")
         );
      }

      boost::apply_visitor(Value_visitor(name, val), bvv_.back());
   }

   unsigned version() const {
      if( ! v_ ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("version is not defined")
      );
      return v_.get();
   }
   meta_ptr_t meta() {return prm_.meta_ptr();}
   Read_result&         result()       {return pr_.deref();}
   Read_result const&   result() const {return pr_.deref();}
   result_ptr_t         result_release() {return  pr_.release();}
   bool has_result() const {return pr_.has_result();}

   void push(builder_variant const& bv) {bvv_.push_back(bv);}

   Vdjml_generator_info const&   generator_info() const {return gi_;}
   Vdjml_generator_info&         generator_info() {return gi_;}
   Proto_results_meta const&  proto_meta() const   {return prm_;}
   Proto_results_meta&        proto_meta()         {return prm_;}
   Proto_read_result const&   proto_result() const {return pr_;}
   Proto_read_result&         proto_result()       {return pr_;}

private:
   boost::optional<unsigned> v_;
   Vdjml_generator_info gi_;
   Proto_results_meta prm_;
   Proto_read_result pr_;
   bv_vector bvv_;
};

}//namespace detail
}//namespace vdjml
#endif /* SAX_BUILDER_HPP_ */
