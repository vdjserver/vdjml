/** @file "/vdjml/lib/sb_result.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#include "sb_result.hpp"

#include "boost/lexical_cast.hpp"
#include "boost/tokenizer.hpp"

#include "utils_io.hpp"
#include "sax_builder.hpp"
#include "vdjml/read_result.hpp"
#include "vdjml/results_meta.hpp"

namespace vdjml{ namespace detail{

/*
*******************************************************************************/
bool parse(std::string const& name, std::string const& val, Match_metrics& mm) {
   typedef vocab::vdjml<>::match_metrics mm_vocab;
   if( name == mm_vocab::identity::id_() ) {
      mm.identity_ = parse_percent(val);
      return true;
   }

   if( name == mm_vocab::score::id_() ) {
      mm.score_ = boost::lexical_cast<Match_metrics::score_t>(val);
      return true;
   }

   if( name == mm_vocab::insertions::id_() ) {
      mm.insertions_ = parse_unsigned<Match_metrics::count_t>(val);
      return true;
   }

   if( name == mm_vocab::deletions::id_() ) {
      mm.deletions_ = parse_unsigned<Match_metrics::count_t>(val);
      return true;
   }

   if( name == mm_vocab::substitutions::id_() ) {
      mm.substitutions_ = parse_unsigned<Match_metrics::count_t>(val);
      return true;
   }

   if( name == mm_vocab::stop_codon::id_() && parse_bool(val) ) {
      mm.v_ |= 1 << Match_metrics::StopCodon;
      return true;
   }

   if( name == mm_vocab::mutated_invariant::id_() && parse_bool(val) ) {
      mm.v_ |= 1 << Match_metrics::MutatedInvariant;
      return true;
   }

   if( name == mm_vocab::inverted::id_() && parse_bool(val) ) {
      mm.v_ |= 1 << Match_metrics::Inverted;
      return true;
   }

   if( name == mm_vocab::out_frame_indel::id_() && parse_bool(val) ) {
      mm.v_ |= 1 << Match_metrics::OutFrameIndel;
      return true;
   }

   if( name == mm_vocab::out_frame_vdj::id_() && parse_bool(val) ) {
      mm.v_ |= 1 << Match_metrics::OutFrameVDJ;
      return true;
   }

   return false;
}

/*
*******************************************************************************/
void Sb_read::value(std::string const& name, std::string const& val) {
   if( name == vocab_t::read_id::id_() ) {
      sb_->proto_result().id(val);
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected text")
            << Err::str1_t("\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );

   //other attributes are allowed (and ignored) under <vdjml:read>
}

/*
*******************************************************************************/
void Sb_read::open(std::string const& name) {
   if( ! sb_->has_result() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("read_id not provided")
            << Err::str2_t(vocab_t::id_())
   );

   if( name == vocab_t::alignment::id_() ) {
      sb_->push(Sb_alignment(sb_));
      return;
   }

   //other elements are allowed (and ignored) under <vdjml:read>
   sb_->push(Sb_any(sb_, name));
}

/*
*******************************************************************************/
void Sb_alignment::open(std::string const& name) {
   if( name == vocab_t::segment_match::id_() ) {
      sb_->push(Sb_seg_match(sb_, this));
      return;
   }

   if( name == vocab_t::combination::id_() ) {
      sb_->push(Sb_combination(sb_, this));
      return;
   }

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected element")
            << Err::str1_t(name)
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_alignment::insert(Proto_segment_match const& psm) {
   sb_->proto_result().insert(psm);
}

/*
*******************************************************************************/
void Sb_alignment::insert(Proto_combination const& pc) {
   sb_->proto_result().insert(pc);
}

/*
*******************************************************************************/
void Sb_seg_match::open(std::string const& name) {
   if( name == vocab_t::btop::id_() ) {
      if( psm_.btop_ ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("duplicate BTOP element")
               << Err::str1_t(name)
               << Err::str2_t(vocab_t::id_())
      );
      sb_->push(Sb_btop(this));
      return;
   }

   if( name == vocab_t::gl_seg_match::id_() ) {
      sb_->push(Sb_gl_segment(this));
      return;
   }

   if( name == vocab_t::aa_substitution::id_() ) {
      sb_->push(Sb_aa_sub(this));
      return;
   }

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected element")
            << Err::str1_t(name)
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_seg_match::close(std::string const& name) {
   if( psm_.id_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("not defined")
            << Err::str1_t(vocab_t::segment_match_id::id_())
   );

   sba_->insert(psm_);
}

/*
*******************************************************************************/
Sb_seg_match::Sb_seg_match(Sax_builder* sb, Sb_alignment* sba)
: sb_(sb),
  sba_(sba),
  psm_(sb_->proto_meta())
{}

/*
*******************************************************************************/
void Sb_seg_match::value(std::string const& name, std::string const& val) {
   if( parse(name, val, psm_.mm_) ) return;

   if( name == vocab_t::segment_match_id::id_() ) {
      psm_.id_ = val;
      return;
   }

   if( name == vocab_t::read_pos0::id_() ) {
      psm_.read_pos0_ = parse_unsigned<unsigned>(val);
      return;
   }

   if( name == vocab_t::read_len::id_() ) {
      psm_.read_len_ = parse_unsigned<unsigned>(val);
      return;
   }

   if( name == vocab_t::gl_len::id_() ) {
      psm_.gl_len_ = parse_unsigned<unsigned>(val);
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected text")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected attribute")
               << Err::str1_t(name + "=\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_btop::value(std::string const& name, std::string const& val) {
   if( name.empty() ) {
      str_ += val;
      return;
   }

   BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected attribute")
               << Err::str1_t(name + "=\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_gl_segment::value(std::string const& name, std::string const& val) {
   if( name == vocab_t::gl_seg_match_id::id_() ) {
      pgsm_.id_ = val;
      return;
   }

   if( name == vocab_t::num_system_t::id_() ) {
      pgsm_.num_system_ = val;
      return;
   }

   if( name == vocab_t::name::id_() ) {
      pgsm_.name_ = val;
      return;
   }

   if( name == vocab_t::gl_db_id::id_() ) {
      pgsm_.gl_db_id_ = val;
      return;
   }

   if( name == vocab_t::gl_pos0::id_() ) {
      pgsm_.gl_pos0_ = parse_unsigned<unsigned>(val);
      return;
   }

   if( name == vocab_t::type::id_() ) {
      if( val.size() != 1 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("germline segment type should be a single character")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
      );
      pgsm_.type_ = val[0];
      return;
   }

   if( name == vocab_t::aligner_id::id_() ) {
      pgsm_.aligner_id_ = val;
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected text")
            << Err::str1_t("\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_gl_segment::close(std::string const& name) {
   check();
   sm_->insert(pgsm_);
}

/*
*******************************************************************************/
void Sb_gl_segment::check() const {
   if( pgsm_.id_.empty() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("germline segment match ID not defined")
      << Err::str2_t(vocab_t::gl_seg_match_id::id_())
   );

   if( pgsm_.name_.empty() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("germline segment name not defined")
      << Err::str2_t(vocab_t::name::id_())
   );

   if( ! pgsm_.type_ ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("germline segment type not defined")
      << Err::str2_t(vocab_t::type::id_())
   );

   if( ! pgsm_.gl_pos0_ ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("germline segment position not defined")
      << Err::str2_t(vocab_t::gl_pos0::id_())
   );

   if( ! pgsm_.gl_pos0_ ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("germline segment position not defined")
      << Err::str2_t(vocab_t::gl_pos0::id_())
   );

   if( pgsm_.num_system_.empty() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("germline segment numbering system not defined")
      << Err::str2_t(vocab_t::num_system_t::id_())
   );

   if( pgsm_.gl_db_id_.empty() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("germline segment database not defined")
      << Err::str2_t(vocab_t::gl_db_id::id_())
   );

   if( pgsm_.aligner_id_.empty() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("germline segment match aligner not defined")
      << Err::str2_t(vocab_t::aligner_id::id_())
   );
}

/*
*******************************************************************************/
void Sb_aa_sub::value(std::string const& name, std::string const& val) {
   if( name == vocab_t::read_pos0::id_() ) {
      read_pos0_ = parse_unsigned<unsigned>(val);
      return;
   }

   if( name == vocab_t::read_aa::id_() ) {
      read_aa_ = aminoacid_index(val);
      return;
   }

   if( name == vocab_t::gl_aa::id_() ) {
      gl_aa_ = aminoacid_index(val);
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected text")
            << Err::str1_t("\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_aa_sub::close(std::string const& name) {
   if( name != vocab_t::id_() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("closing unexpected element")
            << Err::str1_t(name)
            << Err::str2_t(vocab_t::id_())
   );

   if( ! read_pos0_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined read position")
            << Err::str1_t(vocab_t::read_pos0::id_())
   );

   if( ! read_aa_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined read amino acid")
            << Err::str1_t(vocab_t::read_aa::id_())
   );

   if( ! gl_aa_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined germline amino acid")
            << Err::str1_t(vocab_t::gl_aa::id_())
   );

   sm_->insert(Aa_substitution(read_pos0_.get(), read_aa_.get(), gl_aa_.get()));
}

/*
*******************************************************************************/
void Sb_combination::open(std::string const& name) {
   if( name == vocab_t::region::id_() ) {
      sb_->push(Sb_region(this));
      return;
   }

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected element")
            << Err::str1_t(name)
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_combination::value(std::string const& name, std::string const& val) {
   if( name == vocab_t::segments::id_() ) {
      typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
      typedef tokenizer::const_iterator iterator;
      boost::char_separator<char> separator(" ");
      tokenizer tok(val, separator);
      for(iterator i = tok.begin(); i != tok.end(); ++i) {
         if( i->size() ) pc_.midv_.push_back(*i);
      }
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected text")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_region::value(std::string const& name, std::string const& val) {
   if( parse(name, val, pgr_.mm_) ) return;

   if( name == vocab_t::name::id_() ) {
      pgr_.region_name_ = val;
      return;
   }

   if( name == vocab_t::num_system_t::id_() ) {
      pgr_.num_system_ = val;
      return;
   }

   if( name == vocab_t::aligner_id::id_() ) {
      pgr_.aligner_id_ = val;
      return;
   }

   if( name == vocab_t::read_pos0::id_() ) {
      pgr_.read_pos_ = parse_unsigned<unsigned>(val);
      return;
   }

   if( name == vocab_t::read_len::id_() ) {
      pgr_.read_len_ = parse_unsigned<unsigned>(val);
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected text")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected attribute")
               << Err::str1_t(name + "=\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_region::close(std::string const& name) {
   if( name == vocab_t::id_() ) {
      if( pgr_.region_name_.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("region name attribute not defined")
               << Err::str1_t(vocab_t::name::id_())
      );

      if( pgr_.aligner_id_.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("aligner ID attribute not defined")
               << Err::str1_t(vocab_t::aligner_id::id_())
      );

      if( ! pgr_.read_pos_ ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("attribute not defined")
               << Err::str1_t(vocab_t::read_pos0::id_())
      );

      if( ! pgr_.read_len_ ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("undefined read interval length")
               << Err::str1_t(vocab_t::read_len::id_())
      );

      sbc_->insert(pgr_);
      return;
   }

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("closing unexpected element")
            << Err::str1_t(name)
            << Err::str2_t(vocab_t::id_())
   );
}

}//namespace detail
}//namespace vdjml
