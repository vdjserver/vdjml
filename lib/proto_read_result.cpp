/** @file "/vdjml/lib/proto_read_result.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "proto_read_result.hpp"

#include "boost/foreach.hpp"

#include "proto_elements.hpp"
#include "proto_segment_match.hpp"
#include "vdjml/read_result.hpp"
#include "vdjml/segment_combination.hpp"

namespace vdjml{ namespace detail{

/*
*******************************************************************************/
Proto_read_result::Proto_read_result(
         Proto_results_meta& prm,
         std::string const& id
)
: prm_(prm),
  rr_(new Read_result(id))
{}

/*
*******************************************************************************/
Proto_read_result::Proto_read_result(
         Proto_results_meta& prm,
         read_result_ptr rr
)
: prm_(prm),
  rr_(rr)
{}

/*
*******************************************************************************/
void Proto_read_result::id(std::string const& id) {
   if( rr_.get() ) {
      if( rr_->id() != id ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("read ID re-definition")
               << Err::str1_t(id)
               << Err::str2_t(rr_->id())
      );
   } else {
      rr_.reset(new Read_result(id));
      smidm_.clear();
   }
}

/*
*******************************************************************************/
Seg_match_id Proto_read_result::insert(Proto_segment_match const& psm) {
   const sm_id_map::const_iterator i = smidm_.find(psm.id_);
   if( i != smidm_.end() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("duplicate segment match ID")
      << Err::str1_t(psm.id_)
   );
   const Segment_match sm = psm.make();
   const Seg_match_id smid = rr_->insert(sm);
   smidm_[psm.id_] = smid;
   return smid;
}

/*
*******************************************************************************/
Seg_match_id Proto_read_result::segment_id(std::string const& id) const {
   const sm_id_map::const_iterator i = smidm_.find(id);
   if( i == smidm_.end() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("unknown segment match ID")
      << Err::str1_t(id)
   );
   return i->second;
}

/*
*******************************************************************************/
void Proto_read_result::insert(Proto_combination const& pc) {
   Segment_combination sc((Seg_match_id()));
   BOOST_FOREACH(std::string const& str, pc.midv_) {
      const Seg_match_id id = segment_id(str);
      sc.insert(id);
   }

   BOOST_FOREACH(Proto_gene_region const& pgr, pc.pgrv_) {
      const Gene_region gr = make(pgr, prm_);
      sc.insert(gr);
   }
   rr_->insert(sc);
}

}//namespace detail
}//namespace vdjml
