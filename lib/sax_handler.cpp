/** @file "/vdjml/lib/sax_handler.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "sax_handler.hpp"

#include <limits>

#include "sax_builder.hpp"

namespace vdjml{ namespace detail{

/*
*******************************************************************************/
_xmlSAXHandler Sax_handler::init_handler() {
   _xmlSAXHandler h;

   h.internalSubset        = 0; //internalSubsetSAXFunc
   h.isStandalone          = 0; //isStandaloneSAXFunc
   h.hasInternalSubset     = 0; //hasInternalSubsetSAXFunc
   h.hasExternalSubset     = 0; //hasExternalSubsetSAXFunc
   h.resolveEntity         = 0; //resolveEntitySAXFunc
   h.getEntity             = 0; //getEntitySAXFunc
   h.entityDecl            = 0; //entityDeclSAXFunc
   h.notationDecl          = 0; //notationDeclSAXFunc
   h.attributeDecl         = 0; //attributeDeclSAXFunc
   h.elementDecl           = 0; //elementDeclSAXFunc
   h.unparsedEntityDecl    = 0; //unparsedEntityDeclSAXFunc
   h.setDocumentLocator    = 0; //setDocumentLocatorSAXFunc
   h.startDocument         = &start_doc;        //startDocumentSAXFunc
   h.endDocument           = &end_doc;          //endDocumentSAXFunc
   h.startElement          = 0; //startElementSAXFunc
   h.endElement            = 0; //endElementSAXFunc
   h.reference             = 0; //referenceSAXFunc
   h.characters            = &text; //charactersSAXFunc
   h.ignorableWhitespace   = 0; //ignorableWhitespaceSAXFunc
   h.processingInstruction = 0; //processingInstructionSAXFunc
   h.comment               = 0; //commentSAXFunc
   h.warning               = 0; //warningSAXFunc
   h.error                 = 0; //errorSAXFunc
   h.fatalError            = 0; //fatalErrorSAXFunc unused error() get all the errors
   h.getParameterEntity    = 0; //getParameterEntitySAXFunc
   h.cdataBlock            = 0; //cdataBlockSAXFunc
   h.externalSubset        = 0; //externalSubsetSAXFunc
   h.initialized           = XML_SAX2_MAGIC;    //unsigned int
   h._private              = 0;                 //void *
   h.startElementNs        = start_element_ns;  //startElementNsSAX2Func
   h.endElementNs          = &end_element_ns;   //endElementNsSAX2Func
   h.serror                = &error;            //xmlStructuredErrorFunc

   return h;
}

/*
*******************************************************************************/
Sax_handler::Sax_handler(
         Sax_builder* builder,
         std::istream& is,
         char const* filename
)
: builder_(builder),
  ic_(is),
  sh_(init_handler()),
  ctx_(),
  stop_requested_(true)
{
   Chunk chunk;
   ic_.next(chunk);
   ctx_ = xmlCreatePushParserCtxt(
            &sh_,
            0,
            chunk.str_.data(),
            chunk.str_.size(),
            filename
   );
   if( ! ctx_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("error initializing parser")
   );
   ctx_->_private = this;
}

/*
*******************************************************************************/
void Sax_handler::parse() {
   stop_requested_ = false;
   while( ( ! stop_requested_) ) {
      Chunk chunk;
      ic_.next(chunk);

      const int err = xmlParseChunk(
               ctx_,
               chunk.str_.data(),
               chunk.str_.size(),
               chunk.at_end_
      );

      if( err ) {
         xmlErrorPtr ep = xmlCtxtGetLastError(ctx_);
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("parsing error")
                  << Err::str1_t(ep->message)
         );
      }

      if( chunk.at_end_) return;
   }
}

/*
*******************************************************************************/
Sax_builder* Sax_handler::get_builder(void* ctx) {
   return get_handler(ctx)->builder_;
}

/*
*******************************************************************************/
Sax_handler* Sax_handler::get_handler(void* ctx) {
   xmlParserCtxtPtr p = static_cast<xmlParserCtxtPtr>(ctx);
   Sax_handler* h = static_cast<Sax_handler*>(p->_private);
   return h;
}

/*
*******************************************************************************/
void Sax_handler::start_element_ns(
         void * ctx,
         const xmlChar * localname,
         const xmlChar * prefix,
         const xmlChar * URI,
         int nb_namespaces,
         const xmlChar ** namespaces,
         int nb_attributes,
         int nb_defaulted,
         const xmlChar ** attributes
) {
   Sax_builder* sb = get_builder(ctx);
   try{
      sb->open((char const*)localname);
   } catch(std::exception const&) {
      Err e;
      e
      << Err::msg_t("parsing error")
      << Err::line_t(xmlSAX2GetLineNumber(ctx))
      << Err::nested_t(boost::current_exception())
      ;
      get_handler(ctx)->err_ = boost::copy_exception(e);
      BOOST_THROW_EXCEPTION(e);
   }

   for(int n = 0; n != nb_attributes * 5; n += 5) {
      const std::string name = (char const*)attributes[n];
      char const* v0 = (char const*)attributes[n + 3];
      //attribute values may appear as
      // FR1" num_system="IMGT" pos0="0" len="54" identity="100%" score="54"/>
      // instead of FR1
      const std::string val(v0, (char const*)attributes[n + 4] - v0);
      try{
         sb->value(name, val);
      } catch(std::exception const&) {
         Err e;
         e
         << Err::msg_t("parsing error")
         << Err::line_t(xmlSAX2GetLineNumber(ctx))
         << Err::nested_t(boost::current_exception())
         ;
         get_handler(ctx)->err_ = boost::copy_exception(e);
         BOOST_THROW_EXCEPTION(e);
      }
   }
}

/*
*******************************************************************************/
void Sax_handler::end_element_ns(
         void * ctx,
         const xmlChar * localname,
         const xmlChar * prefix,
         const xmlChar * URI
) {
   const std::string ln = (char const*)localname;
   try{
      get_builder(ctx)->close(ln);
   } catch(std::exception const&) {
      Err e;
      e
      << Err::msg_t("parsing error")
      << Err::line_t(xmlSAX2GetLineNumber(ctx))
      << Err::nested_t(boost::current_exception())
      ;
      get_handler(ctx)->err_ = boost::copy_exception(e);
      BOOST_THROW_EXCEPTION(e);
   }

   if( Sax_builder::is_top_element(ln) ) get_handler(ctx)->stop();
}

/*
*******************************************************************************/

void Sax_handler::text(
         void *ctx,
         const xmlChar *ch,
         int len
) {
   try{
      get_builder(ctx)->value("", std::string((char const*)ch, len));
   } catch(std::exception const&) {
      Err e;
      e
      << Err::msg_t("parsing error")
      << Err::line_t(xmlSAX2GetLineNumber(ctx))
      << Err::nested_t(boost::current_exception())
      ;
      get_handler(ctx)->err_ = boost::copy_exception(e);
      BOOST_THROW_EXCEPTION(e);
   }
}

/*
*******************************************************************************/
void Sax_handler::start_doc(void * ctx) {}

/*
*******************************************************************************/
void Sax_handler::end_doc(void * ctx) {}

/*
*******************************************************************************/
void Sax_handler::error(void* data, xmlErrorPtr err) {}

}//namespace detail
}//namespace vdjml
