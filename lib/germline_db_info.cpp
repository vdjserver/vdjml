/** @file "/vdjml/lib/germline_db_info.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/germline_db_info.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"
#include "vdjml/results_meta.hpp"

namespace vdjml {

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Gl_db_info const& gdi,
         Results_meta const& rm,
         const unsigned version
) {
   typedef vocab::vdjml<>::meta::germline_db vocab_t;
   xw.open(vocab_t::id_(), ELEM);

   xw.node(vocab_t::gl_db_id::id_(), ATTR, gdi.id());
   xw.node(vocab_t::name::id_(), ATTR, gdi.name());
   xw.node(vocab_t::species::id_(), ATTR, gdi.species());
   xw.node(vocab_t::version::id_(), ATTR, gdi.version());
   if( gdi.uri().size() ) xw.node(vocab_t::uri::id_(), ATTR, gdi.uri());

   xw.close(); //vocab_t::id_() ELEM
}

}//namespace vdjml
