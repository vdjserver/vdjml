/** @file "/vdjml/lib/proto_segment_match.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#include "proto_segment_match.hpp"

#include "boost/foreach.hpp"

#include "proto_results_meta.hpp"

namespace vdjml{ namespace detail{

/*
*******************************************************************************/
Segment_match Proto_segment_match::make() const {
   if( id_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("segment match ID not defined")
   );

   if( ! read_pos0_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("read match starting position not defined")
   );

   if( ! read_len_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("read match length not defined")
   );

   if( ! btop_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("BTOP not defined")
   );

   if( ! gl_len_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("germline match length not defined")
   );

   if( glsmv_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("no germline segment matches defined")
   );

   Segment_match sm(read_pos0_.get(), Btop(btop_.get()), mm_);

   BOOST_FOREACH(Gl_segment_match const& gsm, glsmv_) sm.insert(gsm);
   BOOST_FOREACH(Aa_substitution const& aas, aasv_) sm.insert(aas);

   return sm;
}

/*
*******************************************************************************/
void Proto_segment_match::insert(Proto_gl_segment_match const& pgsm) {
   const Numsys_id ns = prm_.meta().num_system_map().insert(pgsm.num_system_);
   const Aligner_id aid = prm_.aligner_id(pgsm.aligner_id_);
   const Gl_db_id dbid = prm_.gl_db_id(pgsm.gl_db_id_);
   const Gl_segment_info gsi(dbid, pgsm.type_.get(), pgsm.name_);
   const Gl_seg_id gsid = prm_.meta().insert(gsi);
   const Gl_segment_match gsm(
      ns,
      aid,
      gsid,
      pgsm.gl_pos0_.get()
   );
   glsmv_.push_back(gsm);
}

}//namespace detail
}//namespace vdjml
