/** @file "/vdjml/lib/gene_region.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/gene_region.hpp"

#include "utils_io.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Gene_region const& gr,
         Results_meta const& rm,
         const unsigned v
) {
   typedef
            vocab::vdjml<>::read_results::read::alignment::
            combination::region
            vocab_t;

   xw.open(vocab_t::id_(), ELEM);
   xw.node(vocab_t::name::id_(), ATTR, rm.gene_region_map()[gr.region_type()]);
   if( gr.numbering_system() ) write_id(xw, gr.numbering_system(), rm, v);
   write_id(xw, gr.aligner(), rm, v);

   xw.node(vocab_t::read_pos0::id_(), ATTR, gr.read_range().pos0());
   xw.node(vocab_t::read_len::id_(), ATTR, gr.read_range().length());

   write(xw, gr.match_metrics(), v);
   xw.close(); //vocab_t::id_(), ELEM
}

}//namespace vdjml
