/** @file "/vdjml/lib/proto_elements.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef PROTO_ELEMENTS_HPP_
#define PROTO_ELEMENTS_HPP_
#include <string>
#include <vector>
#include "boost/optional.hpp"
#include "vdjml/match_metrics.hpp"
#include "vdjml/gene_region.hpp"

namespace vdjml{ namespace detail{
class Proto_results_meta;

/**@brief
*******************************************************************************/
struct Proto_aligner_info {
   Proto_aligner_info() : run_id_(0) {}
   unsigned run_id_;
   std::string id_;
   std::string name_;
   std::string version_;
   std::string parameters_;
   std::string uri_;

};

/**@brief
*******************************************************************************/
struct Proto_gl_db_info {
   std::string id_;
   std::string name_;
   std::string version_;
   std::string species_;
   std::string uri_;
};

/**@brief
*******************************************************************************/
struct Proto_gene_region {
   std::string region_name_;
   std::string num_system_;
   std::string aligner_id_;
   Match_metrics mm_;
   boost::optional<unsigned> read_pos_;
   boost::optional<unsigned> read_len_;
};

/**@brief
*******************************************************************************/
Gene_region make(Proto_gene_region const& pgr, Proto_results_meta& prm);

/**@brief
*******************************************************************************/
struct Proto_combination {
   std::vector<std::string> midv_;
   std::vector<Proto_gene_region> pgrv_;
};

/**@brief
*******************************************************************************/
struct Proto_gl_segment_match {
   std::string id_;
   std::string name_;
   boost::optional<char> type_;
   boost::optional<unsigned> gl_pos0_;
   std::string num_system_;
   std::string gl_db_id_;
   std::string aligner_id_;
};

}//namespace detail
}//namespace vdjml
#endif /* PROTO_ELEMENTS_HPP_ */
