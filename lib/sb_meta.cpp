/** @file "/vdjml/lib/sb_meta.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#include "sb_meta.hpp"

#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/foreach.hpp"

#include "sax_builder.hpp"
#include "sb_result.hpp"
#include "utils_io.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vocabulary.hpp"

namespace vdjml{ namespace detail{

/*
*******************************************************************************/
void Sb_meta::value(std::string const& name, std::string const& val) {

   if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected text")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );

   //user attributes are allowed (and ignored) under <vdjml:meta>
}

/*
*******************************************************************************/
void Sb_meta::open(std::string const& name) {
   if( name == vocab_t::generator::id_() ) {
      sb_->push(Sb_generator(this));
      return;
   }

   if( name == vocab_t::aligner::id_() ) {
      sb_->push(Sb_aligner(sb_, this));
      return;
   }

   if( name == vocab_t::germline_db::id_() ) {
      sb_->push(Sb_germline_db(this));
      return;
   }

   //other elements are allowed (and ignored) under <meta>
   sb_->push(Sb_any(sb_, name));
}

/*
*******************************************************************************/
void Sb_meta::insert(Vdjml_generator_info const& gi) {
   sb_->generator_info() = gi;
}

/*
*******************************************************************************/
void Sb_meta::insert(Proto_aligner_info const& ai) {sb_->proto_meta().insert(ai);}

/*
*******************************************************************************/
void Sb_meta::insert(Proto_gl_db_info const& gl_db) {sb_->proto_meta().insert(gl_db);}

/*
*******************************************************************************/
void Sb_generator::value(std::string const& name, std::string const& val) {
   if( name == vocab_t::name::id_() ) {
      name_ = val;
      return;
   }

   if( name == vocab_t::version::id_() ) {
      version_ = val;
      return;
   }

   if( name == vocab_t::time_gmt::id_() ) {
      try{
         time_gmt_ = boost::date_time::
                  parse_delimited_time<boost::posix_time::ptime>(val, 'T');
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                        Err()
                        << Err::msg_t("dateTime parsing error")
                        << Err::str1_t("\"" + val + "\"")
                        << Err::nested_t(boost::current_exception())
            );
      }
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected text")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_generator::close(std::string const& name) {
   if( name_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined VDJML generator name")
            << Err::str2_t(vocab_t::name::id_())
   );

   if( version_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined VDJML generator version")
            << Err::str2_t(vocab_t::version::id_())
   );

   if( ! time_gmt_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined VDJML generation time")
            << Err::str2_t(vocab_t::version::id_())
   );

   sm_->insert(Vdjml_generator_info(name_, version_, time_gmt_.get()));
}

/*
*******************************************************************************/
void Sb_aligner::value(std::string const& name, std::string const& val) {
   if( name == vocab_t::aligner_id::id_() ) {
      pai_.id_ = val;
      return;
   }

   if( name == vocab_t::name::id_() ) {
      pai_.name_ = val;
      return;
   }

   if( name == vocab_t::version::id_() ) {
      pai_.version_ = val;
      return;
   }

   if( name == vocab_t::uri::id_() ) {
      pai_.uri_ = val;
      return;
   }

   if( name == vocab_t::run_id::id_() ) {
      pai_.run_id_ = parse_unsigned<unsigned>(val);
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected text")
            << Err::str1_t("\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_aligner::open(std::string const& name) {
   if( name == vocab_t::parameters::id_() ) {
      sb_->push(Sb_aligner_params(this));
      return;
   }

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected element")
            << Err::str1_t(name)
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_aligner_params::value(std::string const& name, std::string const& val) {
   if( name.empty() ) {
      parameters_ += val;
      return;
   }

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_germline_db::value(std::string const& name, std::string const& val) {
   if( name == vocab_t::gl_db_id::id_() ) {
      pgdi_.id_ = val;
      return;
   }

   if( name == vocab_t::name::id_() ) {
      pgdi_.name_ = val;
      return;
   }

   if( name == vocab_t::version::id_() ) {
      pgdi_.version_ = val;
      return;
   }

   if( name == vocab_t::species::id_() ) {
      pgdi_.species_ = val;
      return;
   }

   if( name == vocab_t::uri::id_() ) {
      pgdi_.uri_ = val;
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected text")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_germline_db::close(std::string const& name) {
   if( pgdi_.id_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined germline database ID")
            << Err::str2_t(vocab_t::gl_db_id::id_())
   );

   if( pgdi_.name_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined aligner name")
            << Err::str2_t(vocab_t::name::id_())
   );

   if( pgdi_.version_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("undefined aligner version")
            << Err::str2_t(vocab_t::version::id_())
   );

   sm_->insert(pgdi_);
}

}//namespace detail
}//namespace vdjml
