/** @file "/vdjml/lib/istream_chunker.hpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef ISTREAM_CHUNKER_HPP_
#define ISTREAM_CHUNKER_HPP_
#include <istream>
#include "boost/utility/string_ref.hpp"

namespace vdjml{ namespace detail{

struct Chunk {
   boost::string_ref str_;
   bool at_end_;
};

/**@brief split input stream into chunks

End of XML element is always at the end of a chunk.
Chunks are fed to XML SAX parser making it possible to pause or abort parsing
after any XML element.
*******************************************************************************/
template<std::size_t N = 1024> class Istream_chunker {
   static const std::size_t buff_len = N;
public:

   explicit Istream_chunker(std::istream& is)
   : is_(is),
     buff_(),
     buff_end_(buff_len),
     chunk_start_(0),
     chunk_end_(buff_len)
   {}

   void next(Chunk& c) {
      if( chunk_end_ == buff_end_ && is_) {
         is_.read(buff_, buff_len);
         buff_end_ = is_.gcount();
         chunk_end_ = 0;
      }
      chunk_start_ = chunk_end_;
      for( ; chunk_end_ != buff_end_; ++chunk_end_) {
         if( buff_[chunk_end_] == '>' ) {
            ++chunk_end_;
            break;
         }
      }
      c.str_ = boost::string_ref(
               buff_ + chunk_start_,
               chunk_end_ - chunk_start_
      );
      c.at_end_ = (chunk_end_ == buff_end_) && (! is_);
   }

private:
   std::istream& is_;
   char buff_[buff_len];
   std::size_t buff_end_;
   std::size_t chunk_start_;
   std::size_t chunk_end_;
};

}//namespace detail
}//namespace vdjml
#endif /* ISTREAM_CHUNKER_HPP_ */
