/** @file "/vdjml/lib/proto_results_meta.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef PROTO_RESULTS_META_HPP_
#define PROTO_RESULTS_META_HPP_
#include <map>
#include "boost/assert.hpp"
#include "boost/optional.hpp"
#include "boost/shared_ptr.hpp"
#include "vdjml/results_meta.hpp"

namespace vdjml{ namespace detail{
struct Proto_aligner_info;
struct Proto_gl_db_info;

/**@brief
*******************************************************************************/
class Proto_results_meta {
   typedef std::map<std::string,Aligner_id> aligner_id_map;
   typedef std::map<std::string,Gl_db_id> gldb_map;
   typedef Results_meta::Err Err;

   typedef boost::shared_ptr<Results_meta> meta_ptr_t;
public:

   Proto_results_meta() : rm_(new Results_meta()) {}

   explicit Proto_results_meta(meta_ptr_t rm) : rm_(rm) {}
   Aligner_id insert(Proto_aligner_info const& pai);
   Gl_db_id insert(Proto_gl_db_info const& pgl_db);

   meta_ptr_t meta_ptr() const {return rm_;}
   Results_meta const&  meta() const   {return *rm_;}
   Results_meta&        meta()         {return *rm_;}

   Aligner_id aligner_id(std::string const& id) const {
      const aligner_id_map::const_iterator i = aligner_ids_.find(id);
      if( i == aligner_ids_.end() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unknown aligner ID")
               << Err::str1_t(id)
      );
      BOOST_ASSERT( (*rm_)[i->second].id() == i->second );
      return i->second;
   }

   Gl_db_id gl_db_id(std::string const& id) const {
      const gldb_map::const_iterator i = gldb_ids_.find(id);
      if( i == gldb_ids_.end() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unknown germline database ID")
               << Err::str1_t(id)
      );
      BOOST_ASSERT( (*rm_)[i->second].id() == i->second );
      return i->second;
   }

private:
   meta_ptr_t rm_;
   aligner_id_map aligner_ids_;
   gldb_map gldb_ids_;
};

}//namespace detail
}//namespace vdjml
#endif /* PROTO_RESULTS_META_HPP_ */
