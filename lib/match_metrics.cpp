/** @file "/vdjml/lib/match_metrics.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/match_metrics.hpp"

#include <limits>

#include "vdjml/xml_writer.hpp"
#include "vdjml/vocabulary.hpp"

namespace vdjml {

const Match_metrics::score_t Match_metrics::unscore =
         std::numeric_limits<score_t>::max();

typedef vocab::vdjml<>::match_metrics mm_vocab;

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Match_metrics const& mm,
         const unsigned version
) {
   if( mm.identity().is_set() )
      xw.node(mm_vocab::identity::id_(), ATTR, mm.identity());

   if( mm.score() != Match_metrics::unscore )
      xw.node(mm_vocab::score::id_(), ATTR, mm.score());

   if( mm.insertions() )
      xw.node(mm_vocab::insertions::id_(), ATTR, mm.insertions());

   if( mm.deletions() )
      xw.node(mm_vocab::deletions::id_(), ATTR, mm.deletions());

   if( mm.substitutions() )
      xw.node(mm_vocab::substitutions::id_(), ATTR, mm.substitutions());

   if( mm.stop_codon() )
      xw.node(mm_vocab::stop_codon::id_(), ATTR, mm.stop_codon());

   if( mm.mutated_invariant() )
      xw.node(mm_vocab::mutated_invariant::id_(), ATTR, mm.mutated_invariant());

   if( mm.inverted() )
      xw.node(mm_vocab::inverted::id_(), ATTR, mm.inverted());

   if( mm.out_frame_indel() )
      xw.node(mm_vocab::out_frame_indel::id_(), ATTR, mm.out_frame_indel());

   if( mm.out_frame_vdj() )
      xw.node(mm_vocab::out_frame_vdj::id_(), ATTR, mm.out_frame_vdj());
}

}//namespace vdjml
