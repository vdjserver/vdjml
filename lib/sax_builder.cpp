/** @file "/vdjml/lib/sax_builder.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#include "sax_builder.hpp"

#include "vdjml/vdjml_version.hpp"

namespace vdjml{ namespace detail{

/*
*******************************************************************************/
void Sb_any::open(std::string const& name) {sb_->push(Sb_any(sb_, name));}

/*
*******************************************************************************/
void Sb_vdjml::open(std::string const& name) {
   if( ! sb_->v_ ) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("VDJML version not defined")
      );
   }

   if( name == vocab_t::meta::id_() ) {
      sb_->push(Sb_meta(sb_));
      return;
   }

   if( name == vocab_t::read_results::id_() ) {
      sb_->push(Sb_read_results(sb_));
      return;
   }

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected element")
            << Err::str1_t(name)
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_vdjml::value(std::string const& name, std::string const& val) {
   if( name == vocab::vdjml<>::version::id_() ) {
      BOOST_ASSERT( ! sb_->v_ );
      const unsigned v = version(val);
      if( ! is_supported(v) ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unsupported VDJML version")
               << Err::str1_t(val)
               << Err::int1_t(v)
      );

      sb_->v_ = v;
      return;
   }

   if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unexpected text")
               << Err::str1_t("\"" + val + "\"")
               << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_read_results::open(std::string const& name) {
   if( name == vocab_t::read::id_() ) {
      sb_->push(Sb_read(sb_));
      return;
   }

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected element")
            << Err::str1_t(name)
            << Err::str2_t(vocab_t::id_())
   );
}

/*
*******************************************************************************/
void Sb_read_results::value(std::string const& name, std::string const& val) {
   if( name.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected text")
            << Err::str1_t("\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );

   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unexpected attribute")
            << Err::str1_t(name + "=\"" + val + "\"")
            << Err::str2_t(vocab_t::id_())
   );
}

}//namespace detail
}//namespace vdjml
