/** @file "/vdjml/lib/sanitize_string.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/sanitize_string.hpp"

#include <sstream>
#include "boost/spirit/include/karma.hpp"

namespace boost { namespace spirit { namespace traits{

template<> struct is_string<boost::string_ref> : mpl::true_ {};

template<> struct char_type_of<boost::string_ref> {
   typedef char type;
};

}}}//namespace boost, namespace spirit, namespace traits

namespace vdjml{ namespace {

namespace karma = boost::spirit::karma;
/*
*******************************************************************************/
template<typename Iter> struct Escape : karma::grammar<Iter, std::string()> {
public:
   Escape() : Escape::base_type(esc_str_)
   {
      esc_str_ =
               karma::repeat [ esc_char_ | karma::print | "\\x" << karma::hex ]
        ;

      esc_char_.add('\a', "\\a")('\b', "\\b")('\f', "\\f")('\n', "\\n")
               ('\r', "\\r")('\t', "\\t")('\v', "\\v")('\\', "\\\\")
               ('\'', "\\\'")('\"', "\\\"")
               ;
   }

private:
   karma::rule<Iter, std::string()> esc_str_;
   karma::symbols<char, char const*> esc_char_;
};

}//anonymous namespace

/*
*******************************************************************************/
std::string sanitize(const char c) {
   std::ostringstream os;
   os << '"';
   typedef std::ostream_iterator<char> iter;
   Escape<iter> escape;
   generate(iter(os, ""), escape, std::string(1, c));
   os << '"';
   return os.str();
}

/*
*******************************************************************************/
std::string sanitize(std::string const& str) {
   std::ostringstream os;
   os << '"';
   typedef std::ostream_iterator<char> iter;
   Escape<iter> escape;
   generate(iter(os, ""), escape, str);
   os << '"';
   return os.str();
}

/*
*******************************************************************************/
std::string sanitize(const boost::string_ref str, const std::size_t max_len) {
   std::ostringstream os;
   os << "\"";
   typedef std::ostream_iterator<char> iter;
   Escape<iter> escape;
   generate(iter(os, ""), escape, str.substr(0, max_len));
   if( str.size() > max_len ) os << "...";
   os << "\"";
   return os.str();
}

}//namespace vdjml
