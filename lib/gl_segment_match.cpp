/** @file "/vdjml/lib/gl_segment_match.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/gl_segment_match.hpp"

#include "utils_io.hpp"
#include "vdjml/gene_segment_type.hpp"
#include "vdjml/germline_segment_info.hpp"
#include "vdjml/results_meta.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {
typedef vocab::vdjml<>::read_results::read read_vocab;
typedef read_vocab::alignment::segment_match segment_vocab;
typedef segment_vocab::gl_seg_match gl_seg_vocab;

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Gl_segment_match const& gsm,
         Results_meta const& rm,
         const unsigned v
) {
   xw.open(gl_seg_vocab::id_(), ELEM);

   write_id(xw, gsm.id(), rm, v);
   write_id(xw, gsm.num_system(), rm, v);
   write(xw, rm.gl_segment_map()[gsm.gl_segment()], rm, v);
   write_id(xw, gsm.aligner(), rm, v);
   xw.node(gl_seg_vocab::gl_pos0::id_(), ATTR, gsm.gl_position());

   xw.close(); //segment_vocab::gl_seg_match::id_(), ELEM
}

/*
*******************************************************************************/
std::string const&
segment_name(Gl_segment_match const& gsm, Results_meta const& rm) {
   return rm[gsm.gl_segment()].name();
}

/*
*******************************************************************************/
std::string const&
numbering_system(Gl_segment_match const& gsm, Results_meta const& rm) {
   return rm[gsm.num_system()];
}

/*
*******************************************************************************/
char
segment_type(Gl_segment_match const& gsm, Results_meta const& rm) {
   return segment_type(rm[gsm.gl_segment()].segment_type());
}

}//namespace vdjml
