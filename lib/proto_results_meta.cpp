/** @file "/vdjml/lib/proto_results_meta.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef PROTO_RESULTS_META_CPP_
#define PROTO_RESULTS_META_CPP_
#include "proto_results_meta.hpp"

#include "proto_elements.hpp"
#include "vdjml/aligner_info.hpp"
#include "vdjml/germline_db_info.hpp"

namespace vdjml{ namespace detail{

/*
*******************************************************************************/
Aligner_id Proto_results_meta::insert(Proto_aligner_info const& pai) {
   const Aligner_info ai(
            pai.name_,
            pai.version_,
            pai.parameters_,
            pai.uri_,
            pai.run_id_
   );
   const aligner_id_map::const_iterator i = aligner_ids_.find(pai.id_);
   if( i != aligner_ids_.end() ) {
      if( (*rm_)[i->second] != ai ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("duplicate aligner ID")
               << Err::str1_t(pai.id_)
      );
      return i->second;
   }

   const Aligner_id id = rm_->insert(ai);
   aligner_ids_[pai.id_] = id;
   return id;
}

/*
*******************************************************************************/
Gl_db_id Proto_results_meta::insert(Proto_gl_db_info const& pgdi) {
   const Gl_db_info gdi(
            pgdi.name_,
            pgdi.version_,
            pgdi.species_,
            pgdi.uri_
   );
   const gldb_map::const_iterator i = gldb_ids_.find(pgdi.id_);
   if( i != gldb_ids_.end() ) {
      if( (*rm_)[i->second] != gdi ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("duplicate germline database ID")
               << Err::str1_t(pgdi.id_)
      );
      return i->second;
   }
   const Gl_db_id id = rm_->insert(gdi);
   gldb_ids_[pgdi.id_] = id;
   return id;
}

}//namespace detail
}//namespace vdjml
#endif /* PROTO_RESULTS_META_CPP_ */
