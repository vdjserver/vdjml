/** @file "/vdjml/lib/results_meta.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/results_meta.hpp"

#include "boost/foreach.hpp"

#include "vdjml/generator_info.hpp"
#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Results_meta const& rm,
         const unsigned v
) {
   typedef vocab::vdjml<>::meta vocab_t;
   xw.open(vocab_t::id_(), ELEM);

   write(xw, Vdjml_generator_info(), v);

   BOOST_FOREACH(Aligner_info const& ai, rm.aligner_map()) {
      write(xw, ai, rm, v);
   }

   BOOST_FOREACH(Gl_db_info const& gdi, rm.gl_db_map()) {
      write(xw, gdi, rm, v);
   }

   xw.close(); //vocab_t::id_() ELEM
}

}//namespace vdjml
