/** @file "/vdjml/lib/proto_read_result.hpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef PROTO_READ_RESULT_HPP_
#define PROTO_READ_RESULT_HPP_
#include <map>
#include <string>
#include "vdjml/exception.hpp"
#include "vdjml/object_ids.hpp"
#include "vdjml/read_result_fwd.hpp"

namespace vdjml{ namespace detail{
class Proto_results_meta;
class Proto_segment_match;
struct Proto_combination;

/**@brief
*******************************************************************************/
class Proto_read_result {
   typedef std::map<std::string,Seg_match_id> sm_id_map;
public:
   struct Err : public base_exception {};
   explicit Proto_read_result(Proto_results_meta& prm) : prm_(prm) {}
   Proto_read_result(Proto_results_meta& prm, std::string const& id);
   Proto_read_result(Proto_results_meta& prm, read_result_ptr rr);

   void id(std::string const& id);
   Seg_match_id insert(Proto_segment_match const& psm);
   Seg_match_id segment_id(std::string const& id) const;
   void insert(Proto_combination const& pc);
   bool has_result() const {return rr_.get();}
   read_result_ptr release() {return rr_;}
   Read_result const& deref() const {return *rr_;}
   Read_result& deref() {return *rr_;}

private:
   Proto_results_meta& prm_;
   read_result_ptr rr_;
   sm_id_map smidm_;
};

}//namespace detail
}//namespace vdjml
#endif /* PROTO_READ_RESULT_HPP_ */
