/** @file "/vdjml/lib/btop.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/btop.hpp"

#include <cctype>
#include <sstream>
#include "boost/algorithm/string/case_conv.hpp"

#include "vdjml/nucleotide_iterator.hpp"
#include "vdjml/codon_iterator.hpp"

namespace vdjml {

/*
*******************************************************************************/
const std::size_t Btop::unset = static_cast<std::size_t>(-1);

/*
*******************************************************************************/
Btop::Btop(std::string const& val)
: val_(val)
{
   boost::to_upper(val_);
   try{
      validate(val_);
   } catch(base_exception const&) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("invalid BTOP")
               << Err::str1_t(val)
               << Err::nested_t(boost::current_exception())
      );
   }
}

/*
*******************************************************************************/
void Btop::validate(std::string const& btop) {
   nucleotide::Nuc nuc1 = nucleotide::Unknown;
   bool expect_second = false;
   for(std::size_t i = 0; i != btop.size(); ++i) {
      const char c = btop[i];
      if( std::isdigit(c) ) {
         if( expect_second ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("expected second nucleotide")
                  << Err::int1_t(i)
         );
         continue;
      }
      if( ! is_nucleotide(c) ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("expected second nucleotide")
               << Err::int1_t(i)
      );

      nucleotide::Nuc nuc;

      try{
         nuc = nucleotide_index(c);
      } catch(base_exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("invalid nucleotide")
                  << Err::str1_t(sanitize(c))
                  << Err::int1_t(i)
                  << Err::nested_t(boost::current_exception())
         );
      }

      if( expect_second ) {
         if( nuc1 == nuc ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("same nucleotides")
                  << Err::str1_t(sanitize(c))
                  << Err::str2_t(sanitize(to_capital(nuc1)))
                  << Err::int1_t(i)
         );
         expect_second = false;
      } else {
         nuc1 = nuc;
         expect_second = true;
      }
   }

   if( expect_second ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("expected second nucleotide at end")
   );
}

/*
*******************************************************************************/
bool check(Btop const& btop, Aligned_seq const& seq) {
   Mismatch_iter i(btop);
   for( ; ! i.at_end(); i.next()) {
      const std::size_t pos = i.positions()[seq.kind()];
      if( pos >= seq.seq().size() ) return false;
      const char nuc = seq.seq()[pos];
      if( ! is_nucleotide(nuc) ) return false;
      if( nucleotide_index(nuc) != nucleotide_index(i.letters()[seq.kind()]) )
         return false;
   }
   if( i.positions()[seq.kind()] != seq.seq().size() ) return false;
   return true;
}

/*
*******************************************************************************/
void validate(Btop const& btop, Aligned_seq const& seq) {
   typedef Btop::Err Err;
   Mismatch_iter i(btop);
   for( ; ! i.at_end(); i.next()) {
      const std::size_t pos = i.positions()[seq.kind()];
      if( pos >= seq.seq().size() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("sequence is too short")
               << Err::int1_t(seq.seq().size())
               << Err::int2_t(pos)
      );
      const char nuc = seq.seq()[pos];
      if( ! is_nucleotide(nuc) ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("invalid nucleotide character")
               << Err::str1_t(sanitize(nuc))
               << Err::int1_t(pos)
      );
      if( nucleotide_index(nuc) != nucleotide_index(i.letters()[seq.kind()]) )
         BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("nucleotide mismatch")
                     << Err::str1_t(sanitize(nuc))
                     << Err::str1_t(sanitize(i.letters()[seq.kind()]))
         );
   }
   if( i.positions()[seq.kind()] != seq.seq().size() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("sequence size mismatch")
            << Err::int1_t(seq.seq().size())
            << Err::int2_t(i.positions()[seq.kind()])
   );
}

/*
*******************************************************************************/
Btop_stats::Btop_stats(Btop const& btop)
: substitutions_(0),
  insertions_(0),
  deletions_(0),
  read_len_(0),
  gl_len_(0)
{
   Mismatch_iter mi(btop);
   for( ; ! mi.at_end(); mi.next() ) {
      if( mi.is_gap(seq::Read) ) {
         ++deletions_;
      } else if( mi.is_gap(seq::GL) ) {
         ++insertions_;
      } else {
         ++substitutions_;
      }
   }
   read_len_ = mi.positions()[seq::Read];
   gl_len_ = mi.positions()[seq::GL];
}

namespace{

/*
*******************************************************************************/
template<class SeqIn> Nucleotide_match nucleotide_match(
         Aligned_nuc_iter<SeqIn>& iter,
         Aligned_pos const& ap
) {
   iter.move_to(ap);
   if( iter.at_end() ) return Nucleotide_match();
   BOOST_ASSERT(iter.positions()[ap.kind()] == ap.pos());
   return Nucleotide_match(iter.positions(), iter.letters());
}

}//anonymous namespace

/*
*******************************************************************************/
Nucleotide_match nucleotide_match(
         Btop const& btop,
         Aligned_pos const& ap,
         const char match
) {
   nucleotide_iter iter = nucleotide_iterator(btop, match);
   return nucleotide_match(iter, ap);
}

/*
*******************************************************************************/
Nucleotide_match nucleotide_match(
         Btop const& btop,
         Aligned_pos const& ap,
         Aligned_seq const& as
) {
   nucleotide_iter_s iter = nucleotide_iterator(btop, as);
   return nucleotide_match(iter, ap);
}

namespace{

/*
*******************************************************************************/
template<class SeqIn> Sequence_match sequence_match(
         Aligned_nuc_iter<SeqIn>& iter,
         Aligned_pos const& start,
         Aligned_pos const& end
) {
   if( start.is_set() ) iter.move_to(start);
   if( iter.at_end() ) {
      const Sequence_match as =
      {{{Btop::unset,Btop::unset}},{{Btop::unset,Btop::unset}}};
      return as;
   }

   Sequence_match as = { iter.positions(), {{Btop::unset,Btop::unset}} };

   boost::array<std::ostringstream,2> oss;

   for( ; ! iter.at_end(); iter.next() ) {
      const boost::array<char,2> p = iter.letters();

      if(
               iter.positions()[end.kind()] == end.pos() &&
               p[end.kind()] != nucleotide::Special::Gap
      ) break;

      oss[seq::Read] << p[seq::Read];
      oss[seq::GL] << p[seq::GL];
   }

   as.seq_[seq::Read] = oss[seq::Read].str();
   as.seq_[seq::GL] = oss[seq::GL].str();
   as.end_ = iter.positions();

   return as;
}

}//anonymous namespace

/*
*******************************************************************************/
Sequence_match sequence_match(
         Btop const& btop,
         Aligned_pos const& start,
         Aligned_pos const& end,
         const char match
) {
   nucleotide_iter iter = nucleotide_iterator(btop, match);
   return sequence_match(iter, start, end);
}

/*
*******************************************************************************/
Sequence_match sequence_match(
         Btop const& btop,
         Aligned_pos const& start,
         Aligned_pos const& end,
         Aligned_seq const& as
) {
   nucleotide_iter_s iter = nucleotide_iterator(btop, as);
   return sequence_match(iter, start, end);
}

/*
*******************************************************************************/
bool is_open_frame(
         Btop const& btop,
         Aligned_pos const& start,
         Aligned_seq const& seq
) {
   for(
            codon_iter_sbsg i = codon_iterator(btop, start, seq, start.kind());
            ! i.at_end();
            i.next()
   ) {
      const codon::Codon c = i.get().codon(start.kind());
      if( is_translatable(c) && translate(c) == aminoacid::Stop ) return false;
   }
   return true;
}

/*
*******************************************************************************/
int find_open_frame(
         Btop const& btop,
         Aligned_pos const& start,
         Aligned_seq const& seq
) {
   const bool b0 = is_open_frame(btop, Aligned_pos(start.kind(), 0), seq);
   const bool b1 = is_open_frame(btop, Aligned_pos(start.kind(), 1), seq);
   const bool b2 = is_open_frame(btop, Aligned_pos(start.kind(), 2), seq);
   if( b0 && !(b1 || b2) ) return 0;
   if( b1 && !(b0 || b2) ) return 1;
   if( b2 && !(b1 || b0) ) return 2;
   return -1;
}

}//namespace vdjml
