/** @file "/vdjml/lib/germline_segment_info.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/germline_segment_info.hpp"

#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {

/**
*******************************************************************************/
void write(
         Xml_writer& xw,
         Gl_segment_info const& gsi,
         Results_meta const&,
         const unsigned v
) {
   typedef vocab::vdjml<>::read_results::read::alignment::
            segment_match::gl_seg_match
            vocab_t;

   xw.node(vocab_t::name::id_(), ATTR, gsi.name());
   xw.node(vocab_t::gl_db_id::id_(), ATTR, gsi.gl_database());
   xw.node(vocab_t::type::id_(), ATTR, segment_type(gsi.segment_type()));
}

}//namespace vdjml
