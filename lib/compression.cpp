/** @file "/vdjml/lib/compression.cpp" 
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/compression.hpp"

#include <fstream>
#include "boost/filesystem.hpp"
#include "boost/algorithm/string/case_conv.hpp"

namespace vdjml {

/*
*******************************************************************************/
Compression guess_compression_magic(std::istream& is) {
   static const std::size_t N = 7;
   char c[N];
   for(std::size_t n = 0; n != N; ++n) {
      is.get(c[n]);
   }

   if( c[0] == (char)0x1F && c[1] == (char)0x8B ) {
      return gzip;
   }

   if( c[0] == (char)0x1F && c[1] == (char)0x9D ) {
      return zlib;
   }

   if( c[0] == (char)0x42 && c[1] == (char)0x5A && c[2] == (char)0x68 ) {
      return bzip2;
   }

   if(
            c[0] == (char)0x50 && c[1] == (char)0x4B && c[2] == (char)0x03 &&
            c[3] == (char)0x04
   ) {
      return pkzip;
   }

   if(
            c[0] == (char)0x52 && c[1] == (char)0x61 && c[2] == (char)0x72 &&
            c[3] == (char)0x21 && c[4] == (char)0x1A && c[5] == (char)0x07 &&
            c[6] == (char)0x01
   ) {
      return rar;
   }
   if(
            c[0] == (char)0x37 && c[1] == (char)0x7A && c[2] == (char)0xBC &&
            c[3] == (char)0xAF && c[4] == (char)0x27 && c[5] == (char)0x1C
   ) {
      return seven_z;
   }

   return Uncompressed;
}

/*
*******************************************************************************/
Compression guess_compression_magic(std::string const& path) {
   std::ifstream ifs(path.c_str(), std::ios_base::binary);
   return guess_compression_magic(ifs);
}

/*
*******************************************************************************/
Compression guess_compression_ext(std::string const& path) {
   std::ifstream ifs(path.c_str(), std::ios_base::binary);
   char c1;
   ifs.get(c1);

   boost::filesystem::path p(path);
   std::string ext = p.extension().string();
   boost::algorithm::to_lower(ext);
   if( ext == ".gz") return gzip;
   if( ext == ".z") return zlib;
   if( ext == ".bz2") return bzip2;
   return Uncompressed;
}

}//namespace vdjml
