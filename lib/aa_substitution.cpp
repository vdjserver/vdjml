/** @file "/vdjml/lib/aa_substitution.cpp"
part of vdjml project.
@n Distributed under the Boost Software License, Version 1.0; see doc/license.txt.
@copyright The University of Texas Southwestern Medical Center, 2014
@date 2014 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJML_SOURCE
#define VDJML_SOURCE
#endif
#include "vdjml/aa_substitution.hpp"

#include "vdjml/vocabulary.hpp"
#include "vdjml/xml_writer.hpp"

namespace vdjml {

/*
*******************************************************************************/
void write(
         Xml_writer& xw,
         Aa_substitution const& aas,
         const unsigned version
) {
   typedef
            vocab::vdjml<>::read_results::read::alignment::
            segment_match::aa_substitution
            vocab_t;

   xw.open(vocab_t::id_(), ELEM);
   xw.node(vocab_t::read_pos0::id_(), ATTR, aas.read_position());
   xw.node(vocab_t::read_aa::id_(), ATTR, to_capital(aas.read_aa()));
   xw.node(vocab_t::gl_aa::id_(), ATTR, to_capital(aas.gl_aa()));
   xw.close(); //vocab_t::id_(), ELEM
}

}//namespace vdjml
