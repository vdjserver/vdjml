*******
*vdjml*
*******

*vdjml* provides tools for capturing the results of immune receptor sequence 
alignment.
The project defines *VDJML* file format for sharing the alignment results.
It also provides *libVDJML*, a native library with Python bindings for reading, 
storing, and writing *VDJML* data.

Authors
-------
 - Lindsay G. Cowell
 - John Fonner
 - Jason Vander Helden
 - Uri Hershberg
 - Christopher Jordan
 - Steven Kleinstein
 - Mikhail K. Levin
 - Stephen A. Mock
 - Nancy Monson
 - William Rounds
 - Florian Rubelt
 - Edward Salinas
 - Walter Scarborough
 - Richard Scheuermann
 - Matt Stelmaszek
 - Inimary Toby
 - Scott Christley

*vdjml* is distributed under the terms of Boost Software License.

For building, see doc/build.txt.

For legal information see doc/license.txt.

Contact: lindsay.cowell(at)utsouthwestern.edu